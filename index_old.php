<?php
require_once '/common/global.inc.php';
require_once '/common/common.php';

if(isset($_POST['submit']))
{

//PERSONAL DETAILS AND EDUCATION/TRAINING

  $em_family_name = $_POST['em_family_name'];
  $em_other_name = $_POST['em_other_name'];
  $em_address = $_POST['em_address'];
  $em_postcode = $_POST['em_postcode'];
  $em_phone_no = $_POST['em_phone_no'];
  $em_mobile_no = $_POST['em_mobile_no'];
  $em_entitled_australia = $_POST['em_entitled_australia_rd'];
  $em_identity_doc = ($_POST['em_identity_doc1'].','.$_POST['em_identity_doc2'].','.$_POST['em_identity_doc3']);
  $em_current_study =  implode('#', $_POST['em_current_study']);
  $em_study_type = implode('#', $_POST['em_study_type']);
  $em_institute_type = implode('#', $_POST['em_institute_type']);
  $em_hs_name = $_POST['em_hs_name'];
  $em_hs_grade = $_POST['em_hs_grade'];
  $em_hs_year = $_POST['em_hs_year'];
  $em_ps_commenced = implode('#', $_POST['em_ps_commenced']);
  $em_ps_year = implode('#', $_POST['em_ps_year']);
  $em_qualification = implode('#', $_POST['em_qualification']);
  $em_signature = $_POST['em_signature'];
  $em_date = $_POST['em_date'];

  //POSITION APPLIED FOR

  $em_job_title = implode('#', $_POST['em_job_title']);
  $em_position_status = implode('#', $_POST['em_position_status_ch']);
  $em_limit_availability = implode('#', $_POST['em_limit_availability']);
  $em_monday_detail = implode('#', $_POST['monday_detail']);
  $em_tuesday_detail = implode('#', $_POST['tuesday_detail']);
  $em_wednesday_detail = implode('#', $_POST['wednesday_detail']);
  $em_thursday_detail = implode('#', $_POST['thursday_detail']);
  $em_friday_detail = implode('#', $_POST['friday_detail']);
  $em_saturday_detail = implode('#', $_POST['saturday_detail']);
  $em_sunday_detail = implode('#', $_POST['sunday_detail']);

  //GENERAL INFO

  $em_skill = $_POST['em_skill'];
  $em_hobby = $_POST['em_hobby'];
  $em_prep_medical = $_POST['em_prep_medical'];
  $em_physical_disability_status = implode('#', array( $_POST['em_disability_status'],$_POST['em_disability_status_info'] ));
  $em_nature_of_work = $_POST['em_nature_of_work'];
  $em_injury_status = implode('#', array( $_POST['em_injury_status'],$_POST['em_injury_status_info'] ));
  $em_criminal_status =  implode('#', array( $_POST['em_criminal_status'],$_POST['em_criminal_status_info'] ));
  $em_charges_status =  implode('#', array( $_POST['em_charges_status'],$_POST['em_charges_status_info'] ));
  $em_additional_info = $_POST['em_additionl'];



  $employee_master = mysqli_query($link,"INSERT INTO `employee_master`(`em_id`, `sh_master_id`, `em_family_name`, `em_other_name`, `em_address`, `em_postcode`, `em_phone_no`, `em_mobile_no`, `em_entitled_austrailia`, `em_identity_doc`, `em_current_study_status`, `em_study_type`, `em_institute_type`, `em_hs_name`, `em_hs_grade`, `em_hs_year`, `em_ps_commenced`, `em_ps_year`, `em_qualification`, `em_signature`, `em_signature_date`, `created_date`, `updated_date`) VALUES ('','1','".$em_family_name."','".$em_other_name."','".$em_address."','".$em_postcode."','".$em_phone_no."','".$em_mobile_no."','".$em_entitled_australia."','".$em_identity_doc."','".$em_current_study."','".$em_study_type."','".$em_institute_type."','".$em_hs_name."','".$em_hs_grade."','".$em_hs_year."','".$em_ps_commenced."','".$em_ps_year."','".$em_qualification."','".$em_signature."','".$em_date."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')") or die(mysqli_error($link));

  $foreign_key = $link->insert_id;

  $employee_applied_for = mysqli_query($link, "INSERT INTO  `employee_applied_for` (  `em_id` ,  `em_job_title` ,  `em_position_status` ,  `em_limit_availability` ,  `em_monday_detail` ,  `em_tuesday_detail` , `em_wednesday_detail` ,  `em_thursday_detail` ,  `em_friday_detail` ,  `em_saturday_detail` ,  `em_sunday_detail` ,  `created_date` ,  `updated_date` ) VALUES ('".$foreign_key."' , '".$em_job_title."' , '".$em_position_status."' , '".$em_limit_availability."' , '".$em_monday_detail."' , '".$em_tuesday_detail."' , '".$em_wednesday_detail."' , '".$em_thursday_detail."' , '".$em_friday_detail."' , '".$em_saturday_detail."','".$em_sunday_detail."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."') ") or die(mysqli_error($link));

  $employee_general_info = mysqli_query($link, " INSERT INTO `employee_general_info`(`em_id`, `em_skill`, `em_hobby`, `em_prep_medical`, `em_physical_disability_status`, `em_nature_of_work`, `em_injury_status`, `em_criminal_status`, `em_charges_status`, `em_additional_info`, `created_date`, `updated_date`) VALUES ('".$foreign_key."','".$em_skill."','".$em_hobby."','".$em_prep_medical."','".$em_physical_disability_status."','".$em_nature_of_work."','".$em_injury_status."','".$em_criminal_status."','".$em_charges_status."','".$em_additional_info."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."') ");


  if(!$employee_master)
  {
    echo $employee_master;
  } 
  else
  {
    echo $employee_master;
  }
}
else
{
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Welcome to OG Foods Pty Ltd</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
 
});
	
function AddRow()
{
    $('#myTable').append('<tr><td>&nbsp;</td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td><td><input type="text" class="form-control"></td></tr>')
}

</script>
</head>

<body>
<h1 class="text-center pt-3">OG Foods Pty Ltd T/As Hungry Jacks Wodonga</h1>
<h2 class="text-center pt-3">Application for Employment Form</h2>
<h3 class="text-center pt-3 pb-3">PRIVATE AND CONFIDENTIAL</h3>
<div class="container">
<form action="" method="POST">
  <div class="col-sm-12 titleColor">1. PERSONAL DETAILS</div>
  <div class="col-sm-12 bgMain">
    <ul class="formMain">
      <li class="col-sm-3">
        <label>Family Name:</label>
        <input type="text" name="em_family_name" id="em_family_name" class="form-control">
      </li>
      <li class="col-sm-3">
        <label>Other Names:</label>
        <input type="text" name="em_other_name" id="em_other_name" class="form-control">
      </li>
      <li class="col-sm-6">
        <label>Address:</label>
        <input type="text" name="em_address" id="em_address" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Postcode:</label>
        <input type="text" name="em_postcode" id="em_postcode" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Phone No:</label>
        <input type="text" name="em_phone_no" id="em_phone_no" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Mobile:</label>
        <input type="text" name="em_mobile_no" id="em_mobile_no" class="form-control">
      </li>
      <li class="col-sm-6 pt-3">
        <label>Are you legally entitled to work in Australia?</label>
        <div class="col-sm-6">Yes
          <input type="radio" id="em_entitled_australia_rd" value="1" name="em_entitled_australia_rd">
        </div>
        <div class="col-sm-6">No
          <input type="radio" name="em_entitled_australia_rd" value="0" id="em_entitled_australia_rd">
        </div>
      </li>
      <li class="col-sm-6 pt-3">
        <label>What identification can you produce to verify this?</label>
        <div class="col-sm-6">Passport</div>
        <div class="col-sm-3">Yes
          <input type="radio" name="em_identity_doc1" value="1" id="em_identity_doc1">
        </div>
        <div class="col-sm-3">No
          <input type="radio" name="em_identity_doc1" value="0" id="em_identity_doc1">
        </div>
        <div class="col-sm-6">Birth Certificate</div>
        <div class="col-sm-3">Yes
          <input type="radio" name="em_identity_doc2" value="1" id="em_identity_doc2">
        </div>
        <div class="col-sm-3">No
          <input type="radio" name="em_identity_doc2" value="0" id="em_identity_doc2">
        </div>
        <div class="col-sm-6">Other (provide details)</div>
        <div class="col-sm-3">Yes
          <input type="radio" name="em_identity_doc3" value="1" id="em_identity_doc3">
        </div>
        <div class="col-sm-3">No
          <input type="radio" name="em_identity_doc3" value="0" id="em_identity_doc3">
        </div>
      </li>
    </ul>
  </div>
  <div class="col-sm-12 titleColor">2. POSITION APPLIED FOR</div>
  <div class="col-sm-12 bgMain">
    <ul class="formMain">
      <li class="col-sm-6">
        <label>Job Title:</label>
        <input type="text" class="form-control" name="em_job_title[]" id="em_job_title">
      </li>
      <li class="col-sm-6">
        <label>Second Choice (if relevant)</label>
        <input type="text" class="form-control" name="em_job_title[]"  id="em_job_title">
      </li>
      <li class="col-sm-6 pt-3">
        <label>Status of position applied for:</label>
        <div class="col-sm-4">
          <input type="checkbox" name="em_position_status_ch[]" value="full_time" id="em_position_status_ch">
          Full-Time</div>
        <div class="col-sm-4">
          <input type="checkbox" name="em_position_status_ch[]" value="part_time" id="em_position_status_ch">
          Part-Time</div>
        <div class="col-sm-4">
          <input type="checkbox" name="em_position_status_ch[]" value="casual" id="em_position_status_ch">
          Casual</div>
      </li>
      <li class="col-sm-12 pt-3">
        <label>Are there any circumstances known to you which could affect your ability to undertake either shift or weekend work?</label>
        <div class="col-sm-4">
          <input type="checkbox" name="em_limit_availability[]" id="em_limit_availability">
          Yes</div>
        <div class="col-sm-4">
          <input type="checkbox" name="em_limit_availability[]" id="em_limit_availability">
          No</div>
      </li>
      <li class="col-sm-12 pt-3">
        <label>If Yes, please provide details of any limitations on your availability.</label>
        <textarea class="form-control" name="em_limit_availability[]" id="em_limit_availability"></textarea>
      </li>
      <li class="col-sm-12 pt-3">
        <label>Please indicate your nonavailability to work below and provide details in the table below:</label>
        <div class="table-responsive">
          <table class="table table-striped table-hover">
            <tbody>
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
                <th>Saturday</th>
                <th>Sunday</th>
              </tr>
            </thead>
            <tr>
              <td>Unavailable</td>
              <td><input type="text" class="form-control" name="monday_detail[]" id="monday_detail"></td>
              <td><input type="text" class="form-control" name="tuesday_detail[]" id="tuesday_detail"></td>
              <td><input type="text" class="form-control" name="wednesday_detail[]" id="wednesday_detail"></td>
              <td><input type="text" class="form-control" name="thursday_detail[]" id="thursday_detail"></td>
              <td><input type="text" class="form-control" name="friday_detail[]" id="friday_detail"></td>
              <td><input type="text" class="form-control" name="saturday_detail[]" id="saturday_detail"></td>
              <td><input type="text" class="form-control" name="sunday_detail[]" id="sunday_detail"></td>
            </tr>
            <tr>
              <td>Details:</td>
              <td><input type="text" class="form-control" name="monday_detail[]" id="monday_detail_1"></td>
              <td><input type="text" class="form-control" name="tuesday_detail[]" id="tuesday_detail_1"></td>
              <td><input type="text" class="form-control" name="wednesday_detail[]" id="wednesday_detail_1"></td>
              <td><input type="text" class="form-control" name="thursday_detail[]" id="thursday_detail_1"></td>
              <td><input type="text" class="form-control" name="friday_detail[]" id="friday_detail_1"></td>
              <td><input type="text" class="form-control" name="saturday_detail[]" id="saturday_detail_1"></td>
              <td><input type="text" class="form-control" name="sunday_detail[]" id="sunday_detail_1"></td>
            </tr>
              </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
  <div class="col-sm-12 titleColor">3. EDUCATION/TRAINING</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-12">
      <div class="bgSub p-3">» Current Study</div>
      <ul class="formMain">
        <li class="col-sm-4 pt-3">
          <label>Are you currently studying?</label>
          <div class="col-sm-9">
            <input type="checkbox" name="em_current_study[]" value="1" id="em_current_study">
            Yes (If yes, specify below)</div>
          <div class="col-sm-3">
            <input type="checkbox" name="em_current_study[]" value="0" id="em_current_study">
            No</div>
        </li>
        <li class="col-sm-4 pt-3">
          <label>What is your form of study?</label>
          <div class="col-sm-6">
            <input type="checkbox" name="em_study_type[]" value="full_time" id="em_study_type">
            Full time</div>
          <div class="col-sm-6">
            <input type="checkbox" name="em_study_type[]" value="part_time" id="em_study_type">
            Part time</div>
        </li>
        <li class="col-sm-4 pt-3">
          <label>Which institution do you attend?</label>
          <div class="col-sm-4">
            <input type="checkbox" name="em_institute_type[]" value="school" id="em_institute_type">
            School</div>
          <div class="col-sm-5">
            <input type="checkbox" name="em_institute_type[]" value="university" id="em_institute_type_cb1">
            University</div>
          <div class="col-sm-5">
            <input type="checkbox" name="em_institute_type[]" value="tafe/other" id="em_institute_type_cb2">
            TAFE/Other</div>
        </li>
      </ul>
    </div>
    <div class="col-sm-12 pt-3">
      <div class="bgSub p-3">» Qualifications Attained</div>
      <div class="brdSub">Secondary -</div>
      <ul class="formMain">
        <li class="col-sm-5 pt-3">
          <label>Name of High School attended:</label>
          <input type="text" class="form-control" name="em_hs_name" id="em_hs_name">
        </li>
        <li class="col-sm-5 pt-3">
          <label>Highest grade/level passed:</label>
          <input type="text" class="form-control" name="em_hs_grade" id="em_hs_grade">
        </li>
        <li class="col-sm-2 pt-3">
          <label>In what year?:</label>
          <input type="text" class="form-control" name="em_hs_year" id="em_hs_year">
        </li>
      </ul>
      <div class="col-sm-12 p-0 pt-3">
        <div class="brdSub">Post-Secondary – <small>(attach a separate document if insufficient space)</small></div>
        <ul class="formMain">
          <li class="col-sm-4 pt-3">
            <label>When commenced:</label>
            <div class="col-sm-4 pt-3"> 
              <input type="text" class="form-control" placeholder="1" name="em_ps_commenced[]" id="em_ps_commenced1">
            </div>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="2" name="em_ps_commenced[]" id="em_ps_commenced2">
            </div>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="3" name="em_ps_commenced[]" id="em_ps_commenced3">
            </div>
          </li>
          <li class="col-sm-4 pt-3">
            <label>Year completed:</label>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="1" name="em_ps_year[]" id="em_ps_year1">
            </div>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="2" name="em_ps_year[]" id="em_ps_year2">
            </div>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="3" name="em_ps_year[]" id="em_ps_year3">
            </div>
          </li>
          <li class="col-sm-4 pt-3">
            <label>Qualification/s Obtained:</label>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="1" name="em_qualification[]" id="em_qualification1">
            </div>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="2" name="em_qualification[]" id="em_qualification2">
            </div>
            <div class="col-sm-4 pt-3">
              <input type="text" class="form-control" placeholder="3" name="em_qualification[]" id="em_qualification3">
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-12 titleColor">4. EMPLOYMENT HISTORY</div>
  <div class="col-sm-12 bgMain">
    <p>By providing us with a list of referees whom we may speak to about your prior employment
      history, you acknowledge that we may approach these referees.
      (Note-we will ordinarily seek your consent before contacting your current employer).<br>
      <br>
      Please complete details below. Also attach Resume if available.</p>
    <div class="table-responsive">
      <table class="table table-striped table-hover" id="myTable">
        <tbody>
        <thead>
          <tr>
            <th nowrap><span class="cursor" onclick="AddRow()">+</span></th>
            <th nowrap>Employer Name,Tel No / Address</th>
            <th nowrap>From / To</th>
            <th nowrap>Position</th>
            <th nowrap>Reason for Leaving</th>
            <th nowrap>Referees Name &amp; Phone</th>
          </tr>
        </thead>
          </tbody>
        
      </table>
    </div>
  </div>
  <div class="col-sm-12 titleColor">5. Please list any skills, knowledge or experience that you may have which are
    relevant to the position for which you are applying.</div>
  <div class="col-sm-12 bgMain">
    <textarea class="form-control" name="em_skill" id="em_skill"></textarea>
  </div>
  <div class="col-sm-12 titleColor">6. What are your hobbies and interests?</div>
  <div class="col-sm-12 bgMain">
    <textarea class="form-control" name="em_hobby" id="em_hobby"></textarea>
  </div>
  <div class="col-sm-12 titleColor">7. Are you prepared to attend a medical examination by our Doctor, if required?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_prep_medical" value="1" id="em_prep_medical">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_prep_medical" value="0" id="em_prep_medical">
    </div>
  </div>
  <div class="col-sm-12 titleColor">8a. Do you suffer from any physical or mental disability for which the Company may need to make allowances?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_disability_status" value="1" id="em_disability_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_disability_status" value="0" id="em_disability_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details of such disability and any accommodation that we might need to make to allow you to perform your duties.</p>
    <textarea class="form-control" name="em_disability_status_info" id="em_disability_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">8b. NOTE – Victorian Applicants only to complete –</div>
  <div class="col-sm-12 bgMain">
    <p class="col-sm-12">The nature of the work you will perform for us is</p>
    <div class="col-sm-12">
      <input type="text" class="form-control" name="em_nature_of_work" id="em_nature_of_work">
    </div>
    <p class="col-sm-12 pt-3">Have you any pre-existing injury that may be affected by the nature of the work you perform for us? (Note that failure to notify us of a pre-existing injury which might be affected by your employment with us could result in an injury not being eligible for future compensation claims).</p>
    <div class="col-sm-2">Yes
      <input type="radio" name="em_injury_status" value="1" id="em_injury_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_injury_status" value="0" id="em_injury_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details:</p>
    <textarea class="form-control" name="em_injury_status_info" id="em_injury_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">9a. Have you been convicted of any minor criminal offence in the last five years, or of any major criminal offence in the last ten years?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_criminal_status" value="1" id="em_criminal_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_criminal_status" value="0" id="em_criminal_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details: (a criminal record will not necessarily preclude you from employment, but full disclosure is required).</p>
    <textarea class="form-control" id="em_criminal_status_info" name="em_criminal_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">9b. Do you have any other charges currently pending?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_charges_status" value="1" id="em_charges_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_charges_status" value="0" id="em_charges_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details.</p>
    <textarea class="form-control" name="em_charges_status_info" id="em_charges_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">10. Is there any additional information which you wish to provide which may assist your application for employment?</div>
  <div class="col-sm-12 bgMain">
    <textarea class="form-control" name="em_additionl" id="em_additionl"></textarea>
  </div>
  <div class="col-sm-12 bgMain mt-3 mb-3">
    <h3>NOTE:</h3>
    <p>(a) A three-month probationary period is applicable to all new employees and during this probationary period the employee’s performance will be reviewed on a regular basis.<br>
      <br>
      (b) The Company reserves the right to install and operate video surveillance equipment in the interests of safety, security and protection of employees and assets. Such equipment will not be used with the intention to monitor employees' work performance or to invade privacy.</p>
  </div>
  <p> In accordance with the Privacy Act, we would like to advise you of the following:
  <h4>Collection of Information:</h4>
  We will only use your personal information for the purpose of assessing your application for employment within the company. The information we collect from you will be handled sensitively and securely with proper regard for privacy. If you do not provide some of the personal information we request when you apply for a position within the company, we may not be able to process your application.<br>
  <br>
  We may choose to keep your details on file for a limited period of time to consider you for other job opportunities as they arise. If you would prefer that we do not keep your details on file, please contact us and we will remove them from our records.<br>
  <br>
  <h4>Disclosures:</h4>
  We will not usually disclose your personal information outside of the comnpany, except where certain functions are outsourced to other organisations (such as recruitment consultants), and then only for the purpose of enabling us to process your application. In these circumstances, confidentiality arrangements will apply to restrict the use and disclosure by those organisations who have your personal information disclosed to them.<br>
  <br>
  <h4>Declaration:</h4>
  Your signature below indicates your consent to the use and disclosure of your personal information as indicated above.<br>
  <br>
  I certify and declare that all particulars and information supplied by me in my application for employment are true and correct and I further understand that if I have knowingly or by neglect supplied details that are found to be false this could lead to the termination of my employment.<br>
  <br>
  Further, I hereby give permission for the company to release any medical or other personal records that may relate to my employment with the company, to specialist advisors for the purposes of obtaining specialist advice reasonably related to my employment.
  </p>
  <div class="col-sm-2">SIGNATURE:</div>
  <div class="col-sm-4">
    <input type="text" class="form-control" name="em_signature" id="em_signature">
  </div>
  <div class="col-sm-2">DATE:</div>
  <div class="col-sm-4">
    <input type="text" class="form-control" name="em_date" id="em_date">
  </div>
  <br><br>
  <div class="col-sm-12 titleColor"></div>
  <div class="col-sm-12 bgMain text-center">
    <input type="submit" name="submit" id="submit">
  </div>
  <p class="pb-3">&nbsp;</p>
</form>
</div>
</body>
</html>
<?php } ?>