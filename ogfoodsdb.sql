-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 16, 2018 at 03:01 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ogfoodsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee_applied_for`
--

CREATE TABLE `employee_applied_for` (
  `em_id` int(11) NOT NULL COMMENT 'employee master''s ID',
  `em_job_title` varchar(251) NOT NULL COMMENT 'Job Title , Second Choice (if relevant)',
  `em_position_status` varchar(51) NOT NULL COMMENT 'Status of position applied for',
  `em_limit_availability` varchar(1000) NOT NULL COMMENT 'Are there any circumstances known to you which could affect your ability to undertake either shift or weekend work?',
  `em_monday_detail` varchar(1000) NOT NULL COMMENT 'Please indicate your nonavailability to work below and provide details in the table below',
  `em_tuesday_detail` varchar(1000) NOT NULL COMMENT 'Please indicate your nonavailability to work below and provide details in the table below',
  `em_wednesday_detail` varchar(1000) NOT NULL COMMENT 'Please indicate your nonavailability to work below and provide details in the table below',
  `em_thursday_detail` varchar(1000) NOT NULL COMMENT 'Please indicate your nonavailability to work below and provide details in the table below',
  `em_friday_detail` varchar(1000) NOT NULL COMMENT 'Please indicate your nonavailability to work below and provide details in the table below',
  `em_saturday_detail` varchar(1000) NOT NULL COMMENT 'Please indicate your nonavailability to work below and provide details in the table below',
  `em_sunday_detail` varchar(1000) NOT NULL COMMENT 'Please indicate your nonavailability to work below and provide details in the table below',
  `created_date` datetime NOT NULL COMMENT 'created date and time',
  `updated_date` datetime NOT NULL COMMENT 'updated date and time'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_general_info`
--

CREATE TABLE `employee_general_info` (
  `em_id` int(11) NOT NULL,
  `em_skill` varchar(1000) NOT NULL COMMENT 'Please list any skills, knowledge or experience that you may have which are relevant to the position for which you are applying.',
  `em_hobby` varchar(1000) NOT NULL COMMENT 'What are your hobbies and interests?',
  `em_prep_medical` enum('0','1') NOT NULL COMMENT 'Are you prepared to attend a medical examination by our Doctor, if required?',
  `em_physical_disability_status` varchar(1000) NOT NULL COMMENT 'Do you suffer from any physical or mental disability for which the Company may need to make allowances?',
  `em_nature_of_work` varchar(1000) NOT NULL COMMENT 'The nature of the work you will perform for us is',
  `em_injury_status` varchar(1000) NOT NULL COMMENT 'Have you any pre-existing injury that may be affected by the nature of the work you perform for us? (Note that failure to notify us of a pre-existing injury which might be affected by your employment with us could result in an injury not being eligible for future compensation claims).',
  `em_criminal_status` varchar(1000) NOT NULL COMMENT 'Have you been convicted of any minor criminal offence in the last five years, or of any major criminal offence in the last ten years?',
  `em_charges_status` varchar(1000) NOT NULL COMMENT 'Do you have any other charges currently pending?',
  `em_additional_info` varchar(1000) NOT NULL COMMENT 'Is there any additional information which you wish to provide which may assist your application for employment?',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_master`
--

CREATE TABLE `employee_master` (
  `em_id` int(11) NOT NULL COMMENT 'employee''s unique ID',
  `sh_master_id` int(11) NOT NULL COMMENT 'shop master id',
  `em_family_name` varchar(211) DEFAULT NULL COMMENT 'employee''s family name',
  `em_other_name` varchar(211) DEFAULT NULL COMMENT 'employee''s other name',
  `em_address` varchar(512) DEFAULT NULL COMMENT 'employee''s address',
  `em_postcode` double DEFAULT NULL COMMENT 'postcode of area',
  `em_phone_no` double DEFAULT NULL COMMENT 'employee''s phone number',
  `em_mobile_no` double DEFAULT NULL COMMENT 'employee''s mobile number',
  `em_entitled_austrailia` enum('0','1') DEFAULT NULL COMMENT 'Are you legally entitled to work in Australia?',
  `em_identity_doc` varchar(51) DEFAULT NULL COMMENT 'What identification can you produce to verify this?',
  `em_current_study_status` varchar(10) DEFAULT NULL COMMENT 'Are you currently studying?',
  `em_study_type` varchar(51) DEFAULT NULL COMMENT 'What is your form of study?',
  `em_institute_type` varchar(51) DEFAULT NULL COMMENT 'Which institution do you attend?',
  `em_hs_name` varchar(251) DEFAULT NULL COMMENT 'Name of High School attended',
  `em_hs_grade` varchar(51) DEFAULT NULL COMMENT 'Highest grade/level passed',
  `em_hs_year` varchar(51) DEFAULT NULL COMMENT 'In what year?',
  `em_ps_commenced` varchar(251) DEFAULT NULL COMMENT 'Post-Secondary When commenced',
  `em_ps_year` varchar(251) DEFAULT NULL COMMENT 'Post-Secondary Year completed',
  `em_qualification` varchar(251) DEFAULT NULL COMMENT 'Post-Secondary Qualification/s Obtained',
  `em_signature` varchar(111) DEFAULT NULL COMMENT 'employee''s signature',
  `em_signature_date` varchar(111) DEFAULT NULL COMMENT 'employee''s signature date',
  `created_date` datetime DEFAULT NULL COMMENT 'created_date',
  `updated_date` datetime DEFAULT NULL COMMENT 'updated_date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_master`
--

INSERT INTO `employee_master` (`em_id`, `sh_master_id`, `em_family_name`, `em_other_name`, `em_address`, `em_postcode`, `em_phone_no`, `em_mobile_no`, `em_entitled_austrailia`, `em_identity_doc`, `em_current_study_status`, `em_study_type`, `em_institute_type`, `em_hs_name`, `em_hs_grade`, `em_hs_year`, `em_ps_commenced`, `em_ps_year`, `em_qualification`, `em_signature`, `em_signature_date`, `created_date`, `updated_date`) VALUES
(1, 0, 'zzzzz1', 'zzzzz', 'zzzzzz', 7878787, 78787998, 548997987, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-28 15:55:06', '2018-04-30 10:03:31'),
(2, 0, 'xxxxx11', 'xxxxx', 'xxxxxxxx', 1111111, 111111111, 111111111, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-28 15:55:33', '2018-04-30 10:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `employee_work_history`
--

CREATE TABLE `employee_work_history` (
  `em_id` int(11) NOT NULL COMMENT 'employee master''s ID',
  `em_employer_name` varchar(111) NOT NULL COMMENT 'Employer Name,Tel No / Address',
  `em_from_to` varchar(111) NOT NULL COMMENT 'Work From / To',
  `em_position` varchar(111) NOT NULL COMMENT 'Last Job Position',
  `em_reason` varchar(511) NOT NULL COMMENT 'Reason for Leaving	',
  `em_reference` varchar(111) NOT NULL COMMENT 'Referees Name & Phone',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `fullname` varchar(251) NOT NULL,
  `username` varchar(251) NOT NULL,
  `password` varchar(351) NOT NULL,
  `email` varchar(351) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `fullname`, `username`, `password`, `email`) VALUES
(1, 'admin', 'admin', '123456', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(211) NOT NULL,
  `last_name` varchar(211) NOT NULL,
  `email_address` varchar(211) NOT NULL,
  `password` varchar(20) NOT NULL,
  `user_type` enum('0','1','2','3') NOT NULL,
  `user_status` enum('0','1') NOT NULL,
  `create_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `first_name`, `last_name`, `email_address`, `password`, `user_type`, `user_status`, `create_date`, `modified_date`) VALUES
(1, 'new1', 'new1', 'user@user.com', '123456', '3', '0', '2018-04-28 15:40:36', '2018-04-28 15:53:27'),
(3, 'Ravi', 'Test', 'ravi@gmail.com', '1234562', '1', '0', '2018-04-30 10:02:34', '2018-04-30 10:03:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee_master`
--
ALTER TABLE `employee_master`
  ADD PRIMARY KEY (`em_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee_master`
--
ALTER TABLE `employee_master`
  MODIFY `em_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'employee''s unique ID', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
