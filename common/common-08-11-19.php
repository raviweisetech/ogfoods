<?php
 // CONNECT  to DB ...
function connectdb()    		             
{
	global $hostname;
	global $db_user;
	global $db_password;
	global $databasename;
	$link= mysqli_connect($hostname, $db_user, $db_password,$databasename) or  die("Could not connect: " . mysqli_error($link));
	//mysql_select_db($databasename);
	return $link;
}
// Link to give connection before query
 $link  = connectdb();

//For executing query
function execute_query($query_stmt) 
{
	global $link;
	// execute the query statement
    if(!$result = mysqli_query($link,$query_stmt)) 
	{
		echo "There is some problem with the site connection. Please contact site administrator";
		//DisplayErrMsg(sprintf("Error executing %s statement", $query_stmt));
		//DisplayErrMsg(sprintf("error:%d %s", mysql_errno($link), mysqli_error($link$link)));
		exit();
	}
	return $result;
}

//For getting request parameters
function fnRequestParam($varname)
{	
	global $link;
	if(isset($_REQUEST[$varname]))
	{	
		$varValue = $_REQUEST[$varname];
		if (get_magic_quotes_gpc()) { $varValue = stripslashes($varValue); }  
		$varValue = mysqli_real_escape_string($link,$varValue);		
		return $varValue;
	}
	else
	{
		return NULL;
	}
}
?>