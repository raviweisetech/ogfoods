<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");

$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

$oStore_DA = new clsStore_DA();

/*if(isset($_POST['submit']))
{
	$store_data = $oStore_DA->Store_Create($_POST);

	if($store_data)
	{
		header('Location: Store.php');
	}
	else
	{
		echo "Your data can't insert";
	}
}*/
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<!-- <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<section class="content-header col-md-6"> <h1> Employee Offer letter </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">


	            		<div class="input form-group">
	    		          <label>Select Employee:</label>
	    		          <select class="form-control" name="employee_name" id="employee_name" tabindex="1" required="true">
	    		          	<option>--Select Employee--</option>
	    		           <?php
						  		$result = $oEmp_DA->Emp_Select();
								while($row = mysqli_fetch_assoc($result))
								{
									if($row['employee_status']== "1")
									{
						   ?>
	    		          		<option value="<?php echo $row['em_id']; ?>"><?php echo $row['em_first_name']." ".$row['em_family_name']; ?></option>

	    		          <?php }} ?>
	    		          </select>
	            		</div>

                        <div class="input form-group">
                        	<label>Select Store</label>
                        	<select class="form-control" name="store_name" id="store_name" required="true">
                        		<option>--Select Store--</option>
                        		<?php
                        		$result = $oStore_DA->Get_active_Store();
                        		while($row = mysqli_fetch_assoc($result))
                        		{
                        		?>
                        			<option><?php echo $row['st_name'];?></option>
                        		<?php } ?>
                        	</select>

                        </div>

	            		<div class="form-group">
                         <label>Employee Address:</label>
	    		          <textarea class="form-control" name="address" id="address"></textarea>
	            		</div>
                        <div class="form-group">
                         <label>Joining Date:</label>
	    		          	<input type="text" class="form-control" id="joining_date" name="joining_date" tabindex="3" autocomplete="off" style="width:15%;" value="<?php $date = date('jS F Y '); echo date('Y-m-d', strtotime($date. ' + 7 days')); ?>">
	            		</div>
						<div class="input form-group">
	    		          <label>Position Applied For:</label>
	    		          <select class="form-control" name="position" id="position" tabindex="1" >

                            <option value="Full Time">Full Time</option>
                            <option value="Part Time">Part Time</option>
                            <option value="Casual">Casual</option>
                            <option value="Contract">Contract</option>

	    		          </select>
                       <!--   <input type="text" name="position" id="position" class="form-control">-->
	            		</div>

	            		<div class="form-group" align="center">
	            			<button  class="mylink btn btn-primary" onClick="printCon('printtable');" > Print Offer</button>
	            		</div>
                        <div class="box-body" id="printtable">
						<div class="hiddenn">

              <table >
              	<thead>
                  <tr>


                  </tr>
                </thead>
                <tbody>

					<tr>
						<td rowspan="3" align="left" class="tbl_data_dtl"><label id="emp_name"></label><br><label id="addres"></label>  </td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td class="tbl_data_dtl" align="left"><?php echo date('jS F Y '); ?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td class="tbl_data_dtl" align="left">Dear <label id='emp_name1'></label></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td class="tbl_data_dtl" align="left">LETTER OF OFFER<br></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td colspan="2" align="left" class="tbl_data_dtl">We are delighted to offer you Permanent <label id="appfor"></label> employment with trading as <br>Hungry Jacks Wagga Wagga </td>
					    </tr>


                    <tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td colspan="2" align="left" class="tbl_data_dtl">If you wish to accept the offer, please sign and return the enclosed contract of </td>
					    </tr>
                    <tr>
						<td colspan="2" align="left" class="tbl_data_dtl">of employment (the Contract) by <label id="joindate"></label>.</td>
					    </tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td colspan="2" align="left" class="tbl_data_dtl">Your date of commencement and all other relevant terms are set out in the Contract. <br></td>
					    </tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td class="tbl_data_dtl" align="left">EMPLOYEE HANDBOOK<br></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td colspan="2" align="left" class="tbl_data_dtl">&nbsp;</td>
					    </tr>
                    <tr>
						<td colspan="2" align="left" class="tbl_data_dtl">You will also be provided with a copy of the Hungry Jacks Wagga Wagga employee </td>
					    </tr>

					<tr>
						<td colspan="2" align="left" class="tbl_data_dtl">handbook for your reference. Please return a signed copy of the acknowledgement form </td>
					    </tr>
                    <tr>
						<td colspan="2" align="left" class="tbl_data_dtl">at the end of the handbook together with your Contract.<br></td>
					    </tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>
					</tr>

                    <tr>
						<td colspan="2" align="left" class="tbl_data_dtl">If you have any questions, please feel free to contact me on payroll.hungryjacks@outlook.com<br></td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
                    <tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">Yours sincerely<br></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>

					</tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					</tr>

					</tr>
					<tr>
						<td class="tbl_data_dtl" align="left">Manager Name:_______________</td>
						<td class="tbl_data_dtl" align="left">Employee Name:_______________</td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					    <td class="tbl_data_dtl" align="left">&nbsp;</td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">Manager Sign:_______________</td>
						<td class="tbl_data_dtl" align="left">Employee Sign:_______________</td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
						<td class="tbl_data_dtl" align="left">&nbsp;</td>
					</tr>

					<tr>
						<td class="tbl_data_dtl" align="left">Date:_________________________</td>
						<td class="tbl_data_dtl" align="left">Date:_________________________</td>
					</tr>

            	</tbody>
              </table><div id="footerr" class="hiddenn"></div>
            </div>
            </div>


<?php /*?>	            		<input type="hidden" name="id" id="id" value="<?php echo $emp_detail['em_id']; ?>">
<?php */?>                    <?php /*?>    <input type="hidden" name="st_admin" id="st_admin" value="<?php echo $_SESSION['varUserID']; ?>"><?php */?>
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>
<style>.hiddenn{ visibility:hidden; height:0px;}</style>

  <script type="text/javascript">

  	function printCon(el)
	{
		var month = new Array();
		month[01] = "January";
		month[02] = "February";
		month[03] = "March";
		month[04] = "April";
		month[05] = "May";
		month[06] = "June";
		month[07] = "July";
		month[08] = "August";
		month[09] = "September";
		month[10] = "October";
		month[11] = "November";
		month[12] = "December";
		$('#joining_date').datepicker({ dateFormat: 'dd-mm-yy' });
		var datte = document.getElementById('joining_date').value;
		var days = datte.split('-');
		var mm = parseInt(days[1]);
		$('#joindate').text(days[2]+'th '+month[mm]+'  '+days[0]);

		var restorepage = document.body.innerHTML;
		var printcon = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcon;
		window.print();
		document.body.innerHTML = restorepage;
		window.location.reload();

	}

  	$(document).ready(function(){
		$('#joining_date').datepicker({ format: 'yyyy-mm-dd' });
	/*	function printCon(el)
	{

		var restorepage = document.body.innerHTML;
		var printcon = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcon;
		window.print();
		//document.body.innerHTML = restorepage;
		window.location.reload();
	}*/
		$('#position').on('change',function(){
			$('#appfor').text($("#position option:selected").text());
			});

		$('#employee_name').on('change',function(){

        	var empl_ID = $(this).val();
			var dataString = "em_id=" + empl_ID;
        	if(empl_ID){
				$.ajax({
				type: 'POST',
				url: 'ajaxEmployee.php',
				dataType: 'json',
				data: dataString,
				success: function (result) {
					document.getElementById("address").value = result.em_address+', '+result.em_postcode;
					 $('#addres').text( result.em_address+', '+result.em_postcode);
					 $('#emp_name').text( result.em_first_name+' '+result.em_family_name);
					  $('#emp_name1').text( result.em_first_name+' '+result.em_family_name);
					document.getElementById("position").selectedIndex = result.position_for;
					$('#appfor').text($("#position option:selected").text());
					 $('#str_name').text( result.str_name );
				//	alert(JSON.stringify(result));
				},error: function (result) {
						alert('error; ' + eval(result));
					}
				});

        	}else{
            	document.getElementById("address").value = "Select Employee first";
        	}
    	});
	});
  </script>
  <script type="text/javascript">
        datepicker( '.datepicker' );

        </script>
<?php include('footer.php'); ?>
<?php ob_flush();?>
