<?php
ob_start();
session_start();
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
require_once("include/clsInclude.php");
$oStore_DA = new clsStore_DA();

if(fnRequestParam('mode') == 'ss') {
	$stid = fnRequestParam('stid');
	$ss = fnRequestParam('ss');
	$oStore_DA->Store_Status($stid, $ss);
	header("Location:Store.php?msg=ss");
	exit;
}

if(fnRequestParam('mode') == 'Delete') {
	$stid = fnRequestParam('id');
	$oStore_DA->Store_Delete($stid);
	header("Location:Store.php?msg=Del");
	exit;	
}
$is_store = 'yes';
include('header.php');
?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Manage Store </h1> </section>
<section class="col-md-6 center" align="right" style="margin-top: 10px"><a href="StoreCreate.php"><button class="btn-primary" >Create Store</button></a></section>
</div>
<br><br>
<section class="content">
	<form name="frmManageUser" method="post" enctype="multipart/form-data">
  	<div class="row">
  		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Store</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">Store Name</th>
                    <th align="left" class="tbl_design1">Email Address</th>
					<th align="left" class="tbl_design1">Store Admin</th>
					<th align="left" class="tbl_design1">Store Country</th>
                    <th align="left" class="tbl_design1">Store State</th>
					<th align="left" class="tbl_design1">Store City</th>
					<th align="left" class="tbl_design1">Store Owner</th>
					<th align="left" class="tbl_design1">Store Status</th>
					<th align="left" class="tbl_design1">Edit</th>
					<th align="left" class="tbl_design1">Delete</th>
                  </tr>
                </thead>
                <tbody>
                	<?php 
					
						$results = $oStore_DA->All_Store();
				
				
			  while($row = mysqli_fetch_assoc($results))
				 {
					 /*$result = $oStore_DA->Select_Store_User($_SESSION['varUserID']);
	            	 while($rows = mysqli_fetch_assoc($result))
	            		 {
							 if($row['id'] == $rows['fk_store_id'])
							 { */?>
	                		<tr>
	                			<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['st_name'];?></td>
	                			<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['store_email_address'];?></td>
								<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['st_address'];?></td>
								<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oStore_DA->getCountry($row['st_country']);?></td>
								<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oStore_DA->getState($row['st_state']);?></td>
								<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oStore_DA->getCity($row['st_city']);?></td>
								<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oStore_DA->getStoreOwner($row['st_owner']);?></td>
								<td class="tbl_data_dtl" align="center">&nbsp;
								<select name="changestatus" onchange="return change_status(<?php echo $row['id']; ?>,this.value)">
								   <option value="1" <?php if($row['st_status'] =='1') echo "selected='selected'";?>>Active</option>
								   <option value="0" <?php if($row['st_status'] =='0') echo "selected='selected'";?>>Deactive</option>
								</select>
					    		</td>
								<td class="tbl_data_dtl" align="center"><a href="StoreEdit.php?id=<?php echo $row['id'];?>" class="mylink">Edit</a></td>
					    		<td class="tbl_data_dtl" align="center"><a href="Store.php?id=<?php echo $row['id'];?>&amp;mode=Delete" class="mylink">Delete</a></td>
	                		</tr>
	                	<?php /*}}*/
						 }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>

<script language="javascript">

function change_status(stid,st_status)
{
	window.location.href="Store.php?mode=ss&stid="+stid+"&ss="+st_status+"";
}
</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>