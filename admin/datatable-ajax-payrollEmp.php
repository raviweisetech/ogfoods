<?php

ob_start();
session_start();

require_once("include/clsInclude.php");

$requestData= $_REQUEST;

if($requestData['ms'] == '0')
{
	$filterQuery = '';
}
else
{
	$empid = '';
	if($requestData['empid'] != '')
	{
		$empid = "and fk_employee_id = '".$requestData['empid']."' ";
	}

	$to_date = strtotime($requestData['ms']);
	$endDate = $requestData['endDate'];

	$filterQuery = " where fk_store_id = '".$requestData['storeid']."' and week_start_date >= '".$requestData['ms']."' and week_end_date <= '".$endDate."' and `payroll_paid` = '0' ".$empid."";
}

$sql_total = "SELECT * from tbl_employee_timesheet_payroll where 1=1"; 
$res_total = execute_query($sql_total,$link) or die(mysqli_error($link));
$totalData = mysqli_num_rows($res_total);
$totalFiltered = $totalData;

$data = array();

$sql = "SELECT * from tbl_employee_timesheet_payroll ".$filterQuery;
//echo $sql;exit;
if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
	$sql_user = " SELECT GROUP_CONCAT(`em_id`) FROM `employee_master` WHERE ( `em_family_name` LIKE '%".$requestData['search']['value']."%' OR `em_first_name` LIKE '%".$requestData['search']['value']."%')";
	
	$sql_user_id = execute_query($sql_user,$link) or die(mysqli_error($link));
	$userids = mysqli_fetch_array($sql_user_id);
	$sql.=" AND fk_employee_id IN (".$userids['GROUP_CONCAT(`em_id`)'].") ";
	
} 
$res = execute_query($sql,$link) or die(mysqli_error($link));
$totalFiltered = mysqli_num_rows($res);


$data = array();
$before6am_total_hours = 0;
$after10pm_total_hours = 0;
$saturday_hours = 0;
$sunday_hours = 0;
$normal_total_hours = 0;
$weekly_total_hours = 0;
$public_holiday_hours = 0;
$sick_leave_hours = 0;
$annual_leave_hours = 0;

while($row=mysqli_fetch_array($res))
{	
	$user_name = get_emp_name($row['fk_employee_id']);
	
	$nestedData=array(); 
	$nestedData[] = $user_name['em_first_name'].' '.$user_name['em_family_name']; ;
	$nestedData[] = $row["before6am_total_hours"]; $before6am_total_hours = $before6am_total_hours + $row['before6am_total_hours'];
	$nestedData[] = $row["after10pm_total_hours"]; $after10pm_total_hours = $after10pm_total_hours + $row['after10pm_total_hours'];
	$nestedData[] = $row["saturday_hours"]; $saturday_hours = $saturday_hours + $row['saturday_hours'];
	$nestedData[] = $row["sunday_hours"]; $sunday_hours = $sunday_hours + $row['sunday_hours'];
	$nestedData[] = $row["public_holiday_hours"]; $public_holiday_hours = $public_holiday_hours + $row['public_holiday_hours'];	
	$nestedData[] = $row["sick_leave_hours"]; $sick_leave_hours = $sick_leave_hours + $row['sick_leave_hours'];	
	$nestedData[] = $row["annual_leave_hours"]; $annual_leave_hours = $annual_leave_hours + $row['annual_leave_hours'];	

	$nestedData[] = $row["normal_total_hours"]; $normal_total_hours = $normal_total_hours + $row['normal_total_hours'];
	$nestedData[] = $row["weekly_total_hours"]; $weekly_total_hours = $weekly_total_hours + $row['weekly_total_hours'];
	$nestedData[] = $row["number_of_shifts"];
	
	$data[] = $nestedData;
}
	$totalValue[]= "Total";
	$totalValue[]= $before6am_total_hours;
	$totalValue[]= $after10pm_total_hours;
	$totalValue[]= $saturday_hours;		
	$totalValue[]= $sunday_hours;
	$totalValue[]= $public_holiday_hours;
	$totalValue[]= $sick_leave_hours;
	$totalValue[]= $annual_leave_hours;
	$totalValue[]= $normal_total_hours;
	$totalValue[]= $weekly_total_hours;
	$totalValue[]= '-';	
	$data[] = $totalValue;
	
		
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalData),
			"recordsFiltered" => intval( $totalFiltered),
			"data"            => $data
			);

echo json_encode($json_data);

?>