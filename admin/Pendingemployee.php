<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

/*Delete user Data start*/
if(fnRequestParam('mode') == 'Delete') {
	$oEmp_CDO->token = fnRequestParam('token');
	$oEmp_DA->Pending_Emp_Delete($oEmp_CDO);
	header("Location:Pendingemployee.php?msg=Del");
	exit;	
}
if(fnRequestParam('mode') == 'es') {
	$oEmp_CDO->id = fnRequestParam('eid');
	$oEmp_CDO->us = fnRequestParam('ems');
	$oEmp_DA->Emp_Status($oEmp_CDO);
	header("Location:Pendingemployee.php?id=$oEmp_CDO->id");
	exit;
}
include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Pending Employees </h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
		<a href="Employeement.php" style="margin-right: 10px"><button class="btn-primary" >New employment</button></a>
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employees</h3>          
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                  	<th align="left" class="tbl_design1">Full Name</th>
                    <th align="left" class="tbl_design1">Email Address</th>
					<th align="left" class="tbl_design1">View Full Detail</th>
					<th align="left" class="tbl_design1">Delete</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                	//print_r(mysqli_fetch_assoc($oEmp_DA->Emp_Select()));die;
                	$result = $oEmp_DA->Emp_Select_Detail();
                	while($row = mysqli_fetch_assoc($result))
                	{ 
						$color = '';
						$check_form = 0;
						$check_form = $oEmp_DA->Submit_Form($row['form_access_token']);
						if($check_form == 1)
						{
							$details_emp = 'Employeedetails.php?token='.$row['form_access_token'];
								$color = '#50c878';
						}else{ $color == ''; $details_emp = '#';}
						
					?>
                		<tr style="background-color:<?php echo $color; ?>";>
                		<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['fullname'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['email'];?></td>
					   
					    <td class="tbl_data_dtl" align="center"><a href="<?php echo $details_emp;?>" class="mylink">View Full Detail</a></td>
					    <td class="tbl_data_dtl" align="center"><a href="Pendingemployee.php?token=<?php echo $row['form_access_token'];?>&amp;mode=Delete" class="mylink" onClick="if (! confirm('Are you sure want to delete this employee?')) { return false; }">Delete</a></td>
						</tr>
             <?php }  ?>
         		</tbody>
         	</table>
         </div>
     </div>
 </div>
</div>
</section>
<script language="javascript">
function change_status(eid,empstatus)
{
		
		window.location.href="Pendingemployee.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}
</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>