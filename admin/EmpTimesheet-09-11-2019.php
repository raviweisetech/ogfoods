<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

$date = new DateTime();
$week_strt_day = constant("week_start_day");
$yy = date('l');
if($yy == $week_strt_day ){
	$to_date = strtotime(date('Y-m-d'));
	$date1 = date('Y-m-d', strtotime("today",$to_date)); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));
}else{
	$date1 = date('Y-m-d', strtotime("last ".constant("week_start_day")));
	$to_date = strtotime($date1); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));	
}

$result = $oEmp_DA->Emp_Timesheet($_REQUEST['EmpId'] , $date1 , $date7);
$result_payroll = $oEmp_DA->Emp_Timesheet_payroll($_REQUEST['EmpId'] , $date1 , $date7);
$row_payroll = mysqli_fetch_array($result_payroll);
$i = 1;
//print_r($row_payroll);
while($row = mysqli_fetch_assoc($result))
{	
	$dates[$row['timesheet_date']] =  $row;
	$i++;
}
// print_r($dates["2019-11-05"]['timesheet_date']);


if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] == 'Submit')
{	
    $Emp_Create_timesheet = $oEmp_DA->Emp_Create_timesheet($_POST);

	
		header('Location:EmpTimesheet.php?EmpId='.$_REQUEST['EmpId']);	
	
	
}
if(isset($_REQUEST['cancel']) && $_REQUEST['cancel'] == 'cancel')
{	
   
		header("location:home.php");
	
}

include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet</h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
		
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<style>
.timesheet-table{
	margin: 5px;
    width: 83px;
}
.col-sm-12 {
    width: 100%;
    padding-bottom: 1%;
}
</style>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
		
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Timesheet</h3><?php echo constant("week_start_day");?>         
            </div>
              <form role="form" method="POST">
		            <div class="box-body">
		            		
                            <div class="col-sm-12">
                                <div class="col-sm-1">Date</div>
                                <div class="col-sm-1">rostered_time</div>
                                <div class="col-sm-1">Ro_time_end</div>
                                <div class="col-sm-1">actual_time</div>


                                <div class="col-sm-1">Act_end</div>
                                <div class="col-sm-1">Meal_brk</div>
                                <div class="col-sm-1">Meal_brk_end </div>
                                <div class="col-sm-1"> before6am</div>
                                <div class="col-sm-1"> after10pm</div>
                                <div class="col-sm-1"> normalHour</div>
                                <div class="col-sm-1"> totalHour</div>
                              </div>
                    	
                    		
                    
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker1" name="datepicker1" value="<?php echo $date1; ?>" readonly/></div>
                                <div class="col-sm-1">
                                 <select class="timesheet-table" id="rostered_time1" name="rostered_time1" onchange="return removeDate(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled";} ?>>
                                 <option >Start Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) { 
													if( array_key_exists($date1,$dates) && $dates[$date1]['rostered_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="rostered_time_end1" name="rostered_time_end1" onchange="return removeDateEnd(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                    	 <?php
                                         	$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date1,$dates) && $dates[$date1]['rostered_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time1" name="actual_time1" onchange="return removeDateactual(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled";} ?>>
                                	<option >Start Date</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date1,$dates) && $dates[$date1]['actual_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                               		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time_end1" name="actual_time_end1" onchange="return actualendtime(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date1,$dates) && $dates[$date1]['actual_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                               		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time1" name="meal_break_time1" onchange="return mealStart(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date1,$dates) && $dates[$date1]['meal_break_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time_end1" name="meal_break_time_end1" onchange="return calculation(1)" <?php if(array_key_exists($date1,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date1,$dates) && $dates[$date1]['meal_break_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="before6am1" name="before6am1" value="<?php if($dates[$date1]['before_6am']){ echo $dates[$date1]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="after10pm1" name="after10pm1" value="<?php if($dates[$date1]['after_10pm']){ echo $dates[$date1]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour1" name="normalHour1" value="<?php if($dates[$date1]['normal_hours']){ echo $dates[$date1]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour1" name="totalHour1" value="<?php if($dates[$date1]['total_hours']){ echo $dates[$date1]['total_hours']; }else { echo "0";} ?>" readonly /></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker2" name="datepicker2" value="<?php  echo $date2; ?>"readonly/></div>
                                <div class="col-sm-1"> 								
                                <select id="rostered_time2" name="rostered_time2" class="timesheet-table" onchange="return removeDate(2);" <?php if(array_key_exists($date2,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date2,$dates) && $dates[$date1]['rostered_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                 <div class="col-sm-1"> 								
                                <select id="rostered_time_end2" name="rostered_time_end2" class="timesheet-table" onchange="return removeDateEnd(2);" <?php if(array_key_exists($date2,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date2,$dates) && $dates[$date1]['rostered_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time2" name="actual_time2" onchange="return removeDateactual(2);"  <?php if(array_key_exists($date2,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date2,$dates) && $dates[$date1]['actual_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                          </select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="actual_time_end2" name="actual_time_end2" onchange="return actualendtime(2);"  <?php if(array_key_exists($date2,$dates)){ echo "disabled";} ?>>
                                 <option >End Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date2,$dates) && $dates[$date1]['actual_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                          </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time2" name="meal_break_time2" onchange="return mealStart(2);"  <?php if(array_key_exists($date2,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date2,$dates) && $dates[$date1]['meal_break_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                </select></div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time_end2" name="meal_break_time_end2" onchange="return calculation(2)"  <?php if(array_key_exists($date2,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if( array_key_exists($date2,$dates) && $dates[$date1]['meal_break_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                </select></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="before6am2" name="before6am2" value="<?php if($dates[$date2]['before_6am']){ echo $dates[$date2]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="after10pm2" name="after10pm2" value="<?php if($dates[$date2]['after_10pm']){ echo $dates[$date2]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour2" name="normalHour2" value="<?php if($dates[$date2]['normal_hours']){ echo $dates[$date2]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour2" name="totalHour2" value="<?php if($dates[$date2]['total_hours']){ echo $dates[$date2]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker3" name="datepicker3" value="<?php echo $date3;  ?>" readonly/></div>
                                <div class="col-sm-1"> <select id="rostered_time3" name="rostered_time3" class="timesheet-table" onchange="return removeDate(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date3,$dates) && $dates[$date3]['rostered_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                  <div class="col-sm-1"> <select id="rostered_time_end3" name="rostered_time_end3" class="timesheet-table" onchange="return removeDateEnd(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled";} ?>>
                                  <option >End Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date3,$dates) && $dates[$date3]['rostered_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time3" name="actual_time3" onchange="return removeDateactual(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                				<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date3,$dates) && $dates[$date3]['actual_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time_end3" name="actual_time_end3" onchange="return actualendtime(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                				<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date3,$dates) && $dates[$date3]['actual_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time3" name="meal_break_time3" onchange="return mealStart(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date3,$dates) && $dates[$date3]['meal_break_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time_end3" name="meal_break_time_end3" onchange="return calculation(3)" <?php if(array_key_exists($date3,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date3,$dates) && $dates[$date3]['meal_break_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="before6am3" name="before6am3" value="<?php if($dates[$date3]['before_6am']){ echo $dates[$date3]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="after10pm3" name="after10pm3" value="<?php if($dates[$date3]['after_10pm']){ echo $dates[$date3]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour3" name="normalHour3" value="<?php if($dates[$date3]['normal_hours']){ echo $dates[$date3]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour3" name="totalHour3" value="<?php if($dates[$date3]['total_hours']){ echo $dates[$date3]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker4" name="datepicker4" value="<?php echo $date4; ?>" readonly/></div>
                                <div class="col-sm-1"> <select id="rostered_time4" name="rostered_time4" class="timesheet-table" onchange="return removeDate(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date4,$dates) && $dates[$date4]['rostered_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="rostered_time_end4" name="rostered_time_end4" class="timesheet-table" onchange="return removeDateEnd(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date4,$dates) && $dates[$date4]['rostered_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time4" name="actual_time4" onchange="return removeDateactual(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date4,$dates) && $dates[$date4]['actual_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time_end4" name="actual_time_end4" onchange="return actualendtime(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date4,$dates) && $dates[$date4]['actual_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time4" name="meal_break_time4" onchange="return mealStart(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date4,$dates) && $dates[$date4]['meal_break_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                        </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time_end4" name="meal_break_time_end4" onchange="return calculation(4)" <?php if(array_key_exists($date4,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date4,$dates) && $dates[$date4]['meal_break_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                        </select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="before6am4" name="before6am4" value="<?php if($dates[$date4]['before_6am']){ echo $dates[$date4]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="after10pm4" name="after10pm4" value="<?php if($dates[$date4]['after_10pm']){ echo $dates[$date4]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour4" name="normalHour4" value="<?php if($dates[$date4]['normal_hours']){ echo $dates[$date4]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour4" name="totalHour4" value="<?php if($dates[$date4]['total_hours']){ echo $dates[$date4]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker5" name="datepicker5" value="<?php echo $date5;  ?>" readonly/></div>
                                <div class="col-sm-1"> <select id="rostered_time5" name="rostered_time5" class="timesheet-table" onchange="return removeDate(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date5,$dates) && $dates[$date5]['rostered_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="rostered_time_end5" name="rostered_time_end5" class="timesheet-table" onchange="return removeDateEnd(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date5,$dates) && $dates[$date5]['rostered_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time5" name="actual_time5" onchange="return removeDateactual(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date5,$dates) && $dates[$date5]['actual_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>		
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time_end5" name="actual_time_end5" onchange="return actualendtime(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date5,$dates) && $dates[$date5]['actual_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>		
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time5" name="meal_break_time5" onchange="return mealStart(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date5,$dates) && $dates[$date5]['meal_break_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time_end5" name="meal_break_time_end5" onchange="return calculation(5)" <?php if(array_key_exists($date5,$dates)){ echo "disabled";} ?>>
                                 <option >End Date</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date5,$dates) && $dates[$date5]['meal_break_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="before6am5" name="before6am5" value="<?php if($dates[$date5]['before_6am']){ echo $dates[$date5]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="after10pm5" name="after10pm5" value="<?php if($dates[$date5]['after_10pm']){ echo $dates[$date5]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour5" name="normalHour5" value="<?php if($dates[$date5]['normal_hours']){ echo $dates[$date5]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour5" name="totalHour5" value="<?php if($dates[$date5]['total_hours']){ echo $dates[$date5]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker6" name="datepicker6" value="<?php echo $date6; ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                <select id="rostered_time6" name="rostered_time6" class="timesheet-table" onchange="return removeDate(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date6,$dates) && $dates[$date6]['rostered_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="rostered_time_end6" name="rostered_time_end6" class="timesheet-table" onchange="return removeDateEnd(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date6,$dates) && $dates[$date6]['rostered_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time6" name="actual_time6" onchange="return removeDateactual(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date6,$dates) && $dates[$date6]['actual_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="actual_time_end6" name="actual_time_end6" onchange="return actualendtime(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled";} ?>>
                                 <option >End Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date6,$dates) && $dates[$date6]['actual_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time6" name="meal_break_time6" onchange="return mealStart(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date6,$dates) && $dates[$date6]['meal_break_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time_end6" name="meal_break_time_end6" onchange="return calculation(6)" <?php if(array_key_exists($date6,$dates)){ echo "disabled";} ?>>
                                 <option >End Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date6,$dates) && $dates[$date6]['meal_break_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="before6am6" name="before6am6" value="<?php if($dates[$date6]['before_6am']){ echo $dates[$date6]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="after10pm6" name="after10pm6" value="<?php if($dates[$date6]['after_10pm']){ echo $dates[$date6]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour6" name="normalHour6" value="<?php if($dates[$date6]['normal_hours']){ echo $dates[$date6]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour6" name="totalHour6" value="<?php if($dates[$date6]['total_hours']){ echo $dates[$date6]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker7" name="datepicker7" value="<?php  echo $date7; ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                <select id="rostered_time7" name="rostered_time7" class="timesheet-table" onchange="return removeDate(7);" <?php if(array_key_exists($date7,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date7,$dates) && $dates[$date7]['rostered_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="rostered_time_end7" name="rostered_time_end7" onchange="return removeDateEnd(7);" class="timesheet-table" <?php if(array_key_exists($date7,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date7,$dates) && $dates[$date7]['rostered_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="actual_time7" name="actual_time7" onchange="return removeDateactual(7);" <?php if(array_key_exists($date7,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date7,$dates) && $dates[$date7]['actual_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="actual_time_end7" name="actual_time_end7" onchange="return actualendtime(7);" <?php if(array_key_exists($date7,$dates)){ echo "disabled";} ?>>
                                 <option >End Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date7,$dates) && $dates[$date7]['actual_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time7" name="meal_break_time7" onchange="return mealStart(7);" <?php if(array_key_exists($date7,$dates)){ echo "disabled";} ?>>
                                <option >Start Date</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date7,$dates) && $dates[$date7]['meal_break_time'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="meal_break_time_end7" name="meal_break_time_end7" onchange="return calculation(7)" <?php if(array_key_exists($date7,$dates)){ echo "disabled";} ?>>
                                <option >End Date</option>
                                		<?php
											$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													if(array_key_exists($date7,$dates) && $dates[$date7]['meal_break_time_end'] == $now->format('g:i a')){
														echo "<option value='".$now->format('g:i a')."' Selected>" . $now->format('g:i a'). "</option>";
													}else{
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													}
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="before6am7" name="before6am7" value="<?php if($dates[$date7]['before_6am']){ echo $dates[$date7]['before_6am'];}else { echo "0";} ?>" readonly /></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="after10pm7" name="after10pm7" value="<?php if($dates[$date7]['after_10pm']){ echo $dates[$date7]['after_10pm'];}else { echo "0";} ?>" readonly /></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour7" name="normalHour7" value="<?php if($dates[$date7]['normal_hours']){ echo $dates[$date7]['normal_hours'];}else { echo "0";} ?>" readonly /></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour7" name="totalHour7" value="<?php if($dates[$date7]['total_hours']){ echo $dates[$date7]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                              </div>
                             
                              <div class="col-sm-12"><hr>
                               <button type="submit" class="btn-primary" name="cancel" value="cancel">cancel</button>
                                <button type="submit" class="btn-primary" name="Submit" value="Submit"onclick="return confirm('Are you sure you want to submit?');">Submit</button>
                              </div>
                              <input type="hidden" value="<?php echo $_REQUEST['EmpId']; ?>" name="fk_employee_id" id="fk_employee_id"/>
				<table width="100%" border="1" class="table table-bordered table-striped dataTable no-footer">
                	<thead>
                    	<th>Store id</th>
                        <th>Emp id</th>
                        <th>Week Start</th>
                        <th>Week End</th>
                        <th>Before 6am</th>
                        <th>After 10pm</th>
                        <th>Saturday Hours</th>
                        <th>Sunday Hours</th>
                        <th>Normal Total Hours</th>
                        <th>Weekly Total Hours</th>
                        <th>Number Of Shifts</th>
                    </thead>
                    <tr>
                    	<td><?php echo $row_payroll['fk_store_id']; ?></td>
                        <td><?php echo $row_payroll['fk_employee_id']; ?></td>
                        <td><?php echo $row_payroll['week_start_date']; ?></td>
                        <td><?php echo $row_payroll['week_end_date']; ?></td>
                        <td><?php echo $row_payroll['before6am_total_hours']; ?></td>
                        <td><?php echo $row_payroll['after10pm_total_hours']; ?></td>
                        <td><?php echo $row_payroll['saturday_hours']; ?></td>
                        <td><?php echo $row_payroll['sunday_hours']; ?></td>
                        <td><?php echo $row_payroll['normal_total_hours']; ?></td>
                        <td><?php echo $row_payroll['weekly_total_hours']; ?></td>
                        <td><?php echo $row_payroll['number_of_shifts']; ?></td>
                    </tr>
                </table>                    
		            </div>
		        </form>         
               
     </div>
 </div>
</div>
</section>
<script language="javascript">
 $(function() {});
  
	/*$("#rostered_time_end1").val('12:15 am');*/
	for(i = 1 ;i<8; i++){
		$("#rostered_time_end"+i).attr('disabled', true);
		$("#actual_time_end"+i).attr('disabled', true);
		$("#meal_break_time"+i).attr('disabled', true); 
		$("#meal_break_time_end"+i).attr('disabled', true); 
	}
	

function mealStart(postion){
	$("#meal_break_time"+postion).attr('disabled', true); 
	$("#meal_break_time_end"+postion).attr('disabled', false); 
}
function removeDateEnd(postion){
	$("#rostered_time_end"+postion).attr('disabled', true); 
}

function removeDate(position){
	
	$("#rostered_time"+position).attr('disabled', true); 
	
	 var rt1 = $("#rostered_time"+position).prop('selectedIndex');
	 var rte1 = document.getElementById("rostered_time_end"+position);
	 var i = rt1;
	 
	 if(rt1 > 0){
	 	$("#rostered_time_end"+position).attr('disabled', false); 
	 }
	 
	 for( i = rt1 ; i >= 0 ; i--){
		 //alert(x.options[i].value);
		 document.getElementById("rostered_time_end"+position).options[i].disabled = true;
	 	//x.remove(i);
	 }
	 $("#rostered_time_end"+position).val(rte1.options[rt1+1].value);
}

function befor6am(startt , endt){
	
	var befor6am = 0;
	 if(startt < 25 && endt <= 25){
	 		befor6am =  ( endt - startt )/4;
	 }else if(startt < 25 && endt > 25){
	 		befor6am = (25 - startt)/4;	
	 }	
	 
	return befor6am;
	
}

function after10pm(startt , endt){
	
	var after10pm = 0;
	//alert(startt + '-' + endt)
	 if(startt >= 89 && endt > 89){
	 		after10pm =  ( endt - startt )/4;
	 }else if(startt < 89 && endt > 89){
	 		after10pm = (endt - 89)/4;	
	 }	
	return after10pm;
		
}


function normalHour(startts , endtt , mstart , mend , ii , aftr){
	
	
	var normalHour=0;	
	var diffbeforafter = 0
	
	var diff = (endtt - startts)/4 ;
	
	var milbreake = (mend - mstart)/4;
	
	var bforAftr = Number(document.getElementById("before6am"+ii).value) + Number(aftr);
	
	diffbeforafter = Number(diff) - Number(bforAftr) ; 
	
	normalHour = Number(diffbeforafter) - Number(milbreake) ;
	
	if(normalHour < 0 ){normalHour = 0;}
	
	return normalHour;
}

function totalHour(normalHour , befor , after ){
	var totalHour = 0;
	
	totalHour = Number(normalHour) + Number(befor) + Number(after);
	
	return totalHour;

}

function rosteedFocuss(){
	if($("#meal_break_time1").prop('selectedIndex') < $("#actual_time1").prop('selectedIndex') || $("#meal_break_time_end1").prop('selectedIndex') > $("#actual_time_end1").prop('selectedIndex'))
	{
		document.getElementById("meal_break_time1").focus();
		return false;
	}
}

function actualendtime(posiion){
	
	$("#actual_time_end"+posiion).attr('disabled', true); 
	
	document.getElementById("before6am"+posiion).value = befor6am( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') );
	
	document.getElementById("after10pm"+posiion).value = after10pm( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') );
	
	document.getElementById("normalHour"+posiion).value = normalHour( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') , $("#meal_break_time"+posiion).prop('selectedIndex') , $("#meal_break_time_end"+posiion).prop('selectedIndex'),posiion,document.getElementById("after10pm"+posiion).value);
	
	document.getElementById("totalHour"+posiion).value = totalHour(document.getElementById("normalHour"+posiion).value , document.getElementById("after10pm"+posiion).value , document.getElementById("before6am"+posiion).value);
	
	$("#meal_break_time"+posiion).attr('disabled', false); 
	
	
	 var at1 = $("#actual_time_end"+posiion).prop('selectedIndex');
	  var i = at1;
	 for( i = at1 ; i <= 97 ; i++){
		 document.getElementById("meal_break_time_end"+posiion).options[i].disabled = true;
		 document.getElementById("meal_break_time"+posiion).options[i].disabled = true;
	 	//x.remove(i);
	 }
	 
	
}

function calculation(posiion){
	$("#meal_break_time_end"+posiion).attr('disabled', true); 
	
	document.getElementById("normalHour"+posiion).value = normalHour( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') , $("#meal_break_time"+posiion).prop('selectedIndex') , $("#meal_break_time_end"+posiion).prop('selectedIndex'),posiion,document.getElementById("after10pm"+posiion).value);
	
//	document.getElementById("totalHour"+posiion).value = totalHour(document.getElementById("normalHour"+posiion).value , document.getElementById("after10pm"+posiion).value , document.getElementById("before6am"+posiion).value);
	
	var mealHour = ($("#meal_break_time_end"+posiion).prop('selectedIndex') - $("#meal_break_time"+posiion).prop('selectedIndex'))/4;
	
	if($("#actual_time_end"+posiion).prop('selectedIndex') > 25){
	document.getElementById("totalHour"+posiion).value = Number(document.getElementById("before6am"+posiion).value) + Number(document.getElementById("after10pm"+posiion).value) + Number(document.getElementById("normalHour"+posiion).value) ;  
	}else{
		document.getElementById("totalHour"+posiion).value = Number(document.getElementById("before6am"+posiion).value) - Number(mealHour) ; 
	}
	
}

function removeDateactual(positin){
	
	$("#actual_time"+positin).attr('disabled', true); 
	$("#actual_time_end"+positin).attr('disabled', false); 
	
	 var at1 = $("#actual_time"+positin).prop('selectedIndex');
	 var ate1 = document.getElementById("actual_time_end"+positin);
	 var i = at1;
	 
	 for( i = at1 ; i >= 0 ; i--){
		 //alert(x.options[i].value);
		 document.getElementById("actual_time_end"+positin).options[i].disabled = true;
		 document.getElementById("meal_break_time"+positin).options[i].disabled = true;
		 document.getElementById("meal_break_time_end"+positin).options[i].disabled = true;
	 	//x.remove(i);
	 }
	 $("#actual_time_end"+positin).val(ate1.options[at1+1].value);
	 $("#meal_break_time"+positin).val(ate1.options[at1+1].value);
	 $("#meal_break_time_end"+positin).val(ate1.options[at1+1].value);
	 
	 document.getElementById("before6am"+positin).value = befor6am( $("#actual_time"+positin).prop('selectedIndex') , $("#actual_time_end"+positin).prop('selectedIndex') );

	document.getElementById("after10pm"+positin).value = after10pm( $("#actual_time"+positin).prop('selectedIndex') , $("#actual_time_end"+positin).prop('selectedIndex') );

}
function checkTime(){
	if($("#actual_time1").prop('selectedIndex') > $("#actual_time1").prop('selectedIndex')){
		alert();
	}
	return false;
}
jQuery(function ($) {        
  $('form').bind('submit', function () {
    $(this).find(':input').prop('disabled', false);
  });
});

</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>