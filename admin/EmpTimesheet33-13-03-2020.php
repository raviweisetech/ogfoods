<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

$date = new DateTime();
$week_strt_day = constant("week_start_day");
$yy = date('l');

$current_date = date('Y-m-d');
$nextdaytime = strtotime(date('Y-m-d'));
$nextday = date('Y-m-d', strtotime("-1 day", $nextdaytime));

if($yy == $week_strt_day ){
	$to_date = strtotime(date('Y-m-d'));
	$date1 = date('Y-m-d', strtotime("today",$to_date)); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));
}else{
	$date1 = date('Y-m-d', strtotime("last ".constant("week_start_day")));
	//$date1 = date('Y-m-d', strtotime("last sat"));
	$to_date = strtotime($date1); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));	
}
$result = $oEmp_DA->Emp_Timesheet($_REQUEST['EmpId'] , $date1 , $date7);
$result_payroll = $oEmp_DA->Emp_Timesheet_payroll($_REQUEST['EmpId'] , $date1 , $date7);
$row_payroll = mysqli_fetch_array($result_payroll);
$i = 1;
//print_r($row_payroll);
while($row = mysqli_fetch_assoc($result))
{	
	$dates[$row['timesheet_date']] =  $row;
	$i++;
}
// print_r($dates["2019-11-05"]['timesheet_date']);


if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] == 'Submit')
{	
//	print_r($_POST);exit;
	if($_POST['fk_employee_id'] == '--Select Employee--' || $_POST['fk_employee_id'] == NULL){
		echo "<script type='text/javascript'>alert('Please Select Employee First...');</script>";
	}else{
		
		$Emp_Create_timesheet = $oEmp_DA->Emp_Create_timesheet($_POST);
		
		header('Location:EmpTimesheet33.php');	
	}

	
}
if(isset($_REQUEST['cancel']) && $_REQUEST['cancel'] == 'cancel')
{	
   
		header("location:home.php");
	
}

include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet</h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
		
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<style>
.timesheet-table{
	margin: 5px;
    width: 83px;
}
.col-sm-12 {
    width: 100%;
    padding-bottom: 1%;
}
/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

/*input#user_name {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}

input#user_name[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}*/

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}

.autocomplete-items {
  position: relative;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
label{
	margin-left: 3px;
	font-weight: 600;	
}
.mobiles{
	display:none;
}
</style>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
		
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Timesheet <?php //echo $current_date.'--'.$nextday;?></h3>      
            </div>
              <form role="form" method="POST" autocomplete="nope">
		            <div class="box-body">
                    
                    <div class="input form-group">
                    <input type="hidden" name="curentDate" value="<?php echo $current_date; ?>" />
                    <input type="hidden" name="previDate" value="<?php echo $nextday; ?>" />
                    
                    <input id="user_name" type="text" name="user_name" class="form-control" placeholder="Select user" autocomplete="nope" onchange="return getuserid(this.value);">
	    		     
	            		</div>
		            		
                            <div class="col-sm-12 destop">
                                <div class="col-sm-1"><label>Date</label></div>
                                <div class="col-sm-1"><label>Rostered Time</label></div>
                                <div class="col-sm-1"><label>Ro. Time end</label></div>
                                <div class="col-sm-1"><label>Actual Time</label></div>

                                <div class="col-sm-1"><label>Actual End</label></div>
                                <div class="col-sm-1"><label>Meal Break</label></div>
                                <div class="col-sm-1"><label>Meal brk end</label></div>
                                <div class="col-sm-1"><label>P_Holiday /sick_leave</label></div>
                                <div class="col-sm-1"><label>before6am</label></div>
                                <div class="col-sm-1"><label>after10pm</label></div>
                                <div class="col-sm-1"><label>normalHour</label></div>
                                <div class="col-sm-1"><label>totalHour</label></div>
                              </div>
                    	
                    		
                    
                              <div class="col-sm-12">
                                <div class="col-sm-1"> 
                                	 <div class="col-sm-12 mobiles"><label>Date :</label></div>
                                	<input type="text" class="timesheet-table" id="datepicker1" name="datepicker1" value="<?php echo $date1; ?>" readonly/></div>
                                <div class="col-sm-1">
                                	 <div class="col-sm-12 mobiles"><label>Rostered Time :</label></div>
                                 <select class="timesheet-table" id="rostered_time1" name="rostered_time1" onchange="return removeDate(1);" <?php if(array_key_exists($date1,$dates) || ($date1 != date('Y-m-d') && $date2 != date('Y-m-d')) ){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                 <option >Start Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) { 
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Rostered Time End :</label></div>
                                 <select class="timesheet-table" id="rostered_time_end1" name="rostered_time_end1" onchange="return removeDateEnd(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                    	 <?php
                                         	$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time :</label></div>
                                <select class="timesheet-table" id="actual_time1" name="actual_time1" onchange="return removeDateactual(1);" <?php if(array_key_exists($date1,$dates) || ($date1 != date('Y-m-d') && $date2 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                	<option >Start Time</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                               		</select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Actual Time End :</label></div>
                                <select class="timesheet-table" id="actual_time_end1" name="actual_time_end1" onchange="return actualendtime(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                               		</select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Meal Break Time :</label></div>
                                <select class="timesheet-table" id="meal_break_time1" name="meal_break_time1" onchange="return mealStart(1);" <?php if(array_key_exists($date1,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Meal Break Time End :</label></div>
                                 <select class="timesheet-table" id="meal_break_time_end1" name="meal_break_time_end1" onchange="return calculation(1)" <?php if(array_key_exists($date1,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1">
                                 <div class="col-sm-12 mobiles"><label>Public Holiday / Sick Leave :</label></div>
                                 		<input type="checkbox" class="" name="public_holiday1" id="public_holiday1" style="margin-left: 15px;margin-right: 2px;" onclick="return clickchaeckbox(1,0);" >/
                                        <input type="checkbox" class="" name="sick_leave1" id="sick_leave1" onclick="return clickchaeckbox(1,1);">
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Before 6am :</label></div>
                                <input type="text" class="timesheet-table" id="before6am1" name="before6am1" value="<?php if($dates[$date1]['before_6am']){ echo $dates[$date1]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>After 10pm :</label></div>
                                <input type="text" class="timesheet-table" id="after10pm1" name="after10pm1" value="<?php if($dates[$date1]['after_10pm']){ echo $dates[$date1]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Normal Hours :</label></div>
                                <input type="text" class="timesheet-table" id="normalHour1" name="normalHour1" value="<?php if($dates[$date1]['normal_hours']){ echo $dates[$date1]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Total Hours :</label></div>
                                <input type="text" class="timesheet-table" id="totalHour1" name="totalHour1" value="<?php if($dates[$date1]['total_hours']){ echo $dates[$date1]['total_hours']; }else { echo "0";} ?>" readonly /></div>
                                <hr />
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Date :</label></div>
                                 <input type="text" class="timesheet-table" id="datepicker2" name="datepicker2" value="<?php  echo $date2; ?>"readonly/></div>
                                <div class="col-sm-1"> 	
                                 <div class="col-sm-12 mobiles"><label>Rostered Time :</label></div>							
                                <select id="rostered_time2" name="rostered_time2" class="timesheet-table" onchange="return removeDate(2);" <?php if(array_key_exists($date2,$dates) || ($date2 != date('Y-m-d') && $date3 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                 <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Rostered Time End :</label></div>								
                                <select id="rostered_time_end2" name="rostered_time_end2" class="timesheet-table" onchange="return removeDateEnd(2);" <?php if(array_key_exists($date2,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time :</label></div>
                                <select class="timesheet-table" id="actual_time2" name="actual_time2" onchange="return removeDateactual(2);"  <?php if(array_key_exists($date2,$dates) || ($date2 != date('Y-m-d') && $date3 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                          </select>
                                </div>
                                 <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Actual Time End :</label></div>
                                 <select class="timesheet-table" id="actual_time_end2" name="actual_time_end2" onchange="return actualendtime(2);"  <?php if(array_key_exists($date2,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                 <option >End Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                          </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time :</label></div>
                                <select class="timesheet-table" id="meal_break_time2" name="meal_break_time2" onchange="return mealStart(2);"  <?php if(array_key_exists($date2,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                </select></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time End :</label></div>
                                <select class="timesheet-table" id="meal_break_time_end2" name="meal_break_time_end2" onchange="return calculation(2)"  <?php if(array_key_exists($date2,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                </select></div>
                                 <div class="col-sm-1">
                                 <div class="col-sm-12 mobiles"><label>Public Holiday / Sick Leave :</label></div>
                                 		<input type="checkbox"  name="public_holiday2" id="public_holiday2" style="margin-left: 15px;margin-right: 2px;" onclick="return clickchaeckbox(2,0);">/
                                        <input type="checkbox" class="" name="sick_leave2" id="sick_leave2" onclick="return clickchaeckbox(2,1);">
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Before 6am :</label></div>
                                <input type="text" class="timesheet-table" id="before6am2" name="before6am2" value="<?php if($dates[$date2]['before_6am']){ echo $dates[$date2]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>After 10pm :</label></div>
                                <input type="text" class="timesheet-table" id="after10pm2" name="after10pm2" value="<?php if($dates[$date2]['after_10pm']){ echo $dates[$date2]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Normal Hours :</label></div>
                                <input type="text" class="timesheet-table" id="normalHour2" name="normalHour2" value="<?php if($dates[$date2]['normal_hours']){ echo $dates[$date2]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Total Hours :</label></div>
                                <input type="text" class="timesheet-table" id="totalHour2" name="totalHour2" value="<?php if($dates[$date2]['total_hours']){ echo $dates[$date2]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                                 <hr />
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Date</label></div>
                                 <input type="text" class="timesheet-table" id="datepicker3" name="datepicker3" value="<?php echo $date3;  ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Rostered Time</label></div>
                                <select id="rostered_time3" name="rostered_time3" class="timesheet-table" onchange="return removeDate(3);" <?php if(array_key_exists($date3,$dates) || ($date3 != date('Y-m-d') && $date4 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
												
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
												
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                  <div class="col-sm-1"> 
                                  <div class="col-sm-12 mobiles"><label>Rostered Time End</label></div>
                                  <select id="rostered_time_end3" name="rostered_time_end3" class="timesheet-table" onchange="return removeDateEnd(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                  <option >End Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time</label></div>
                                <select class="timesheet-table" id="actual_time3" name="actual_time3" onchange="return removeDateactual(3);" <?php if(array_key_exists($date3,$dates) || ($date3 != date('Y-m-d') && $date4 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                				<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time End :</label></div>
                                <select class="timesheet-table" id="actual_time_end3" name="actual_time_end3" onchange="return actualendtime(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                				<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time :</label></div>
                                <select class="timesheet-table" id="meal_break_time3" name="meal_break_time3" onchange="return mealStart(3);" <?php if(array_key_exists($date3,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time End :</label></div>
                                <select class="timesheet-table" id="meal_break_time_end3" name="meal_break_time_end3" onchange="return calculation(3)" <?php if(array_key_exists($date3,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1">
                                 <div class="col-sm-12 mobiles"><label>Public Holiday / Sick Leave :</label></div>
                                 		<input type="checkbox"  name="public_holiday3" id="public_holiday3" style="margin-left: 15px;margin-right: 2px;" onclick="return clickchaeckbox(3,0);">/
                                        <input type="checkbox" class="" name="sick_leave3" id="sick_leave3" onclick="return clickchaeckbox(3,1);">
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Before 6am :</label></div>
                                <input type="text" class="timesheet-table" id="before6am3" name="before6am3" value="<?php if($dates[$date3]['before_6am']){ echo $dates[$date3]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>After 10pm :</label></div>
                                <input type="text" class="timesheet-table" id="after10pm3" name="after10pm3" value="<?php if($dates[$date3]['after_10pm']){ echo $dates[$date3]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Normal Hours :</label></div>
                                <input type="text" class="timesheet-table" id="normalHour3" name="normalHour3" value="<?php if($dates[$date3]['normal_hours']){ echo $dates[$date3]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Total Hours :</label></div>
                                <input type="text" class="timesheet-table" id="totalHour3" name="totalHour3" value="<?php if($dates[$date3]['total_hours']){ echo $dates[$date3]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                                 <hr />
                              </div>
                              
                              <div class="col-sm-12">
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Date</label></div>
                                <input type="text" class="timesheet-table" id="datepicker4" name="datepicker4" value="<?php echo $date4; ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Rostered Time</label></div>
                                <select id="rostered_time4" name="rostered_time4" class="timesheet-table" onchange="return removeDate(4);" <?php if(array_key_exists($date4,$dates) || ($date4 != date('Y-m-d') && $date5 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Rostered Time End</label></div>
                                 <select id="rostered_time_end4" name="rostered_time_end4" class="timesheet-table" onchange="return removeDateEnd(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time</label></div>
                                <select class="timesheet-table" id="actual_time4" name="actual_time4" onchange="return removeDateactual(4);" <?php if(array_key_exists($date4,$dates) || ($date4 != date('Y-m-d') && $date5 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                		</select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time End :</label></div>
                                <select class="timesheet-table" id="actual_time_end4" name="actual_time_end4" onchange="return actualendtime(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                		</select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time :</label></div>
                                <select class="timesheet-table" id="meal_break_time4" name="meal_break_time4" onchange="return mealStart(4);" <?php if(array_key_exists($date4,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                        </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time End :</label></div>
                                <select class="timesheet-table" id="meal_break_time_end4" name="meal_break_time_end4" onchange="return calculation(4)" <?php if(array_key_exists($date4,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                        </select>
                                </div>
                                 <div class="col-sm-1">
                                 <div class="col-sm-12 mobiles"><label>Public Holiday / Sick Leave :</label></div>
                                 		<input type="checkbox" name="public_holiday4" id="public_holiday4" style="margin-left: 15px;margin-right: 2px;" onclick="return clickchaeckbox(4,0);">/
                                        <input type="checkbox" class="" name="sick_leave4" id="sick_leave4" onclick="return clickchaeckbox(4,1);">
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Before 6am :</label></div>
                                <input type="text" class="timesheet-table" id="before6am4" name="before6am4" value="<?php if($dates[$date4]['before_6am']){ echo $dates[$date4]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>After 10pm :</label></div>
                                <input type="text" class="timesheet-table" id="after10pm4" name="after10pm4" value="<?php if($dates[$date4]['after_10pm']){ echo $dates[$date4]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Normal Hours :</label></div>
                                <input type="text" class="timesheet-table" id="normalHour4" name="normalHour4" value="<?php if($dates[$date4]['normal_hours']){ echo $dates[$date4]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Total Hours :</label></div>
                                <input type="text" class="timesheet-table" id="totalHour4" name="totalHour4" value="<?php if($dates[$date4]['total_hours']){ echo $dates[$date4]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                                 <hr />
                              </div>
                              
                              <div class="col-sm-12">
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Date</label></div>
                                <input type="text" class="timesheet-table" id="datepicker5" name="datepicker5" value="<?php echo $date5;  ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Rostered Time</label></div>
                                <select id="rostered_time5" name="rostered_time5" class="timesheet-table" onchange="return removeDate(5);" <?php if(array_key_exists($date5,$dates) || ($date5 != date('Y-m-d') && $date6 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Rostered Time End</label></div>
                                 <select id="rostered_time_end5" name="rostered_time_end5" class="timesheet-table" onchange="return removeDateEnd(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Actual Time</label></div>
                                 <select class="timesheet-table" id="actual_time5" name="actual_time5" onchange="return removeDateactual(5);" <?php if(array_key_exists($date5,$dates) || ($date5 != date('Y-m-d') && $date6 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>		
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time End :</label></div>
                                <select class="timesheet-table" id="actual_time_end5" name="actual_time_end5" onchange="return actualendtime(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>		
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time :</label></div>
                                <select class="timesheet-table" id="meal_break_time5" name="meal_break_time5" onchange="return mealStart(5);" <?php if(array_key_exists($date5,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Meal Break Time End :</label></div>
                                 <select class="timesheet-table" id="meal_break_time_end5" name="meal_break_time_end5" onchange="return calculation(5)" <?php if(array_key_exists($date5,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                 <option >End Time</option>
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1">
                                 <div class="col-sm-12 mobiles"><label>Public Holiday / Sick Leave :</label></div>
                                 		<input type="checkbox"  name="public_holiday5" id="public_holiday5" style="margin-left: 15px;margin-right: 2px;" onclick="return clickchaeckbox(5,0);">/
                                        <input type="checkbox" class="" name="sick_leave5" id="sick_leave5" onclick="return clickchaeckbox(5,1);">
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Before 6am :</label></div>
                                <input type="text" class="timesheet-table" id="before6am5" name="before6am5" value="<?php if($dates[$date5]['before_6am']){ echo $dates[$date5]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>After 10pm :</label></div>
                                <input type="text" class="timesheet-table" id="after10pm5" name="after10pm5" value="<?php if($dates[$date5]['after_10pm']){ echo $dates[$date5]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Normal Hours :</label></div>
                                <input type="text" class="timesheet-table" id="normalHour5" name="normalHour5" value="<?php if($dates[$date5]['normal_hours']){ echo $dates[$date5]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Total Hours :</label></div>
                                <input type="text" class="timesheet-table" id="totalHour5" name="totalHour5" value="<?php if($dates[$date5]['total_hours']){ echo $dates[$date5]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                                <hr />
                              </div>
                              
                              <div class="col-sm-12">
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Date</label></div>
                                 <input type="text" class="timesheet-table" id="datepicker6" name="datepicker6" value="<?php echo $date6; ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Rostered Time</label></div>
                                <select id="rostered_time6" name="rostered_time6" class="timesheet-table" onchange="return removeDate(6);" <?php if(array_key_exists($date6,$dates) || ($date6 != date('Y-m-d') && $date7 != date('Y-m-d'))){ echo "disabled style=' color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Rostered Time End</label></div>
                                 <select id="rostered_time_end6" name="rostered_time_end6" class="timesheet-table" onchange="return removeDateEnd(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time</label></div>
                                <select class="timesheet-table" id="actual_time6" name="actual_time6" onchange="return removeDateactual(6);" <?php if(array_key_exists($date6,$dates) || ($date6 != date('Y-m-d') && $date7 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Actual Time End :</label></div>
                                 <select class="timesheet-table" id="actual_time_end6" name="actual_time_end6" onchange="return actualendtime(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                 <option >End Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time :</label></div>
                                <select class="timesheet-table" id="meal_break_time6" name="meal_break_time6" onchange="return mealStart(6);" <?php if(array_key_exists($date6,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Meal Break Time End :</label></div>
                                 <select class="timesheet-table" id="meal_break_time_end6" name="meal_break_time_end6" onchange="return calculation(6)" <?php if(array_key_exists($date6,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                 <option >End Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1">
                                 <div class="col-sm-12 mobiles"><label>Public Holiday / Sick Leave :</label></div>
                                 		<input type="checkbox"  name="public_holiday6" id="public_holiday6" style="margin-left: 15px;margin-right: 2px;" onclick="return clickchaeckbox(6,0);">/
                                        <input type="checkbox" class="" name="sick_leave6" id="sick_leave6" onclick="return clickchaeckbox(6,1);">
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Before 6am :</label></div>
                                <input type="text" class="timesheet-table" id="before6am6" name="before6am6" value="<?php if($dates[$date6]['before_6am']){ echo $dates[$date6]['before_6am'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>After 10pm :</label></div>
                                <input type="text" class="timesheet-table" id="after10pm6" name="after10pm6" value="<?php if($dates[$date6]['after_10pm']){ echo $dates[$date6]['after_10pm'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Normal Hours :</label></div>
                                <input type="text" class="timesheet-table" id="normalHour6" name="normalHour6" value="<?php if($dates[$date6]['normal_hours']){ echo $dates[$date6]['normal_hours'];}else { echo "0";} ?>" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Total Hours :</label></div>
                                <input type="text" class="timesheet-table" id="totalHour6" name="totalHour6" value="<?php if($dates[$date6]['total_hours']){ echo $dates[$date6]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                                <hr />
                              </div>
                              
                              <div class="col-sm-12">
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Date</label></div>
                                <input type="text" class="timesheet-table" id="datepicker7" name="datepicker7" value="<?php  echo $date7; ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Rostered Time</label></div>
                                <select id="rostered_time7" name="rostered_time7" class="timesheet-table" onchange="return removeDate(7);" <?php if(array_key_exists($date7,$dates) || ($date7 != date('Y-m-d')) ){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Rostered Time End</label></div>
                                <select id="rostered_time_end7" name="rostered_time_end7" onchange="return removeDateEnd(7);" class="timesheet-table" <?php if(array_key_exists($date7,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Actual Time</label></div>
                                <select class="timesheet-table" id="actual_time7" name="actual_time7" onchange="return removeDateactual(7);" <?php if(array_key_exists($date7,$dates) || ($date7 != date('Y-m-d'))){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                 <div class="col-sm-1"> 
                                 <div class="col-sm-12 mobiles"><label>Actual Time End :</label></div>
                                 <select class="timesheet-table" id="actual_time_end7" name="actual_time_end7" onchange="return actualendtime(7);" <?php if(array_key_exists($date7,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                 <option >End Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time :</label></div>
                                <select class="timesheet-table" id="meal_break_time7" name="meal_break_time7" onchange="return mealStart(7);" <?php if(array_key_exists($date7,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >Start Time</option>
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Meal Break Time End :</label></div>
                                <select class="timesheet-table" id="meal_break_time_end7" name="meal_break_time_end7" onchange="return calculation(7)" <?php if(array_key_exists($date7,$dates)){ echo "disabled style='   color: #c3c3c3; '";} ?>>
                                <option >End Time</option>
                                		<?php
											$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now < $end) {
													
														echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1">
                                <div class="col-sm-12 mobiles"><label>Public Holiday / Sick Leave :</label></div>
                                 		<input type="checkbox"  name="public_holiday7" id="public_holiday7" style="margin-left: 15px;margin-right: 2px;" onclick="return clickchaeckbox(7,0);">/
                                        <input type="checkbox" class="" name="sick_leave7" id="sick_leave7" onclick="return clickchaeckbox(7,1);">
                                </div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Before 6am :</label></div>
                                <input type="text" class="timesheet-table" id="before6am7" name="before6am7" value="<?php if($dates[$date7]['before_6am']){ echo $dates[$date7]['before_6am'];}else { echo "0";} ?>" readonly /></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>After 10pm :</label></div>
                                <input type="text" class="timesheet-table" id="after10pm7" name="after10pm7" value="<?php if($dates[$date7]['after_10pm']){ echo $dates[$date7]['after_10pm'];}else { echo "0";} ?>" readonly /></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Normal Hours :</label></div>
                                <input type="text" class="timesheet-table" id="normalHour7" name="normalHour7" value="<?php if($dates[$date7]['normal_hours']){ echo $dates[$date7]['normal_hours'];}else { echo "0";} ?>" readonly /></div>
                                <div class="col-sm-1"> 
                                <div class="col-sm-12 mobiles"><label>Total Hours :</label></div>
                                <input type="text" class="timesheet-table" id="totalHour7" name="totalHour7" value="<?php if($dates[$date7]['total_hours']){ echo $dates[$date7]['total_hours']; }else { echo "0";} ?>" readonly/></div>
                              </div>
                             
                              <div class="col-sm-12"><hr>
                               <button type="submit" class="btn-primary" name="cancel" value="cancel">cancel</button>
                                <button type="submit" class="btn-primary" name="Submit" value="Submit"onclick="return confirm('Are you sure you want to submit?');">Submit</button>
                              </div>
                              <input type="hidden" value="" name="fk_employee_id" id="fk_employee_id"/>
                             <input type="hidden" value="<?php echo $_SESSION['varStore']; ?>" name="sesstion_id" id="sesstion_id"/>
                             
                             
				<table width="100%" border="1" class="table table-bordered table-striped dataTable no-footer">
                	<thead>
                    	<!--<th>Store id</th>-->
                        <!--<th>Emp id</th>-->
                       <!-- <th>Week Start</th>
                        <th>Week End</th>-->
                        <th>Before 6am</th>
                        <th>After 10pm</th>
                        <th>Saturday Hours</th>
                        <th>Sunday Hours</th>
                        <th>Public Holiday Hours</th>
                        <th>Sick Leave Hours</th>
                        <th>Normal Total Hours</th>
                        <th>Weekly Total Hours</th>
                        <th>Number Of Shifts</th>
                    </thead>
                    <tr>
                    	<!--<td><label id="storeId"></label></td>-->
                        <!--<td><label id="emplId"></label></td>-->
                        <!--<td><label id="weekStart"></label></td>
                        <td><label id="weekEnd"></label></td>-->
                        <td><label id="before6amTotal"></label></td>
                        <td><label id="after10pmTotal"></label></td>
                        <td><label id="saturdayHours"></label></td>
                        <td><label id="sundayHours"></label></td>
                        <td><label id="publicHolidayHours"></label></td>
                        <td><label id="sickLeaveHours"></label></td>
                        <td><label id="normalTotalHours"></label></td>
                        <td><label id="weeklyTotalHours"></label></td>
                        <td><label id="numberOfShifts"></label></td>
                    </tr>
                </table>                    
		            </div>
                    
		        </form>         
               
     </div>
 </div>
</div>
</section>
<script language="javascript">
 $(function() {});
  
	/*$("#rostered_time_end1").val('12:15 am');*/
	var isSuperAdmin = "<?php echo $_SESSION['U_Type'] ?>";
//	alert(isSuperAdmin);
	if(isSuperAdmin == 0){
		for(i = 1 ;i<8; i++){
			$("#rostered_time"+i).attr('disabled', false);
			$("#actual_time"+i).attr('disabled', false);
			$("#actual_time"+i).css('color', 'black');
			$("#rostered_time"+i).css('color', 'black');
		}
	}
	
	for(i = 1 ;i<8; i++){
		$("#rostered_time_end"+i).attr('disabled', true);
		$("#rostered_time_end"+i).css('color', '#c3c3c3');
		$("#actual_time_end"+i).attr('disabled', true);
		$("#actual_time_end"+i).css('color', '#c3c3c3');
		$("#meal_break_time"+i).attr('disabled', true); 
		$("#meal_break_time"+i).css('color', '#c3c3c3');
		$("#meal_break_time_end"+i).attr('disabled', true); 
		$("#meal_break_time_end"+i).css('color', '#c3c3c3');
	//	$('#public_holiday'+i).attr('disabled', true); 
	//	$('#sick_leave'+i).attr('disabled', true); 
	}
	
function appentoption( selectid , position ,disabledend , isselect){
	
	$.ajax({
    type: "GET",
    url: "getDropdown.php",
    data: {    },
    success: function (response) {
		var alldata = JSON.parse(response);
		//alert(alldata[0]);
		$('#'+selectid + position ).empty();
		$('#'+selectid + position )
         .append($("<option disabled></option>")
                    .attr("value","End Time")
                    .text("End Time")); 
					
					
		for(var i = 0 ; i < alldata.length ; i++){
			if(i <= disabledend){
				var disabled = 'disabled'
			}else{
				var disabled = '';
			}
			
		if(i == isselect){
			var selected = 'selected';
		}else{
			var selected = '';
		}
		 $('#'+selectid + position )
         .append($("<option "+disabled+" "+selected+"></option>")
                    .attr("value",alldata[i])
                    .text(alldata[i])); 
		}
	}});
	
}
function appentOptionMeal( selectid , position ,disabledend , disabledstart ,isselect){
	
	$.ajax({
    type: "GET",
    url: "getDropdown.php",
    data: {    },
    success: function (response) {
		var alldata = JSON.parse(response);
		//alert(alldata[0]);
		$('#'+selectid + position ).empty();
		$('#'+selectid + position )
         .append($("<option disabled></option>")
                    .attr("value","End Time")
                    .text("End Time")); 
					
					
		for(var i = 0 ; i < alldata.length ; i++){
			if(i < disabledend || i >= disabledstart){
				var disabled = 'disabled';
			}else{
				var disabled = '';
			}
			if(i == isselect){
			var selected = 'selected';
		}else{
			var selected = '';
		}

		 $('#'+selectid + position )
         .append($("<option "+disabled+" "+selected+"></option>")
                    .attr("value",alldata[i])
                    .text(alldata[i])); 
		}
	}});
	
}
function mealStart(postion){
	
	 var at1 = $("#actual_time"+postion).prop('selectedIndex');
	 var atend1 = $("#actual_time_end"+postion).prop('selectedIndex');
	 var ate1 = document.getElementById("actual_time"+postion);
	 var ml1 = document.getElementById("meal_break_time"+postion);
	 var mposition = $("#meal_break_time"+postion).prop('selectedIndex');
	 var i = at1;
	
	var diffrnt = $("#meal_break_time_end"+postion).prop('selectedIndex') - $("#meal_break_time"+postion).prop('selectedIndex')
	
	if(at1 > $("#meal_break_time"+postion).prop('selectedIndex') || $("#meal_break_time"+postion).prop('selectedIndex') > atend1){
		alert("You can only select time between actual time and actual time end..");
		$("#meal_break_time"+postion).val(ate1.options[at1+1].value);
		$("#meal_break_time_end"+postion).val(ate1.options[at1+1].value);
		return false;
	}else{
	//	$("#meal_break_time_end"+postion).val(ml1.options[mposition].value);
	}
//	appentOptionMeal( "meal_break_time_end" , postion , mposition , atend1 , mposition-1);
	
	//$("#meal_break_time"+postion).attr('disabled', true); 
	$("#meal_break_time_end"+postion).attr('disabled', false); 
	$("#meal_break_time_end"+postion).css('color', 'black');
	
	calculation(postion);
}
function removeDateEnd(postion){
//	$("#rostered_time_end"+postion).attr('disabled', true); 
}

function removeDate(position){
	
	 var rt1 = $("#rostered_time"+position).prop('selectedIndex');
	 var rte1 = document.getElementById("rostered_time_end"+position);
	 var i = rt1;
	 
	 appentoption( "rostered_time_end" , position ,rt1-1);
	 
	 if(rt1 > 0){
	 	$("#rostered_time_end"+position).attr('disabled', false); 
		$("#rostered_time_end"+position).css('color', 'black');
	 }
	 
	 for( i = rt1 ; i >= 0 ; i--){
		 //alert(x.options[i].value);
		 document.getElementById("rostered_time_end"+position).options[i].disabled = true;
	 	//x.remove(i);
	 }
	 $("#rostered_time_end"+position).val(rte1.options[rt1+1].value);
}

function befor6am(startt , endt){
	
	var befor6am = 0;
	 if(startt < 25 && endt <= 25){
	 		befor6am =  ( endt - startt )/4;
	 }else if(startt < 25 && endt > 25){
	 		befor6am = (25 - startt)/4;	
	 }	
	 
	return befor6am;
	
}

function after10pm(startt , endt){
	
	var after10pm = 0;
	//alert(startt + '-' + endt)
	 if(startt >= 89 && endt > 89){
	 		after10pm =  ( endt - startt )/4;
	 }else if(startt < 89 && endt > 89){
	 		after10pm = (endt - 89)/4;	
	 }	
	return after10pm;
		
}


function normalHour(startts , endtt , mstart , mend , ii , aftr){
	
	
	var normalHour=0;	
	var diffbeforafter = 0
	
	var diff = (endtt - startts)/4 ;
	
	var milbreake = (mend - mstart)/4;
	
	var bforAftr = Number(document.getElementById("before6am"+ii).value) + Number(aftr);
	
	diffbeforafter = Number(diff) - Number(bforAftr) ; 
	
	normalHour = Number(diffbeforafter) - Number(milbreake) ;
	
	if(normalHour < 0 ){normalHour = 0;}
	
	return normalHour;
}

function totalHour(normalHour , befor , after ){
	var totalHour = 0;
	
	totalHour = Number(normalHour) + Number(befor) + Number(after);
	
	return totalHour;

}

function rosteedFocuss(){
	if($("#meal_break_time1").prop('selectedIndex') < $("#actual_time1").prop('selectedIndex') || $("#meal_break_time_end1").prop('selectedIndex') > $("#actual_time_end1").prop('selectedIndex'))
	{
		document.getElementById("meal_break_time1").focus();
		return false;
	}
}

function actualendtime(posiion){
	//$("#actual_time_end"+posiion).attr('disabled', true); 
	 var at1 = $("#actual_time"+posiion).prop('selectedIndex');
	 var atend1 = $("#actual_time_end"+posiion).prop('selectedIndex');
	 var ate1 = document.getElementById("actual_time"+posiion);
	 var i = at1;
	 
	// alert($("#meal_break_time"+posiion).prop('selectedIndex'));
	 
	if($("#actual_time"+posiion).prop('selectedIndex') > $("#meal_break_time"+posiion).prop('selectedIndex')){
		var mealsetdate =  $("#actual_time"+posiion).prop('selectedIndex');
		var mealsetdateend = mealsetdate;
	}else if($("#actual_time_end"+posiion).prop('selectedIndex') < $("#meal_break_time_end"+posiion).prop('selectedIndex')){
		var mealsetdate =  $("#actual_time"+posiion).prop('selectedIndex');
		var mealsetdateend = mealsetdate;
	}else{
		var mealsetdate =  $("#meal_break_time"+posiion).prop('selectedIndex')-1;
		var mealsetdateend = $("#meal_break_time_end"+posiion).prop('selectedIndex')-1;
	}
	
	
	appentOptionMeal( "meal_break_time" , posiion , at1 , atend1 , mealsetdate);
	appentOptionMeal( "meal_break_time_end" , posiion , at1 , atend1 , mealsetdateend);
	
	if(at1 > atend1 && atend1 >1){
		//alert();
		$("#actual_time_end"+posiion).val(ate1.options[at1+1].value);
		return false;
	}
	
	document.getElementById("before6am"+posiion).value = befor6am( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') );
	
	document.getElementById("after10pm"+posiion).value = after10pm( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') );
	
	document.getElementById("normalHour"+posiion).value = normalHour( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') , $("#meal_break_time"+posiion).prop('selectedIndex') , $("#meal_break_time_end"+posiion).prop('selectedIndex'),posiion,document.getElementById("after10pm"+posiion).value);
	
	var mealHour = ($("#meal_break_time_end"+posiion).prop('selectedIndex') - $("#meal_break_time"+posiion).prop('selectedIndex'))/4;
		
	if($("#actual_time_end"+posiion).prop('selectedIndex') > 25){
	document.getElementById("totalHour"+posiion).value = Number(document.getElementById("before6am"+posiion).value) + Number(document.getElementById("after10pm"+posiion).value) + Number(document.getElementById("normalHour"+posiion).value) ;  
	}else{
		
		document.getElementById("totalHour"+posiion).value = Number(document.getElementById("before6am"+posiion).value) - Number(mealHour) ; 
	
	}
	
	$("#meal_break_time"+posiion).attr('disabled', false); 
	$("#meal_break_time"+posiion).css('color', 'black');
	
	calculation(posiion);
	
}

function calculation(posiion){
	
	
	 var at1 = $("#actual_time"+posiion).prop('selectedIndex');
	 var atend1 = $("#actual_time_end"+posiion).prop('selectedIndex');
	 var ate1 = document.getElementById("actual_time"+posiion);
	 var ml1 = document.getElementById("meal_break_time"+posiion);
	 var mposition = $("#meal_break_time"+posiion).prop('selectedIndex');
	 var i = at1;
	
	if($("#meal_break_time"+posiion).prop('selectedIndex') > $("#meal_break_time_end"+posiion).prop('selectedIndex') || $("#meal_break_time_end"+posiion).prop('selectedIndex') > atend1 ){
		//alert("You can only select time between start meal break time and actual time end..");
		$("#meal_break_time_end"+posiion).val(ml1.options[mposition].value);
	//	return false;
	}
		
	document.getElementById("normalHour"+posiion).value = normalHour( $("#actual_time"+posiion).prop('selectedIndex') , $("#actual_time_end"+posiion).prop('selectedIndex') , $("#meal_break_time"+posiion).prop('selectedIndex') , $("#meal_break_time_end"+posiion).prop('selectedIndex'),posiion,document.getElementById("after10pm"+posiion).value);
		
	var mealHour = ($("#meal_break_time_end"+posiion).prop('selectedIndex') - $("#meal_break_time"+posiion).prop('selectedIndex'))/4;
	
	if($("#actual_time_end"+posiion).prop('selectedIndex') > 25){
	document.getElementById("totalHour"+posiion).value = Number(document.getElementById("before6am"+posiion).value) + Number(document.getElementById("after10pm"+posiion).value) + Number(document.getElementById("normalHour"+posiion).value) ;  
	}else{
		document.getElementById("totalHour"+posiion).value = Number(document.getElementById("before6am"+posiion).value) - Number(mealHour) ; 
	}
	
}
function reset_total(positions){
	document.getElementById("before6am"+positions).value  = '0';
	document.getElementById("after10pm"+positions).value  = '0';
	document.getElementById("normalHour"+positions).value = '0';
	document.getElementById("totalHour"+positions).value  = '0';
}

function removeDateactual(positin){
	
	//alert($("#actual_time_end"+positin).prop('selectedIndex'));
	if($("#actual_time"+positin).prop('selectedIndex') > $("#actual_time_end"+positin).prop('selectedIndex')){
		var acualendsdate =  $("#actual_time"+positin).prop('selectedIndex');
	}else{
		var acualendsdate =  $("#actual_time_end"+positin).prop('selectedIndex')-1;
	}
	appentoption( "actual_time_end" , positin , $("#actual_time"+positin).prop('selectedIndex') , acualendsdate );
	
	actualendtime(positin); 
	
	$("#actual_time_end"+positin).attr('disabled', false); 
	$("#actual_time_end"+positin).css('color', 'black');
	
	resetActualtime = acualendsdate+1;
	
	document.getElementById("before6am"+positin).value = befor6am( $("#actual_time"+positin).prop('selectedIndex') , resetActualtime );

	document.getElementById("after10pm"+positin).value = after10pm( $("#actual_time"+positin).prop('selectedIndex') , resetActualtime );
	
	document.getElementById("normalHour"+positin).value = normalHour( $("#actual_time"+positin).prop('selectedIndex') , resetActualtime , $("#meal_break_time"+positin).prop('selectedIndex') , $("#meal_break_time_end"+positin).prop('selectedIndex'),positin,document.getElementById("after10pm"+positin).value);
	
	if($("#actual_time_end"+positin).prop('selectedIndex') > 25){
	document.getElementById("totalHour"+positin).value = Number(document.getElementById("before6am"+positin).value) + Number(document.getElementById("after10pm"+positin).value) + Number(document.getElementById("normalHour"+positin).value) ;  
	}else{
		
		if($("#meal_break_time"+positin).prop('selectedIndex') >= $("#actual_time"+positin).prop('selectedIndex'))
		{
			document.getElementById("totalHour"+positin).value = Number(document.getElementById("before6am"+positin).value) - Number(mealHour) ; 
		}else{
			document.getElementById("totalHour"+positin).value = Number(document.getElementById("before6am"+positin).value) ; 
		}
		
	}

}
function appendoptionSelect( selectid , position , selectOption)
{
	$.ajax({
    type: "GET",
    url: "getDropdown.php",
    data: {    },
    success: function (response) {
		var alldata = JSON.parse(response);
		//alert(alldata[0]);
		$('#'+selectid + position ).empty();
		$('#'+selectid + position )
         .append($("<option disabled></option>")
                    .attr("value","End Time")
                    .text("End Time")); 
					
					
		for(var i = 0 ; i < alldata.length ; i++){
			//alert(selectOption);
			if(i == selectOption){
				var disabled = 'selected'
			}else{
				var disabled = '';
			}
		 $('#'+selectid + position )
         .append($("<option "+disabled+"></option>")
                    .attr("value",alldata[i])
                    .text(alldata[i])); 
		}
	}});

}
function setvalues( positin, rostered_time, rostered_time_end , actual_time, actual_time_end, meal_break_time, meal_break_time_end, before_6am, after_10pm, normal_hours, totalhour , checkholiday , checksickleave){	
	
	$("#rostered_time"+positin).val(rostered_time);
	$("#rostered_time_end"+positin).val(rostered_time_end);
	$("#actual_time"+positin).val(actual_time);
	$("#actual_time_end"+positin).val(actual_time_end);
	$("#meal_break_time"+positin).val(meal_break_time);
	$("#meal_break_time_end"+positin).val(meal_break_time_end);
	document.getElementById("before6am"+positin).value = before_6am;
	document.getElementById("after10pm"+positin).value = after_10pm;
	document.getElementById("normalHour"+positin).value = normal_hours;
	document.getElementById("totalHour"+positin).value = totalhour;
	
	if(checkholiday == 1)
	{
		$('#public_holiday'+positin).prop('checked', true);
	}
	if(checksickleave == 1)
	{
		$('#sick_leave'+positin).prop('checked', true);
	}
	
	$("#rostered_time"+positin).css('color', 'green');
	$("#rostered_time_end"+positin).css('color', 'green');
	$("#actual_time"+positin).css('color', 'green');
	$("#actual_time_end"+positin).css('color', 'green');
	$("#meal_break_time"+positin).css('color', 'green');
	$("#meal_break_time_end"+positin).css('color', 'green');
}
function resetvalue(position){
	//alert();
	$("#rostered_time"+position).val("Start Time");
	$("#rostered_time"+position).css('color', '#c3c3c3');
	
	$("#rostered_time_end"+position).val("End Time");
	$("#rostered_time_end"+position).css('color', '#c3c3c3');
	
	$("#actual_time"+position).val("Start Time");
	$("#actual_time"+position).css('color', '#c3c3c3');
	
	$("#actual_time_end"+position).val("End Time");
	$("#actual_time_end"+position).css('color', '#c3c3c3');
	
	$("#meal_break_time"+position).val("Start Time");
	$("#meal_break_time"+position).css('color', '#c3c3c3');
	
	$("#meal_break_time_end"+position).val("End Time");
	$("#meal_break_time_end"+position).css('color', '#c3c3c3');
	
	document.getElementById("before6am"+position).value = 0;
	document.getElementById("after10pm"+position).value = 0;
	document.getElementById("normalHour"+position).value = 0;
	document.getElementById("totalHour"+position).value = 0;
	
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	
	if(position == 1){
		if(date ==  $("#datepicker"+position).val()){
			refresh_dropdown(position);
		}
	}else{
		if(date ==  $("#datepicker"+position).val()){
			minusone = position - 1;
			//if(document.getElementById("totalHour"+minusone).value != 0){
				 refresh_dropdown(minusone);
			//}
			refresh_dropdown(position);
		}
	}
	
	var isSuperAdmin = "<?php echo $_SESSION['U_Type'] ?>";
//	alert(isSuperAdmin);
	if(isSuperAdmin == 0){
		for(i = 1 ;i<8; i++){
			$("#rostered_time"+i).attr('disabled', false);
			$("#actual_time"+i).attr('disabled', false);
			$("#actual_time"+i).css('color', 'black');
			$("#rostered_time"+i).css('color', 'black');
		}
	}
	
}
function refresh_dropdown(i){
	
		$("#rostered_time"+i).attr('disabled', false);
		$("#rostered_time"+i).css('color', 'black');
		
		$('#public_holiday'+i).attr('disabled', false);
		$('#sick_leave'+i).attr('disabled', false);

		$("#actual_time"+i).attr('disabled', false);
		$("#actual_time"+i).css('color', 'black');
}
function Emp_select(uid){
	var storeid = $("#sesstion_id").val();
	var startDate = $("#datepicker1").val();
	var secDate = $("#datepicker2").val();
	var thirdDate = $("#datepicker3").val();
	var fourthDate = $("#datepicker4").val();
	var fifthDate = $("#datepicker5").val();
	var sixDate = $("#datepicker6").val();
	var endDAte =$("#datepicker7").val();
	
	for(p = 1 ; p < 8 ; p++){
		$('#public_holiday'+p).prop('checked', false);
		$('#sick_leave'+p).prop('checked', false);
	}
	
	$.ajax({
    type: "POST",
    url: "getEmployeeTimesheet.php",
    data: {
        userID: uid, 
        storeid:storeid,
		startDate:startDate,
		endDAte:endDAte
    },
    success: function (response) {
		var alldata = JSON.parse(response);
		//alert(JSON.stringify(alldata[startDate]));
		
		for(var y = 1 ; y < 8 ; y++)
		{
			if(alldata[$("#datepicker"+y).val()] != null){
			setvalues(y , alldata[$("#datepicker"+y).val()].rostered_time , alldata[$("#datepicker"+y).val()].rostered_time_end ,alldata[$("#datepicker"+y).val()].actual_time , alldata[$("#datepicker"+y).val()].actual_time_end , alldata[$("#datepicker"+y).val()].meal_break_time ,alldata[$("#datepicker"+y).val()].meal_break_time_end ,alldata[$("#datepicker"+y).val()].before_6am ,alldata[$("#datepicker"+y).val()].after_10pm ,alldata[$("#datepicker"+y).val()].normal_hours ,alldata[$("#datepicker"+y).val()].total_hours ,alldata[$("#datepicker"+y).val()].public_holiday ,alldata[$("#datepicker"+y).val()].sick_leave );
			}else{
				resetvalue(y);
			}
				
		}
		
		//alert(JSON.stringify(alldata['week']));
		if(alldata['week'] != null){	
			//$("#storeId").html(alldata['week'].fk_store_id);
			//$("#emplId").html(alldata['week'].fk_employee_id);
			//$("#weekStart").html(alldata['week'].week_start_date);
			//$("#weekEnd").html(alldata['week'].week_end_date);
			$("#before6amTotal").html(alldata['week'].before6am_total_hours);
			$("#after10pmTotal").html(alldata['week'].after10pm_total_hours);
			$("#saturdayHours").html(alldata['week'].saturday_hours);
			$("#sundayHours").html(alldata['week'].sunday_hours);
			$("#publicHolidayHours").html(alldata['week'].public_holiday_hours);
			$("#sickLeaveHours").html(alldata['week'].sick_leave_hours);			
			$("#normalTotalHours").html(alldata['week'].normal_total_hours);
			$("#weeklyTotalHours").html(alldata['week'].weekly_total_hours);
			$("#numberOfShifts").html(alldata['week'].number_of_shifts);
		}else{
			//$("#storeId").html(0);
			//$("#emplId").html(0);
			//$("#weekStart").html(0);
			//$("#weekEnd").html(0);
			$("#before6amTotal").html(0);
			$("#after10pmTotal").html(0);
			$("#saturdayHours").html(0);
			$("#sundayHours").html(0);
			$("#publicHolidayHours").html(0);
			$("#sickLeaveHours").html(0);
			$("#normalTotalHours").html(0);
			$("#weeklyTotalHours").html(0);
			$("#numberOfShifts").html(0);
		}
    }
});
		
                  
}
jQuery(function ($) {        
  $('form').bind('submit', function () {
    $(this).find(':input').prop('disabled', false);
  });
});
function getuserid(username){
	//alert(username);
}

</script>

<script>
function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
		res = arr[i].split(" ");
		//alert(res[1]);
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase() || res[1].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
			  var fields = inp.value.split('-');
			  if(fields[1] > 0 && fields[1] != null){
			  		$("#fk_employee_id").val(fields[1]);
					Emp_select(fields[1]);
			  }
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

var userName = [];

$.ajax({
    type: "GET",
    url: "getEmployeeUsername.php",
    success: function (response) {
		var alldata = JSON.parse(response);
		//alert(JSON.stringify(alldata))
		alldata.forEach(myFunction);
		
		function myFunction(item, index) {
		  userName.push( item.name );
		}
		//alert(JSON.stringify(userName))
	}
});

/*initiate the autocomplete function on the "user_name" element, and pass along the userName array as possible autocomplete values:*/
autocomplete(document.getElementById("user_name"), userName);
function clickchaeckbox(position,cboxtype)
{
	if(cboxtype == 0){
		$('#sick_leave'+position).prop('checked', false);
	}else{
		$('#public_holiday'+position).prop('checked', false);
	}
}

</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>