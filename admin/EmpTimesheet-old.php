<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] == 'Submit')
{	
    $Emp_Create_timesheet = $oEmp_DA->Emp_Create_timesheet($_POST);

	if($Emp_Create_timesheet)
	{
		header('Location:EmpTimesheet.php?msg=succ');	
	}
	
	
}
if(isset($_REQUEST['cancel']) && $_REQUEST['cancel'] == 'cancel')
{	
   
		header("location:Activeemployee.php");
	
}

include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet</h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
		
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<style>
.timesheet-table{
	margin: 5px;
    width: 83px;
}
.col-sm-12 {
    width: 100%;
    padding-bottom: 1%;
}
</style>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Timesheet</h3><?php echo constant("week_start_day");?>         
            </div>
              <form role="form" method="POST">
		            <div class="box-body">
		                
		                     <div class="col-sm-12">
                                <div class="col-sm-1">Date</div>
                                <div class="col-sm-1">Rostered_time</div>
                                <div class="col-sm-1">Ro_time_end</div>
                                <div class="col-sm-1">Actual_time</div>
                                <div class="col-sm-1">Act_end</div>
                                <div class="col-sm-1">Meal_brk</div>
                                <div class="col-sm-1">Meal_brk_end </div>
                                <div class="col-sm-1"> Before6am</div>
                                <div class="col-sm-1"> After10pm</div>
                                <div class="col-sm-1"> normalHour</div>
                                <div class="col-sm-1"> totalHour</div>
                              </div>
		            
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker1" name="datepicker1" value="<?php $date = new DateTime(); $date->modify('next '.constant("week_start_day")); echo $date->format('Y-m-d'); ?>" readonly/></div>
                                <div class="col-sm-1">
                                 <select class="timesheet-table" id="Rostered_time1" name="Rostered_time1" onchange="return removeDate(1);">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Rostered_time_end1" name="Rostered_time_end1" >
                                    	 <?php
                                         	$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time1" name="Actual_time1" onchange="return removeDateactual(1);">
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                               		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time_end1" name="Actual_time_end1" onchange="return actualendtime(1);">
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                               		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time1" name="Meal_break_time1">
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time_end1" name="Meal_break_time_end1" onchange="return calculation(1)">
                                			 <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="Before6am1" name="Before6am1" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="After10pm1" name="After10pm1" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour1" name="normalHour1" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour1" name="totalHour1" value="0" readonly /></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker2" name="datepicker2" value="<?php  $date->modify('next '.constant("week_day1")); echo $date->format('Y-m-d'); ?>"readonly/></div>
                                <div class="col-sm-1"> 								
                                <select id="Rostered_time2" name="Rostered_time2" class="timesheet-table" onchange="return removeDate(2);" ="return rosteedFocuss();">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                 <div class="col-sm-1"> 								
                                <select id="Rostered_time_end2" name="Rostered_time_end2" class="timesheet-table">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time2" name="Actual_time2" onchange="return removeDateactual(2);">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                          </select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time_end2" name="Actual_time_end2" onchange="return actualendtime(2);">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                          </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time2" name="Meal_break_time2">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                </select></div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time_end2" name="Meal_break_time_end2" onchange="return calculation(2)">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                </select></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="Before6am2" name="Before6am2" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="After10pm2" name="After10pm2" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour2" name="normalHour2" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour2" name="totalHour2" value="0" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker3" name="datepicker3" value="<?php $date->modify('next '.constant("week_day2")); echo $date->format('Y-m-d');  ?>" readonly/></div>
                                <div class="col-sm-1"> <select id="Rostered_time3" name="Rostered_time3" class="timesheet-table" onchange="return removeDate(3);">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                  <div class="col-sm-1"> <select id="Rostered_time_end3" name="Rostered_time_end3" class="timesheet-table">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                 </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time3" name="Actual_time3" onchange="return removeDateactual(3);">
                                				<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time_end3" name="Actual_time_end3" onchange="return actualendtime(3);">
                                				<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time3" name="Meal_break_time3">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time_end3" name="Meal_break_time_end3" onchange="return calculation(3)">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="Before6am3" name="Before6am3" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="After10pm3" name="After10pm3" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour3" name="normalHour3" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour3" name="totalHour3" value="0" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker4" name="datepicker4" value="<?php $date->modify('next '.constant("week_day3")); echo $date->format('Y-m-d'); ?>" readonly/></div>
                                <div class="col-sm-1"> <select id="Rostered_time4" name="Rostered_time4" class="timesheet-table" onchange="return removeDate(4);">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="Rostered_time_end4" name="Rostered_time_end4" class="timesheet-table">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time4" name="Actual_time4" onchange="return removeDateactual(4);">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time_end4" name="Actual_time_end4" onchange="return actualendtime(4);">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                		</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time4" name="Meal_break_time4">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                        </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time_end4" name="Meal_break_time_end4" onchange="return calculation(4)">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                        </select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="Before6am4" name="Before6am4" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="After10pm4" name="After10pm4" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour4" name="normalHour4" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour4" name="totalHour4" value="0" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker5" name="datepicker5" value="<?php $date->modify('next '.constant("week_day4")); echo $date->format('Y-m-d');  ?>" readonly/></div>
                                <div class="col-sm-1"> <select id="Rostered_time5" name="Rostered_time5" class="timesheet-table" onchange="return removeDate(5);">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="Rostered_time_end5" name="Rostered_time_end5" class="timesheet-table">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time5" name="Actual_time5" onchange="return removeDateactual(5);">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>		
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time_end5" name="Actual_time_end5" onchange="return actualendtime(5);">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>		
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time5" name="Meal_break_time5">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time_end5" name="Meal_break_time_end5" onchange="return calculation(5)">
                                			<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="Before6am5" name="Before6am5" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="After10pm5" name="After10pm5" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour5" name="normalHour5" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour5" name="totalHour5" value="0" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker6" name="datepicker6" value="<?php $date->modify('next '.constant("week_day5")); echo $date->format('Y-m-d');  ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                <select id="Rostered_time6" name="Rostered_time6" class="timesheet-table" onchange="return removeDate(6);">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="Rostered_time_end6" name="Rostered_time_end6" class="timesheet-table">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time6" name="Actual_time6" onchange="return removeDateactual(6);">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time_end6" name="Actual_time_end6" onchange="return actualendtime(6);">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time6" name="Meal_break_time6">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time_end6" name="Meal_break_time_end6" onchange="return calculation(6)">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="Before6am6" name="Before6am6" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="After10pm6" name="After10pm6" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour6" name="normalHour6" value="0" readonly onblur="return addtosum();"/></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour6" name="totalHour6" value="0" readonly/></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="datepicker7" name="datepicker7" value="<?php  $date->modify('next '.constant("week_end_day")); echo $date->format('Y-m-d');  ?>" readonly/></div>
                                <div class="col-sm-1"> 
                                <select id="Rostered_time7" name="Rostered_time7" class="timesheet-table" onchange="return removeDate(7);">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select id="Rostered_time_end7" name="Rostered_time_end7" class="timesheet-table">
                                    	  <?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time7" name="Actual_time7" onchange="return removeDateactual(7);">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                 <div class="col-sm-1"> <select class="timesheet-table" id="Actual_time_end7" name="Actual_time_end7" onchange="return actualendtime(7);">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                    </select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time7" name="Meal_break_time7">
                                		<?php
												$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <select class="timesheet-table" id="Meal_break_time_end7" name="Meal_break_time_end7" onchange="return calculation(7)">
                                		<?php
											$now = new DateTime('00:00');
												$end = clone $now;
												$end->modify("+24 hours");
												
												while ($now <= $end) {
													echo "<option value='".$now->format('g:i a')."'>" . $now->format('g:i a'). "</option>";
													$now->modify('+15 minutes');
												}
												?>
                                	</select>
                                </div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="Before6am7" name="Before6am7" value="0" readonly /></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="After10pm7" name="After10pm7" value="0" readonly /></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="normalHour7" name="normalHour7" value="0" readonly /></div>
                                <div class="col-sm-1"> <input type="text" class="timesheet-table" id="totalHour7" name="totalHour7" value="0" readonly/></div>
                              </div>
                             
                              <div class="col-sm-12"><hr>
                               <button type="submit" class="btn-primary" name="cancel" value="cancel">cancel</button>
                                <button type="submit" class="btn-primary" name="Submit" value="Submit" >Submit</button>
                              </div>
                              <input type="hidden" value="<?php echo $_REQUEST['EmpId']; ?>" name="fk_employee_id" id="fk_employee_id"/>
						                          
		            </div>
		        </form>         
     </div>
 </div>
</div>
</section>
<script language="javascript">
 $(function() {});
  
	$("#Rostered_time_end1").val('12:15 am');
	$("#Rostered_time_end2").val('12:15 am');
	$("#Rostered_time_end3").val('12:15 am');
	$("#Rostered_time_end4").val('12:15 am');
	$("#Rostered_time_end5").val('12:15 am');
	$("#Rostered_time_end6").val('12:15 am');
	$("#Rostered_time_end7").val('12:15 am');
	$("#Actual_time_end1").val('12:15 am');
	$("#Actual_time_end2").val('12:15 am');
	$("#Actual_time_end3").val('12:15 am');
	$("#Actual_time_end4").val('12:15 am');
	$("#Actual_time_end5").val('12:15 am');
	$("#Actual_time_end6").val('12:15 am');
	$("#Actual_time_end7").val('12:15 am');

function removeDate(position){
	 var rt1 = $("#Rostered_time"+position).prop('selectedIndex');
	 var rte1 = document.getElementById("Rostered_time_end"+position);
	 var i = rt1;
	 
	 for( i = rt1 ; i >= 0 ; i--){
		 //alert(x.options[i].value);
		 document.getElementById("Rostered_time_end"+position).options[i].disabled = true;
	 	//x.remove(i);
	 }
	 $("#Rostered_time_end"+position).val(rte1.options[rt1+1].value);
}

function befor6am(startt , endt){
	
	var befor6am = 0;
	 if(startt < 24 && endt <= 24){
	 		befor6am =  ( endt - startt )/4;
	 }else if(startt < 24 && endt > 24){
	 		befor6am = (24 - startt)/4;	
	 }	
	 
	return befor6am;
	
}

function after10pm(startt , endt){
	
	var after10pm = 0;
	//alert(startt + '-' + endt)
	 if(startt >= 88 && endt > 88){
	 		after10pm =  ( endt - startt )/4;
	 }else if(startt < 88 && endt > 88){
	 		after10pm = (endt - 88)/4;	
	 }	
	return after10pm;
		
}
function normalHour(startts , endtt , mstart , mend , ii , aftr){
	var normalHour=0;
	var diff = (endtt - startts)/4 ;
	var milbreake = (mend - mstart)/4;
	
	var bforAftr = Number(document.getElementById("Before6am"+ii).value) + Number(aftr);
	var diffbeforafter = diff - bforAftr ; 
	
	normalHour = diffbeforafter - milbreake ;
	if(normalHour < 0 ){normalHour = 0;}
	return normalHour;
}

function totalHour(normalHour , befor , after ){
	var totalHour = 0;
	
	totalHour = Number(normalHour) + Number(befor) + Number(after);
	
	return totalHour;

}

function rosteedFocuss(){
	if($("#Meal_break_time1").prop('selectedIndex') < $("#Actual_time1").prop('selectedIndex') || $("#Meal_break_time_end1").prop('selectedIndex') > $("#Actual_time_end1").prop('selectedIndex'))
	{
		document.getElementById("Meal_break_time1").focus();
		return false;
	}
}

function actualendtime(posiion){
	document.getElementById("Before6am"+posiion).value = befor6am( $("#Actual_time"+posiion).prop('selectedIndex') , $("#Actual_time_end"+posiion).prop('selectedIndex') );
	
	document.getElementById("After10pm"+posiion).value = after10pm( $("#Actual_time"+posiion).prop('selectedIndex') , $("#Actual_time_end"+posiion).prop('selectedIndex') );
	
	document.getElementById("normalHour"+posiion).value = normalHour( $("#Actual_time"+posiion).prop('selectedIndex') , $("#Actual_time_end"+posiion).prop('selectedIndex') , $("#Meal_break_time"+posiion).prop('selectedIndex') , $("#Meal_break_time_end"+posiion).prop('selectedIndex'),1,document.getElementById("After10pm"+posiion).value);
	
	document.getElementById("totalHour"+posiion).value = totalHour(document.getElementById("normalHour"+posiion).value , document.getElementById("After10pm"+posiion).value , document.getElementById("Before6am"+posiion).value);
	
	
	 var at1 = $("#Actual_time_end"+posiion).prop('selectedIndex');
	  var i = at1;
	 for( i = at1 ; i <= 96 ; i++){
		 document.getElementById("Meal_break_time_end"+posiion).options[i].disabled = true;
	 	//x.remove(i);
	 }
	 
	
}

function calculation(posiion){
	var mealHour = ($("#Meal_break_time_end"+posiion).prop('selectedIndex') - $("#Meal_break_time"+posiion).prop('selectedIndex'))/4;
	document.getElementById("totalHour"+posiion).value = Number(document.getElementById("totalHour"+posiion).value) - Number(mealHour);  
	
}

function removeDateactual(positin){
	 var at1 = $("#Actual_time"+positin).prop('selectedIndex');
	 var ate1 = document.getElementById("Actual_time_end"+positin);
	 var i = at1;
	 
	 for( i = at1 ; i >= 0 ; i--){
		 //alert(x.options[i].value);
		 document.getElementById("Actual_time_end"+positin).options[i].disabled = true;
		 document.getElementById("Meal_break_time"+positin).options[i].disabled = true;
		 document.getElementById("Meal_break_time_end"+positin).options[i].disabled = true;
	 	//x.remove(i);
	 }
	 $("#Actual_time_end"+positin).val(ate1.options[at1+1].value);
	 $("#Meal_break_time"+positin).val(ate1.options[at1+1].value);
	 $("#Meal_break_time_end"+positin).val(ate1.options[at1+1].value);
	 
	 document.getElementById("Before6am"+positin).value = befor6am( $("#Actual_time"+positin).prop('selectedIndex') , $("#Actual_time_end"+positin).prop('selectedIndex') );

	document.getElementById("After10pm"+positin).value = after10pm( $("#Actual_time"+positin).prop('selectedIndex') , $("#Actual_time_end"+positin).prop('selectedIndex') );

}
function checkTime(){
	if($("#Actual_time1").prop('selectedIndex') > $("#Actual_time1").prop('selectedIndex')){
		alert();
	}
	return false;
}


</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>