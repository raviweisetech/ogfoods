<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

$date = new DateTime();
$week_strt_day = constant("week_start_day");
$yy = date('l');
if($yy == $week_strt_day ){
	$to_date = strtotime(date('Y-m-d'));
	$date1 = date('Y-m-d', strtotime("today",$to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));
}else{
	$date1 = date('Y-m-d', strtotime("last ".constant("week_start_day")));
	$to_date = strtotime($date1);
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));	
}

include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet Payroll</h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
	
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employees Payroll</h3>          
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                   <th>Store id</th>
                        <th>Emp Name</th>
                        <th>Week Start</th>
                        <th>Week End</th>
                        <th>Before 6am</th>
                        <th>After 10pm</th>
                        <th>Saturday Hours</th>
                        <th>Sunday Hours</th>
                        <th>Normal Total Hours</th>
                        <th>Weekly Total Hours</th>
                        <th>Number Of Shifts</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                	//print_r(mysqli_fetch_assoc($oEmp_DA->Emp_Select()));die;
                	$result = $oEmp_DA->Emp_all_payroll($date1 , $date7);
					$before6am_total_hours = 0;
					$after10pm_total_hours = 0;
					$saturday_hours = 0;
					$sunday_hours = 0;
					$normal_total_hours = 0;
					$weekly_total_hours = 0;
					
                	while($row = mysqli_fetch_assoc($result))
                	{ 
						$user_name = get_emp_name($row['fk_employee_id']);
					?>
                		<tr>
					    <td><?php echo $row['fk_store_id']; ?></td>
                        <td><?php echo $user_name['em_first_name'].' '.$user_name['em_family_name']; ?></td>
                        <td><?php echo $row['week_start_date']; ?></td>
                        <td><?php echo $row['week_end_date']; ?></td>
                        <td><?php echo $row['before6am_total_hours']; $before6am_total_hours = $before6am_total_hours + $row['before6am_total_hours'];?></td>
                        <td><?php echo $row['after10pm_total_hours']; $after10pm_total_hours = $after10pm_total_hours + $row['after10pm_total_hours'];?></td>
                        <td><?php echo $row['saturday_hours']; $saturday_hours = $saturday_hours + $row['saturday_hours'];?></td>
                        <td><?php echo $row['sunday_hours']; $sunday_hours = $sunday_hours + $row['sunday_hours'];?></td>
                        <td><?php echo $row['normal_total_hours']; $normal_total_hours = $normal_total_hours + $row['normal_total_hours']; ?></td>
                        <td><?php echo $row['weekly_total_hours']; $weekly_total_hours = $weekly_total_hours + $row['weekly_total_hours'];?></td>
                        <td><?php echo $row['number_of_shifts']; ?></td>
						</tr>
             <?php  } ?>
         		</tbody>
                <tfoot>
                	<th>Total</th>
                    <th></th>
                    <th></th>
                    <th><?php echo '';?></th>
                    <th><?php echo $before6am_total_hours;?></th>
                    <th><?php echo $after10pm_total_hours;?></th>
                    <th><?php echo $saturday_hours;?></th>
	                <th><?php echo $sunday_hours;?></th>
                    <th><?php echo $normal_total_hours;?></th>
                    <th><?php echo $weekly_total_hours;?></th>
                    <th><?php echo '';?></th>
                </tfoot>
         	</table>
         </div>
     </div>
 </div>
</div>
</section>
<script language="javascript">
var i = 1;
function change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}
function resign_change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=res&eid="+eid+"&ems="+empstatus+"";
}
function datechange(datas)
{
	if(i == 1)
	{
		var del=confirm("Are you sure want to resign this employee?");
		if (del==true){
			window.location.href="Activeemployee.php?mode=res&eid="+datas.id+"&ems=2&resign_date="+datas.value;
		 //  alert ("Activeemployee.php?mode=res&eid="+datas.id+"&ems=2&resign_date="+datas.value)
		}else{
			//break;
		}
	}else if(i == 3)
	{
		i = 0;	
	}
	i++;
}
</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>