<?php
ob_start();
session_start();
	
if(!isset($_SESSION['varUserName']))
{
	header('Location:Login.php');
}
require_once("include/clsInclude.php");
$oUser_DA = new clsUser_DA();
$user_detail = $oUser_DA->count_records();

if(isset($_POST['submit']))
{
  $user_data = $oUser_DA->profile_update($_POST);

  if($user_data)
  {
    header('Location: Profile.php'); 
  }
  else
  {
    echo "Your data can't Updated";
  }
}
include('header.php'); 
?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Update Profile </h1> </section>
</div>
<br><br>
<section class="content">
    <div class="row">   
    <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
                <div class="box-body">
                  <div class="form-group">
                    <label>First Name:</label>
                    <input name="f_name" type="text" id="f_name" class="input form-control" tabindex="1" value="<?php echo $user_detail['first_name']; ?>" required="true">
                  </div>
                  <div class="form-group">
                    <label>Last Name:</label>
                    <input name="l_name" type="text" id="l_name" class="input form-control" tabindex="2" value="<?php echo $user_detail['last_name']; ?>" required="true">
                  </div>
                  <div class="form-group">
                    <label>Username:</label>
                    <input name="username" type="text" id="username" class="input form-control" tabindex="3" value="<?php echo $user_detail['username']; ?>" required="true">
                  </div>
                  <div class="form-group">
                    <label>E-mail:</label>
                    <input name="email" type="email" id="email" class="input form-control" tabindex="4" value="<?php echo $user_detail['email_address']; ?>" required="true">
                  </div>
                  <input type="hidden" name="id" id="id" value="<?php echo $user_detail['id']; ?>">
                  <div class="form-group" align="center">
                    <input type="submit" name="submit" id="submit" value="Update Profile">
                  </div>
                </div>
            </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>    
<?php include('footer.php'); ?>
<?php ob_flush();?>