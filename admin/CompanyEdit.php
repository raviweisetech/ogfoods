<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oCompany_CDO = new clsCompany_CDO();
$oCompany_DA = new clsCompany_DA();

if(isset($_POST['submit']))
{
	$user_data = $oCompany_DA->Company_Edit($_POST);

	if($user_data)
	{
		header('Location: Managecompany.php');	
	}
	else
	{
		echo "Your data can't Updated";
	}
}
else
{
	if($_GET['id'])
	{
		$company_detail = $oCompany_DA->Company_Detail($_GET['id']);
	}
}
?>

<?php include('header.php');?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Update Company Detail </h1> </section>
</div>
<br><br>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<br>
					<form role="form" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label>Company Name:</label>
								<input name="companyname" type="text" id="companyname" class="input form-control" tabindex="1" value="<?php echo $company_detail['company_name'] ?>" required="true">
							</div>
							<div class="form-group">
		    		          <label>ABN Number:</label>
				              <input name="abnnumber" type="text" id="abnnumber" class="input form-control" tabindex="2" value="<?php echo $company_detail['abn_number'] ?>" required="true">
		            		</div>
		            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Update Company" onclick="return chk(this);">
	            			</div>
	            			<input type="hidden" name="id" id="id" value="<?php echo $company_detail['id'] ?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script language="javascript">

	function chk(form)
	{
		if(document.getElementById("companyname").value=='')
		{
			alert("Please Enter Company Name");
			document.getElementById("companyname").focus();
			return false;
		}
		if(document.getElementById("abnnumber").value=='')
		{
			alert("Please Enter ABN Number");
			document.getElementById("abnnumber").focus();
			return false;
		}
	}
</script>
<?php  include('footer.php');?>
<?php ob_flush();?>