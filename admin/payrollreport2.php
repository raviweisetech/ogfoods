<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

$date = new DateTime();
$week_strt_day = constant("week_start_day");
$yy = date('l');
if($yy == $week_strt_day ){
	$to_date = strtotime(date('Y-m-d'));
	$date1 = date('Y-m-d', strtotime("today",$to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));
}else{
	$date1 = date('Y-m-d', strtotime("last ".constant("week_start_day")));
	$to_date = strtotime($date1);
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));	
}

if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] == 'Submit')
{	
	$std =  $_POST['fk_week_start'];
	$storeid = $_POST['sesstion_id'];
	
	header("location:payroll_report_export.php?st=$std&storeid=$storeid");
	
}

include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet Payroll</h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
	
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
	<form name="frmManagePayroll" method="post" action="" >
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employees Payroll</h3>    
               <button type="submit" name="Submit" value="Submit" class="mylink btn btn-primary" style="float: right; padding-left: 10px;"> Download Report</button>  
               <button  class="mylink btn btn-primary" onclick="printCon('printtable');" style="float: right; padding-left: 10px;margin-right: 8px;"> Print Reports</button> 
            </div>
            <div class="box-body">
              <div class="input form-group">
              <input type="hidden" value="<?php echo $_SESSION['varStore']; ?>" name="sesstion_id" id="sesstion_id"/>
              <input type="hidden" value="<?php echo $date1; ?>" name="this_week" id="this_week"/>
              
	    		          <label>Select Week Start Date:</label>
	    		          <select class="form-control" name="fk_week_start" id="fk_week_start" tabindex="1" required="true" onchange="return datechange(this.value);">
	    		          	<option>--Select Date--</option>
	    		           <?php 
						  		$result = $oEmp_DA->Emp_Select_payroll();
								while($row = mysqli_fetch_assoc($result))
								{ 
						   ?>
	    		          		<option value="<?php echo $row['week_start_date']; ?>"<?php if($row['week_start_date'] == $date1){echo "selected";} ?>><?php echo $row['week_start_date']; ?></option>
	    		          	
	    		          <?php } ?>
	    		          </select>
	            		</div>
	            		 <div class="table-responsive">
                              <table id="example1" class="table table-bordered table-striped">
                              	<thead>
                                  <tr>
                                        <th>Emp Name</th>
                                        <th>Before 6am</th>
                                        <th>After 10pm</th>
                                        <th>Saturday Hours</th>
                                        <th>Sunday Hours</th>
                                        <th>Public Holiday Hours</th>
                                        <th>Sick Leave Hours</th>
                                        <th>Annual Leave Hours</th>
                                        <th>Normal Total Hours</th>
                                        <th>Weekly Total Hours</th>
                                        <th>Number Of Shifts</th>
                                  </tr>
                                </thead>
                                <tbody>
                               
                         		</tbody>
                                <tfoot>
                                	
                                </tfoot>
                         	</table>
         	            </div>
         </div>
     </div>
 </div>
</div>
</form>
<!-- print -->
	 <div class="box-body" id="printtable">
						<div class="hiddenn"><h1>
							<!--<img src="logo.png" width="80" height="60" /> </h1>-->
									
						</div><div class="hiddenn" style="float:right;"> Payroll Report</div>
              	 <table id="examplePrint" class="table table-bordered table-striped">
                              	<thead>
                                  <tr>
                                        <th>Emp Name</th>
                                        <th>Before 6am</th>
                                        <th>After 10pm</th>
                                        <th>Saturday Hours</th>
                                        <th>Sunday Hours</th>
                                        <th>Public Holiday Hours</th>
                                        <th>Sick Leave Hours</th>
                                        <th>Annual Leave Hours</th>
                                        <th>Normal Total Hours</th>
                                        <th>Weekly Total Hours</th>
                                        <th>Number Of Shifts</th>
                                  </tr>
                                </thead>
                                <tbody>
                               
                         		</tbody>
                                <tfoot>
                                	
                                </tfoot>
                         	</table>

              <div id="footerr" class="hiddenn"></div>
            </div>
<!-- print -->
</section>
<style>.hiddenn{ visibility:hidden; height:0px;}  th.tbl_data_dtl {  text-align: center;} #printtable{display:none}</style>
<script language="javascript" >
	function printCon(el)
	{
		var restorepage = document.body.innerHTML;
		var printcon = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcon;
		window.print();
		//document.body.innerHTML = restorepage;
		window.location.reload();
		
	}
</script>	
<script language="javascript">
var i = 1;
function change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}
function resign_change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=res&eid="+eid+"&ems="+empstatus+"";
}

 $(function() { datechange($("#this_week").val()); });
 
function datechange(datas)
{
	var table = $('#example1').DataTable();
	table.destroy();
	var ptable = $('#examplePrint').DataTable();
	ptable.destroy();
	var storeid = $("#selectedstore").val();
	document.getElementById("sesstion_id").value = storeid;
	
	$('#example1').DataTable( {
		"processing": true,
		"serverSide": true,
		"stateSave": true,
		"bRetrieve": true,
		"oLanguage": {
      "sLengthMenu": 'Show <select>'+
        '<option value="10">10</option>'+
        '<option value="20">20</option>'+
        '<option value="30">30</option>'+
        '<option value="40">40</option>'+
        '<option value="100">100</option>'+
        '<option value="-1">All</option>'+
        '</select> Entries Per Page'
    },
		"ajax":{
			url :"datatable-ajax-payroll.php",
			data : {ms:datas , storeid:storeid},
			type: "post",
			error: function(){
				$(".table-grid-error").html("");
				$("#example1").append('<tbody class="table-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#example1_processing").css("display","none");
			}
		}
	} );
	$('#examplePrint').DataTable( {
		"processing": true,
		"serverSide": true,
		"stateSave": true,
		"bRetrieve": true,
		"oLanguage": {
      "sLengthMenu": 'Show <select>'+
        '<option value="10">10</option>'+
        '<option value="20">20</option>'+
        '<option value="30">30</option>'+
        '<option value="40">40</option>'+
        '<option value="100">100</option>'+
        '<option value="-1">All</option>'+
        '</select> Entries Per Page'
    },
		"ajax":{
			url :"datatable-ajax-payroll.php",
			data : {ms:datas , storeid:storeid},
			type: "post",
			error: function(){
				$(".table-grid-error").html("");
				$("#example1").append('<tbody class="table-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#example1_processing").css("display","none");
			}
		}
	} );

}
</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>