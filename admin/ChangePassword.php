<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oUser_DA = new clsUser_DA();
					//	print_r($oUser_DA->User_Select());exit();
if(isset($_POST['submit']))
{
	$user_pass = $oUser_DA->Change_Password($_POST);
		
	if($user_pass == 1)
	{
		header('Location: Home.php');	
	}
	else if($user_pass == 0)
	{
		echo "New Password is same as Current Password.";
	}
	else if($user_pass == 2)
	{
		echo "New Password does't Match With Confirm Password.";
	}
	else
	{
		echo "500 INTERNAL SERVER ERROR  !!";
	}
}

include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Change Password </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	<div class="form-group">
	    		          <label>Current Password:</label>
			              <input name="cur_pass" type="password" id="cur_pass" class="input form-control" tabindex="1" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>New Password:</label>
			              <input name="new_pass" type="password" id="new_pass" class="input form-control" tabindex="2" required="true">
	            		</div>
	            		<div class="form-group">
	    		          <label>Confirm Password:</label>
			              <input name="con_pass" type="password" id="con_pass" class="input form-control" tabindex="3" required="true">
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Change Password">
	            		</div>
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<?php include('footer.php'); ?>
<?php ob_flush();?>