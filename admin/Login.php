<?php 
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(E_ALL);

require_once("include/clsInclude.php");

//$_SESSION['ref'] = $_SERVER['PHP_SELF'];
/*if(!isset($_SESSION['varUserName']))
{
	header('Location:../main/frmLogin.php');
	header("location:$temp");
}*/
/*if(isset($_SESSION['ref']))	
	{
		$temp=$_SESSION['ref'];
		//unset($_SESSION['ref']);
		header("location:$temp");
	}
	else
	{
		header("location:frmLogin.php");
	}*/
if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] == 'Submit') {
	$obj_login = new clsLogin_DA();
	$obj_login_CDO = new clsLogin_CDO();

	$obj_login_CDO->uname = $_REQUEST['uname'];

	$obj_login_CDO->password = $_REQUEST['password'];

  $duplicate = $obj_login->Check_Login($obj_login_CDO);
	if($duplicate == 1) {
	  header("Location:Login.php?msg=Invalid");
    exit;
	}
	else {
	  header("Location:Home.php");
    exit;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Login</title>

<!-- <link href="css/style.css" rel="stylesheet" type="text/css" /> -->

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="plugins/iCheck/square/blue.css">

<script type="text/javascript" language="javascript">
function check() {
	if(trim(document.getElementById('uname').value) == '') {
		alert('Please Enter Username');
		document.getElementById('uname').focus();
		return false;
	}
	if(trim(document.getElementById('password').value) == '') {
		alert('Please Enter Password');
		document.getElementById('password').focus();
		return false;
	}
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
</script>
</head>

<body class="hold-transition login-page">
    <?php if(isset($_REQUEST['msg']) && trim($_REQUEST['msg'])=='Invalid') { ?>
      <table width="40%" border="0" align="center">
        <tr>
          <td class="msg1"><?php echo"Invalid UserName or Password"; ?></td>
        </tr>
      </table>
    <?php } ?>
    <div class="login-box">
      <div class="login-box-body">
        <p class="login-box-msg">Admin Login</p>

        <form name="login" method="post" action="">
          <div class="form-group has-feedback">
            <input class="textfield form-control" name="uname" type="text" id="uname" placeholder="Please enter your username">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input class="textfield form-control" name="password" type="password" id="password"  placeholder="Please enter your password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button class="btn btn-primary btn-block btn-flat" type="submit" name="Submit" value="Submit" onClick="return check();"> Sign In</button>
            </div>
          </div>
        </form>
        <a href="ForgotPassword.php" class="mylink">Forgot Password</a>
      </div>
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
<?php ob_flush(); ?>