<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");

$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] =='Submit')
{



	$offer_letter = $oEmp_DA->Update_Offer_Letter($_POST);

	if($offer_letter == 1)
	{
		header('Location: uploademployeeofferletter.php');
	}
	else
	{
		echo "Your data can't insert";
	}
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<!-- <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<section class="content-header col-md-6"> <h1> Employee Offer letter </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST" enctype="multipart/form-data" >
		            <div class="box-body">


	            		<div class="input form-group">
	    		          <label>Select Employee:</label>
	    		          <select class="form-control" name="employee_id" id="employee_id" tabindex="1" required="true">
	    		          	<option>--Select Employee--</option>
	    		           <?php
						  		$result = $oEmp_DA->Emp_Select();
								while($row = mysqli_fetch_assoc($result))
								{
									if($row['employee_status']== "1")
									{
						   ?>
	    		          		<option value="<?php echo $row['em_id']; ?>"><?php echo $row['em_first_name']." ".$row['em_family_name']; ?></option>

	    		          <?php }} ?>
	    		          </select>
	            		</div>


	            		<div class="form-group">
                         <label>Upload Offer Letter:</label>
	    		         <input type="file" accept="application/pdf" name="offer_letter" id="offer_letter">
	            		</div>



	            		<div class="form-group" align="center">
	            			<input type="submit" class="btn btn-primary" name="Submit" value="Submit" value="Upload Offer Letter">
	            		</div>


		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>

  <script type="text/javascript">

  </script>
  <script type="text/javascript">
        datepicker( '.datepicker' );

        </script>
<?php include('footer.php'); ?>
<?php ob_flush();?>
