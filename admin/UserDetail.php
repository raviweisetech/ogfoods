<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oUser_CDO = new clsUser_CDO();
$oUser_DA = new clsUser_DA();

$oStore_DA = new clsStore_DA();
	//	print_r($oUser_DA->User_Select());exit();

if($_GET['id'])
{
	
	$user_detail = $oUser_DA->User_Detail($_GET['id']);
	//print_r($user_detail);
	//$Store_detail = $oStore_DA->Select_Store_User($_GET['id']);
	//print_r($Store_detail);exit;
	
	/*while($row = mysqli_fetch_array($Store_detail))
	{
		//echo $row['id'];
		$store = $oStore_DA->Store_Detail($row['fk_store_id']);
		//echo $store; exit;
		//echo $store['st_name']; exit;

		//echo $store; exit;

		
	}*/
	
	

}



//Edit user start
if(fnRequestParam('mode') == 'us') {
	$oUser_CDO->id = fnRequestParam('uid');
	$oUser_CDO->us = fnRequestParam('us');
	$oUser_DA->User_Status($oUser_CDO);
	header("Location:User.php?msg=us");
	exit;
}
/*Edit user End*/

/*Delete user Data start*/
if(fnRequestParam('mode') == 'Delete') {
	$oUser_CDO->id = fnRequestParam('id');
	$oUser_DA->User_Delete($oUser_CDO);
	header("Location:User.php?msg=Del");
	exit;	
}

/*Delete user data end*/
if(isset($_REQUEST['search_submit'])) {
	unset($_REQUEST['msg']);
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> View User Detail </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User</h3>
              <br>
	            <div class="box-body col-md-6">
	              <table id="example" class="table table-bordered table-striped">
	              		<tr>
							<td class="col-md-3"><b>Store Name</b></td>
							<td class="col-md-3">
								<?php
								foreach ($user_detail['store_name'] as $st_name) {
									echo $st_name.'<br>';
								}
								 ?>
									
							</td>
						</tr>
						<tr>
							<td class="col-md-3"><b>First Name</b></td><td class="col-md-3"><?php echo $user_detail['first_name'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Last Name</b></td><td class="col-md-3"><?php echo $user_detail['last_name'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Username</b></td><td class="col-md-3"><?php echo $user_detail['username'] ?></td>
						</tr>						
						<tr>
							<td class="col-md-3"><b>Email Address</b></td><td class="col-md-3"><?php echo $user_detail['email_address'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Password</b></td><td class="col-md-3"><?php echo $user_detail['password'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>User Type</b></td><td class="col-md-3"><?php echo $user_detail['user_type'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>User Status</b></td><td class="col-md-3"><?php echo $user_detail['user_status'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Created Date</b></td><td class="col-md-3"><?php echo $user_detail['create_date'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>User Status</b></td><td class="col-md-3"><?php echo $user_detail['modified_date'] ?></td>
						</tr>
	              </table>
	            </div>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<?php include('footer.php'); ?>
<?php ob_flush();?>