<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
require_once("include/clsInclude.php");
$oSetting_CDO = new clsSetting_CDO();
$oSetting_DA = new clsSetting_DA();

if(isset($_POST['submit']) )
{	
	$exe=$oSetting_DA->Setting_create($_POST);

	if($exe==true)
	{	
		header('Location:Managesettings.php?msg=1');
		// "Insert success"
	}
	else
	{
		header('Location:Managesettings.php?msg=2');
		// "Insert fail"
	}
}

include('header.php');  
?>

<div class="col-md-12">
	<section class="content-header col-md-6"> <h1> Manage Setting </h1> </section>
</div>
<br><br>
<section class="content"> 
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
              		<br>
              		<form role="form" method="POST">
              			<div class="box-body">
              				<div class="form-group">
	    		          		<label>Payroll Email Address:</label>
			              		<input name="payrollemailaddress" type="text" id="payrollemailaddress" class="input form-control" tabindex="1" required>
	            				</div>
	            				<div class="form-group" align="center">
	            				<input type="submit" name="submit" id="submit" value="Submit"  onclick="return chk(this);">
	            			</div>
              			</div>
              		</form>
            	</div>
			</div>
		</div>
	</div>
</section>
<script language="javascript">
	function chk(form)
	{
		if(document.getElementById("payrollemailaddress").value=='')
		{
			alert("Please Enter Payroll Email Address");
			document.getElementById("payrollemailaddress").focus();
			return false;
		}
	}
</script>
<?php include('footer.php'); 
if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == '1') {
     ?>
     <script type="text/javascript">alert("Record added successfully");
     location.replace("Managesettings.php");

 	</script>
     <?php
    }
    if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == '2') {
     ?>
     <script type="text/javascript">alert("Something went wrong");
     location.replace("Managesettings.php");
     
 	</script>
     <?php
    }
?>
<?php ob_flush();?>