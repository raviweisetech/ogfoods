	</div>
	<footer class="main-footer">
	    <div class="pull-right hidden-xs">
		    <b>Version</b> 2.3.6
	    </div>
	    <?php $year = date('Y'); ?>
	    <strong>Copyright &copy; <?php echo $year?> By OgFoods.</strong> All rights reserved.
	</footer>
	<div class="control-sidebar-bg"></div>
</div>

<script src="js/adminprofile.js"></script>

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- date-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- CK Editor-->
<script src="plugins/ckeditor/ckeditor.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<link rel="stylesheet" href="plugins/datepicker/bootstrap-datepicker.min.css" />
<script>
  $(function () {
   // $("#example1").DataTable({"ordering": false});
    $('#example3').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

    $('#nodata').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": false
    });


	 $(".resign_date").datepicker({
    format: 'yyyy-mm-dd',autoclose: true,cursor: 'pointer'  });
   $(".start_date").datepicker({
    format: 'yyyy-mm-dd',autoclose: true,cursor: 'pointer'  });
   $(".end_date").datepicker({
    format: 'yyyy-mm-dd',autoclose: true,cursor: 'pointer'  });
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('txttournamentdesc')
  });
</script>

</body>
</html>