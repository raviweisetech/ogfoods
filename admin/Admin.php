<?php
ob_start();
session_start();
require_once("include/clsInclude.php");
	
if(!isset($_SESSION['varUserName']))
{
	header('Location:Login.php');
}
$_SESSION['ref'] = $_SERVER['PHP_SELF'];
$oclsAdmin_DA= new clsAdmin_DA();
$oclsAdmin_CDO= new clsAdmin_CDO();

$oclsAdmin_DA->count_records();

if(isset($_REQUEST['Update']) && trim($_REQUEST['Update'])=='Update') {
  $oclsAdmin_CDO->intAdminID=$oclsAdmin_DA->intAdminID;
  $oclsAdmin_CDO->txtEmail=trim($_REQUEST['txtEmail']);
  $oclsAdmin_CDO->txtPassword=trim($_REQUEST['txtPassword']);
  $oclsAdmin_CDO->txtFullname=trim($_REQUEST['txtFullname']);
  $oclsAdmin_CDO->txtUsername=trim($_REQUEST['txtUsername']);

  $oclsAdmin_DA->Edit_record($oclsAdmin_CDO);
}
?>
<?php include('header.php'); ?>
<section class="content-header"> <h1> Admin Panel </h1> </section>
<?php if(isset($_REQUEST['Msg']) && trim($_REQUEST['Msg']) == 'Edit') { ?>
<div class="alert alert-info alert-dismissible">
  Your Profile Updated Successfully.
</div>
<?php } 
if(isset($_REQUEST['Msg']) && trim($_REQUEST['Msg']) == 'Add') {
?>
<div class="alert alert-info alert-dismissible">
  Profle Information Added Successfully.
</div>
<?php } ?>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Profile</h3>
        </div>
        <form role="form" method="POST">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Email:</label>
              <input name="txtEmail" type="text" id="txtEmail" value="<?php echo $oclsAdmin_DA->txtEmail; ?>"  class="input form-control" tabindex="1" size="45">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password:</label>
              <input name="txtPassword" type="password" id="txtPassword" value="<?php echo $oclsAdmin_DA->txtPassword; ?>" class="input form-control" tabindex="3" size="30">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Full Name:</label>
              <input name="txtFullname" type="text" id="txtFullname" value="<?php echo $oclsAdmin_DA->txtFullname; ?>" class="input form-control" tabindex="2" size="30">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">User Name:</label>
              <input name="txtUsername" type="text" id="txtUsername" value="<?php echo $oclsAdmin_DA->txtUsername; ?>" class="input form-control" tabindex="4" size="45">
            </div>
          </div>

          <div class="box-footer">
            <input name="Update" type="submit" id="Update" class="btn btn-primary" tabindex="8" value="Update" onClick="return check();">
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php include('footer.php'); ?>
<?php ob_flush();?>