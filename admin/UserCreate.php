<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oUser_CDO = new clsUser_CDO();
$oUser_DA = new clsUser_DA();
					//	print_r($oUser_DA->User_Select());exit();
if(isset($_POST['submit'])) 	
{
	$user_data = $oUser_DA->User_Create($_POST);
	
	
	
	
	if($user_data)
	{
		header('Location: User.php');	
	}
	else
	{
		echo "Your data can't added";
	}
}

//Edit user start
if(fnRequestParam('mode') == 'us') {
	$oUser_CDO->id = fnRequestParam('uid');
	$oUser_CDO->us = fnRequestParam('us');
	$oUser_DA->User_Status($oUser_CDO);
	header("Location:User.php?msg=us");
	exit;
}
/*Edit user End*/

/*Delete user Data start*/
if(fnRequestParam('mode') == 'Delete') {
	$oUser_CDO->id = fnRequestParam('id');
	$oUser_DA->User_Delete($oUser_CDO);
	header("Location:User.php?msg=Del");
	exit;	
}

/*Delete user data end*/
if(isset($_REQUEST['search_submit'])) {
	unset($_REQUEST['msg']);
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Create User </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	<div class="form-group">
	    		          <label>First Name:</label>
			              <input name="f_name" type="text" id="f_name" class="input form-control" tabindex="1" required>
	            		</div>
		            	<div class="form-group">
	    		          <label>Last Name:</label>
			              <input name="l_name" type="text" id="l_name" class="input form-control" tabindex="2" required>
	            		</div>
	            		<div class="form-group">
	    		          <label>Username:</label>
			              <input name="username" type="text" id="username" class="input form-control" tabindex="3" required>
	            		</div>
		            	<div class="form-group">
	    		          <label>E-mail:</label>
			              <input name="email" type="email" id="email" class="input form-control" tabindex="4" required>
	            		</div>
		            	<div class="form-group">
	    		          <label>Password:</label>
			              <input name="password" type="text" id="password" class="input form-control" tabindex="5" required>
	            		</div>
		            	<div class="input form-group">
	    		          <label>User Type:</label>
	    		          <select class="form-control" name="u_type" id="u_type" tabindex="6" required="true">
	    		          	<option value="0">Super Admin</option>
	    		          	<option value="1">Store Manager</option>
	    		          	<option value="2">Assistant Manager</option>
	    		          </select>
	            		</div>
	            		<div class="form-group">
	    		          <label>User Status:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="u_status" value="1" id="u_status"> Active
	    		          	&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="u_status" value="0" id="u_status"> Deactive
	            		</div>
                        <div class="form-group">
	    		          <label>Store to Allow:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <?php  
								$oStore_DA = new clsStore_DA();
								$result = $oStore_DA->All_Store();
								 while($row = mysqli_fetch_assoc($result))
								 {?>
	    		          	<input type="checkbox"  name="store[]" id="store" value="<?php echo $row['id'];?>"><?php echo " ".$row['st_name'];?>
                           <?php } ?>
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Submit">
	            		</div>
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<?php include('footer.php'); ?>
<?php ob_flush();?>