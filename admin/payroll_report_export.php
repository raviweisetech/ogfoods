<?php

ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
$DateOfRequest = date("Y-m-d-H:i:s");
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename= PayrollReport-for-'.$_REQUEST['st'].'-on-'.$DateOfRequest.'.csv') ;
	$output = fopen("php://output", "w");
	fputcsv($output ,array('Emp Name','Before 6am','After 10pm','Saturday Hours','Sunday Hours','Public Holiday Hours','Sick Leave Hours','Normal Total Hours','Weekly Total Hour','Number Of Shifts'));
	$st= $_REQUEST['st']; // Stratting Date

	$to_date = strtotime($st);
	$endDate = date('Y-m-d', strtotime("+6 day",$to_date));

	$empid = '';
	if(isset($_REQUEST['emplid']) != '')
	{
		$empid = "and fk_employee_id = '".$_REQUEST['emplid']."' ";
	}

	$filterQuery = " where fk_store_id = '".$_REQUEST['storeid']."' and week_start_date = '".$_REQUEST['st']."' and week_end_date = '".$endDate."' and `payroll_paid` = '0'  ".$empid."";;

	$data = array();

	$sql = "SELECT * from tbl_employee_timesheet_payroll ".$filterQuery;

	//fputcsv($output,array($sql));exit;
	global $link;
	$re= mysqli_query($link,$sql);
	$nr=mysqli_num_rows($re);

	if($nr){

	$before6am_total_hours = 0;
	$after10pm_total_hours = 0;
	$saturday_hours = 0;
	$sunday_hours = 0;
	$normal_total_hours = 0;
	$weekly_total_hours = 0;
	$public_holiday_hours = 0;
	$sick_leave_hours = 0;

	while($row= mysqli_fetch_assoc($re))
	{
				
			$user_name = get_emp_name($row['fk_employee_id']);
			$before6am_total_hours = $before6am_total_hours + $row['before6am_total_hours'];
			$after10pm_total_hours = $after10pm_total_hours + $row['after10pm_total_hours'];
			$saturday_hours = $saturday_hours + $row['saturday_hours'];
			$sunday_hours = $sunday_hours + $row['sunday_hours'];
			$public_holiday_hours = $public_holiday_hours + $row['public_holiday_hours'];	
			$sick_leave_hours = $sick_leave_hours + $row['sick_leave_hours'];	
			$normal_total_hours = $normal_total_hours + $row['normal_total_hours'];
			 $weekly_total_hours = $weekly_total_hours + $row['weekly_total_hours'];

				$values = array($user_name['em_first_name'].' '.$user_name['em_family_name'],
								$row["before6am_total_hours"],
								$row["after10pm_total_hours"],
								$row["saturday_hours"],
								$row["sunday_hours"],
								$row["public_holiday_hours"],
								$row["sick_leave_hours"],
								$row["normal_total_hours"],
								$row["weekly_total_hours"],
								$row["number_of_shifts"]);
				//print_r($values);exit;
					fputcsv($output,$values);
				
	}}else{
					$values = array('NO Record for this date',
								'NO Record for this date',
								'NO Record for this date',
								'NO Record for this date',
								'NO Record for this date',
								'NO Record for this date',
								'NO Record for this date',
								'NO Record for this date',
								'NO Record for this date');
			//	print_r($values);
				fputcsv($output,$values);
	
	}

	if($weekly_total_hours > 0)
	{
		$values = array('Total',
						$before6am_total_hours,
						$after10pm_total_hours,
						$saturday_hours,
						$sunday_hours,
						$public_holiday_hours,
						$sick_leave_hours,
						$normal_total_hours,
						$weekly_total_hours,
						'-');
				//print_r($values);exit;
					fputcsv($output,$values);
	}
	fclose($output);

?>
