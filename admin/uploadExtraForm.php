<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");

$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

if(fnRequestParam('mode') == 'Delete') {
	$oEmp_CDO->id = fnRequestParam('id');
	$oEmp_DA->Emp_extraFrom_Delete($oEmp_CDO);
	header('Location:uploadExtraForm.php?id='.$_REQUEST['em_id']);
	exit;	
}

if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] =='Upload Extra Form')
{
	$extra_form = $oEmp_DA->Update_extra_form($_POST);

	if($extra_form == 1)
	{
		header('Location: uploadExtraForm.php?id='.$_POST['emp_id']);	
	}
	else
	{
		header('Location: uploadExtraForm.php?id='.$_POST['emp_id']);
	}
}

?>

<?php include('header.php'); ?>
<div class="col-md-12">
<!-- <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<section class="content-header col-md-6"> <h1> Employee Offer letter </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST" enctype="multipart/form-data" >
		            <div class="box-body">
		            	
	            		
	            		<div class="input form-group">
	    		          		<input type="hidden" name="emp_id" id="emp_id" value="<?php echo $_REQUEST['id']; ?>">
                                 <label>Form Name:</label>
                                <input type="text" class="form-control" name="extra_form_name" id="extra_form_name" required>
                        </div>
                        
                        
	            		<div class="form-group">
                         <label>Upload Form:</label>
	    		         <input type="file" multiple accept="application/pdf" name="extra_form[]" id="extra_form" required>
	            		</div>
                        
	            		<div class="form-group" align="center">
	            			<input type="submit" class="btn btn-primary" name="Submit" value="Upload Extra Form"/>
	            		</div>
                        
                        
		            </div>
		        </form>
            
            <div class="box">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">Name</th>
                    <th align="left" class="tbl_design1">Form URL</th>
					<th align="left" class="tbl_design1">Delete</th>
                  </tr>
                </thead>
                <tbody>
                <?php
					$result = $oEmp_DA->Emp_extra_form_list($_REQUEST['id']);
                	while($row = mysqli_fetch_assoc($result))
                	{ 
					?>
                		<tr>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['extra_form_name'];?></td>
                        <td class="tbl_data_dtl" align="left"><a href="<?php echo $row['form_url'];?>" target="_blank">&nbsp;View Document</a></td>
					    <td class="tbl_data_dtl" align="center"><a href="uploadExtraForm.php?id=<?php echo $row['id'];?>&amp;mode=Delete&amp;em_id=<?php echo $row['em_id']; ?>" class="mylink" onClick="if (! confirm('Are you sure want to delete this employee?')) { return false; }" >Delete</a></td>
						</tr>
             <?php  } ?>
         		</tbody>
         	</table>
         </div>
          </div></div>
           
        </div>
  </div>
  </form>
</section>		

  <script type="text/javascript">
  
  </script>
  <script type="text/javascript">
        datepicker( '.datepicker' );
		
        </script>
<?php include('footer.php'); ?>
<?php ob_flush();?>
