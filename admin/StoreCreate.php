<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");

$oStore_DA = new clsStore_DA();

if(isset($_POST['submit']))
{
	$store_data = $oStore_DA->Store_Create($_POST);

	if($store_data)
	{
		header('Location: Store.php');	
	}
	else
	{
		echo "Your data can't insert";
	}
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<!-- <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<section class="content-header col-md-6"> <h1> Create Store </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	<div class="form-group">
	    		          <label>Store Name:</label>
			              <input name="st_name" type="text" id="st_name" class="input form-control" tabindex="1" required>
	            		</div>
	            		<div class="form-group">
	    		          <label>Email Address:</label>
			              <input name="store_email_address" type="text" id="store_email_address" class="input form-control" tabindex="2" required>
	            		</div>
		            	<div class="form-group">
	    		          <label>Store Address:</label>
			              <textarea name="address" id="address" class="input form-control" rows="3" tabindex="3" required></textarea>
	            		</div>
		            	<div class="input form-group">
	    		          <label>Country:</label>
	    		          <select class="form-control" name="country" id="country" tabindex="6" required="true">
	    		          	
	    		          	<?php 
	    		          	$countries = $oStore_DA->getCountries();
	    		          	while ($row = mysqli_fetch_assoc($countries)) {
	    		          		if($row['name']=='Australia'){
								?>
	    		          		<option value="<?php echo $row['id']; ?>"> <?php  echo $row['name']; ?> </option>
	    		          		<?php
	    		          	}}
	    		          	?>
	    		          </select>
	            		</div>
	            		<div class="input form-group">
	    		          <label>State:</label>
	    		          <select class="form-control" name="state" id="state" tabindex="6" required="true">
	    		          	<option>Select country first</option>
	    		          		<option value="266"> New South Wales </option>
                                <option value="269"> Queensland </option>
                                <option value="270"> South Australia </option>
                                <option value="273"> Victoria </option>	
                                <option value="275"> Western Australia </option>
                            </select>
	            		</div>
	            	   	<div class="input form-group">
	    		          <label>City:</label>
	    		          <select class="form-control" name="city" id="city" tabindex="8" required="true">
							<option>Select state first</option>
                           
	    		          </select>
	            		</div>
	            		<div class="input form-group">
	    		          <label>Select Store Admin:</label>
	    		          <select class="form-control" name="st_admin" id="st_admin" tabindex="9" required="true">
	    		          	<option>--Select Store Admin--</option>
	    		           <?php 
						  		$oUser_DA = new clsUser_DA();
								$res = $oUser_DA->User_Store_manager();
								while ($row = mysqli_fetch_assoc($res)) 
								{
						   ?>
	    		          		<option value="<?php echo $row['id']; ?>"><?php echo $row['first_name']." ".$row['last_name']; ?></option>
	    		          	
	    		          <?php } ?>
	    		          </select>
	            		</div>
                        
                        
	            		<div class="form-group">
	    		          <label>Store Status:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
	    		          	<input type="radio" name="st_status" value="1" id="st_status" checked> Active
	    		          	&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="st_status" value="0" id="st_status"> Deactive
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Create Store">
	            		</div>
	            		<input type="hidden" name="id" id="id" value="<?php echo $emp_detail['em_id']; ?>">
                    <?php /*?>    <input type="hidden" name="st_admin" id="st_admin" value="<?php echo $_SESSION['varUserID']; ?>"><?php */?>
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
  <script type="text/javascript">
  	
  	$(document).ready(function(){

		

		$('#state').on('change',function(){
        	var stateID = $(this).val();
        	if(stateID){
            	$.ajax({
        	        type:'POST',
    	            url:'ajaxData.php',
               	    data:'state_id='+stateID,
                	success:function(html){
                    $('#city').html(html);
            	    }
            	}); 
        	}else{
            	$('#city').html('<option value="">Select state first</option>'); 
        	}
    	});
	});
  </script>
<?php include('footer.php'); ?>
<?php ob_flush();?>
