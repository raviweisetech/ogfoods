<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");

$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

/*if(isset($_POST['submit']))
{
	$store_data = $oStore_DA->Store_Create($_POST);

	if($store_data)
	{
		header('Location: Store.php');	
	}
	else
	{
		echo "Your data can't insert";
	}
}*/
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<!-- <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<section class="content-header col-md-6"> <h1> Create Store </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	
	            		
	            		<div class="input form-group">
	    		          <label>Select Employee:</label>
	    		          <select class="form-control" name="employee_name" id="employee_name" tabindex="1" required="true">
	    		          	<option>--Select Employee--</option>
	    		           <?php 
						  		$result = $oEmp_DA->Emp_Select();
								while($row = mysqli_fetch_assoc($result))
								{ 
									if($row['employee_status']== "0")
									{
						   ?>
	    		          		<option value="<?php echo $row['em_id']; ?>"><?php echo $row['em_first_name']." ".$row['em_family_name']; ?></option>
	    		          	
	    		          <?php }} ?>
	    		          </select>
	            		</div>
                        
                        
	            		<div class="form-group">
                         <label>Employee Address:</label>
	    		          <textarea class="form-control" name="address" id="address"></textarea>
	            		</div>
                        <div class="form-group">
                         <label>Joining Date:</label>
	    		          	<input type="text" class="form-control" id="joining_date" name="joining_date" tabindex="3" autocomplete="off" style="width:15%;" readonly> 
	            		</div>
						<div class="input form-group">
	    		          <label>Position Applied For:</label>
	    		          <select class="form-control" name="position" id="position" tabindex="1" required="true">
	    		          
                            <option value="">Full Time</option>
                            <option value="">Part Time</option>
                            <option value="">Casual</option>

	    		          </select>
                       <!--   <input type="text" name="position" id="position" class="form-control">-->
	            		</div>
                        
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Print">
	            		</div>
<?php /*?>	            		<input type="hidden" name="id" id="id" value="<?php echo $emp_detail['em_id']; ?>">
<?php */?>                    <?php /*?>    <input type="hidden" name="st_admin" id="st_admin" value="<?php echo $_SESSION['varUserID']; ?>"><?php */?>
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
  <script type="text/javascript">
  	
  	$(document).ready(function(){

		

		$('#employee_name').on('change',function(){

        	var empl_ID = $(this).val();
			var dataString = "em_id=" + empl_ID;
        	if(empl_ID){
				$.ajax({
				type: 'POST',
				url: 'ajaxEmployee.php',
				dataType: 'json',
				data: dataString,
				success: function (result) { 
					document.getElementById("address").value = result.em_address+', '+result.em_postcode;
					document.getElementById("position").selectedIndex = result.position_for;
					alert(JSON.stringify(result));
				},error: function (result) {
						alert('error; ' + eval(result));
					}
				});
            	
        	}else{
            	document.getElementById("address").value = "Select Employee first"; 
        	}
    	});
	});
  </script>
  <script type="text/javascript">
        datepicker( '.datepicker' );
        </script>
<?php include('footer.php'); ?>
<?php ob_flush();?>
