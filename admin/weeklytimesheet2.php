<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();
//$is_store = 'yes';

$date = new DateTime();
$week_strt_day = constant("week_start_day");
$yy = date('l');
if($yy == $week_strt_day ){
	$to_date = strtotime(date('Y-m-d'));
	$date1 = date('Y-m-d', strtotime("today",$to_date)); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));
}else{
	$date1 = date('Y-m-d', strtotime("last ".constant("week_start_day")));
	$to_date = strtotime($date1); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));	
}
include('header.php');

?>
<style>

td,
th {
  border: 1px solid #000;
  width: 100px;
}
td:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:#f9f9f9;
}
th:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:#fff;
}
td:nth-child(2),th:nth-child(2)  { 
  position:sticky;
  left:50px;
  z-index:1;
  background-color:white;
}
td:nth-child(2),th:nth-child(2)  { 
  z-index:3;
}
table.dataTable thead .sorting, 
table.dataTable thead .sorting_asc, 
table.dataTable thead .sorting_desc {
    background : none;
}
th:first-child, th:last-child {z-index:2;}

</style>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet Report </h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box"  style="overflow: scroll; overflow: auto;">
            <div class="box-header">
              <h3 class="box-title">Employees Timesheet Report</h3>          
            </div>
            <div class="box-body">
             <input type="hidden" value="<?php echo $_SESSION['varStore']; ?>" name="sesstion_id" id="sesstion_id"/>
             <input type="hidden" value="<?php echo $date1; ?>" name="this_week" id="this_week"/>
	    		          <label>Select Week Start Date:</label>
	    		          <select class="form-control" name="fk_week_start" id="fk_week_start" tabindex="1" required="true" onchange="return datechange(this.value);">
	    		          	<option>--Select Date--</option>
	    		           <?php 
						  		$result = $oEmp_DA->Emp_Select_payroll();
								while($row = mysqli_fetch_assoc($result))
								{ 
						   ?>
	    		          		<option value="<?php echo $row['week_start_date']; ?>"<?php if($row['week_start_date'] == $date1){echo "selected";} ?>><?php echo $row['week_start_date']; ?></option>
	    		          	
	    		          <?php } ?>
	    		          </select>
                          <br />
            
            <div>
              <table id="example2" class="table table-bordered table-striped" >
              <thead>
               <tr>
                    <th align="center" class="tbl_design1">Week End</th>
                    <th align="center" class="tbl_design1"><label id="enddates"></label></th>
                    <th align="center" class="tbl_design1" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" class="text-right" style="    border: antiquewhite;">TUE
                    <th align="left" class="tbl_design1" style="    border: antiquewhite; min-width:100px !important;"><label id="date1"></label></th>
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border-left: 2px #f4f4f4 !important;    border-bottom-color: white;">
                    
                                        <th align="center" class="tbl_design1" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" class="text-right" style="    border: antiquewhite;">WED
                    <th align="left" class="tbl_design1" style="    border: antiquewhite; min-width:100px !important;""><label id="date2"></label></th>
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                        <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border-left: 2px #f4f4f4 !important;    border-bottom-color: white;">
                    
                                        <th align="center" class="tbl_design1" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" class="text-right" style="    border: antiquewhite;">THU
					<th align="left" class="tbl_design1" style="    border: antiquewhite; min-width:100px !important;"><label id="date3"></label></th>
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                        <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border-left: 2px #f4f4f4 !important;    border-bottom-color: white;">
                    
                                        <th align="center" class="tbl_design1" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" class="text-right" style="    border: antiquewhite;">FRI
                    <th align="left" class="tbl_design1" style="    border: antiquewhite; min-width:100px !important;"><label id="date4"></label></th>
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                        <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border-left: 2px #f4f4f4 !important;    border-bottom-color: white;">
                    
                                        <th align="center" class="tbl_design1" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" class="text-right" style="    border: antiquewhite;">SAT
                    <th align="left" class="tbl_design1" style="    border: antiquewhite; min-width:100px !important;"><label id="date5"></label></th>
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                        <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border-left: 2px #f4f4f4 !important;    border-bottom-color: white;">
                    
                                        <th align="center" class="tbl_design1" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" class="text-right" style="    border: antiquewhite;">SUN
                    <th align="left" class="tbl_design1" style="    border: antiquewhite; min-width:100px !important;"><label id="date6"></label></th>
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                        <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border-left: 2px #f4f4f4 !important;    border-bottom-color: white;">
                    
                                        <th align="center" class="tbl_design1" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" class="text-right" style="    border: antiquewhite;">MON
                    <th align="left" class="tbl_design1" style="    border: antiquewhite; min-width:100px !important;"><label id="date7"></label></th>
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                        <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border: antiquewhite;">
                    <th align="center" style="    border-left: 2px #f4f4f4 !important;    border-bottom-color: white;">
                  </tr>
                  <tr style="display:none">
                    <th align="center" class="tbl_design1">Week End</th>
                    <th align="center" class="tbl_design1"><?php echo $date7; ?></th>
                    <th align="center" class="tbl_design1 text-center" colspan="10" >TUE<?php echo " ( ".$date1." )";?></th>
                    <th align="center" class="tbl_design1 text-center" colspan="10" >WED<?php echo " ( ".$date2." )";?></th>
                    <th align="center" class="tbl_design1 text-center" colspan="10" >THU<?php echo " ( ".$date3." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="10" >FRI<?php echo " ( ".$date4." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="10" >SAT<?php echo " ( ".$date5." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="10" >SUN<?php echo " ( ".$date6." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="10" >MON<?php echo " ( ".$date7." )";?></th>                                    
                  </tr>
                </thead>
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">Name</th>
                    <th align="left" class="tbl_design1">SurName</th>
					<th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
                    <th align="left" class="tbl_design1">Public Holiday</th>
                    <th align="left" class="tbl_design1">Sick Leave</th>
                    <th align="left" class="tbl_design1">Annual Leave</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
					<th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
                    <th align="left" class="tbl_design1">Public Holiday</th>
                    <th align="left" class="tbl_design1">Sick Leave</th>
                    <th align="left" class="tbl_design1">Annual Leave</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
					<th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
                    <th align="left" class="tbl_design1">Public Holiday</th>
                    <th align="left" class="tbl_design1">Sick Leave</th>
                    <th align="left" class="tbl_design1">Annual Leave</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
                    <th align="left" class="tbl_design1">Public Holiday</th>
                    <th align="left" class="tbl_design1">Sick Leave</th>
                    <th align="left" class="tbl_design1">Annual Leave</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
                    <th align="left" class="tbl_design1">Public Holiday</th>
                    <th align="left" class="tbl_design1">Sick Leave</th>
                    <th align="left" class="tbl_design1">Annual Leave</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
                    <th align="left" class="tbl_design1">Public Holiday</th>
                    <th align="left" class="tbl_design1">Sick Leave</th>
                    <th align="left" class="tbl_design1">Annual Leave</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
                    <th align="left" class="tbl_design1">Public Holiday</th>
                    <th align="left" class="tbl_design1">Sick Leave</th>
                    <th align="left" class="tbl_design1">Annual Leave</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    
                  </tr>
                </thead>
                <tbody>
         		</tbody>
                <tfoot>
                </tfoot>
         	</table></div>
         </div>
     </div>
 </div>
</div>
</section>
<script language="javascript">
var i = 1;
function change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}
function resign_change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=res&eid="+eid+"&ems="+empstatus+"";
}
 $(function() { datechange($("#this_week").val()); });
 
function datechange(datas)
{
	var dateStr = datas;
	var daysf = 1;
	$("#enddates").html(datas);
	$("#date1").html(datas);
	
	var result = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + 1));
	var secondday = result.toISOString().substr(0, 10);
	$("#date2").html(secondday);
	
	var result = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + 2));
	var third = result.toISOString().substr(0, 10);
	$("#date3").html(third);
	
	var result = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + 3));
	var fourth = result.toISOString().substr(0, 10);
	$("#date4").html(fourth);
	
	var result = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + 4));
	var fifth = result.toISOString().substr(0, 10);
	$("#date5").html(fifth);
	
	var result = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + 5));
	var sixx = result.toISOString().substr(0, 10);
	$("#date6").html(sixx);
	
	var result = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + 6));
	var sevens = result.toISOString().substr(0, 10);
	$("#date7").html(sevens);
	
	
	// add a day
	var table = $('#example2').DataTable();
	table.destroy();
	var storeid = $("#selectedstore").val();
    document.getElementById("sesstion_id").value = storeid;

	$('#example2').DataTable( {
		"processing": true,
		"serverSide": true,
		"bPaginate": false,
    	"bLengthChange": false,
		"bFilter": true,
		"ajax":{
			url :"datatable-ajax-weekly.php",
			data : {ms:datas , storeid:storeid},
			type: "post",
			error: function(){
				$(".table-grid-error").html("");
				$("#example2").append('<tbody class="table-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#example2_processing").css("display","none");
			}
		}
	} );

}</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>