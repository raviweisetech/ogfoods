<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

$date = new DateTime();
$week_strt_day = constant("week_start_day");
$yy = date('l');
if($yy == $week_strt_day ){
	$to_date = strtotime(date('Y-m-d'));
	$date1 = date('Y-m-d', strtotime("today",$to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));
}else{
	$date1 = date('Y-m-d', strtotime("last ".constant("week_start_day")));
	$to_date = strtotime($date1);
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));	
}

if(isset($_REQUEST['Submit']) && $_REQUEST['Submit'] == 'Submit')
{	
	$std =  $_POST['start_date'];
  $edt = $_POST['end_date'];
	$storeid = $_POST['sesstion_id'];
	$emplid = $_POST['emplid'];
	
	header("location:emp_payroll_report_export.php?st=$std&storeid=$storeid&emplid=$emplid&edt=$edt");
	
}

include('header.php');

?>
<style type="text/css">
/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

/*input#user_name {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}

input#user_name[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}*/

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}

.autocomplete-items {
  position: relative;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
  width: 20%;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
label{
	margin-left: 3px;
	font-weight: 600;	
}	

</style>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet Payroll</h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
	
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
	<form name="frmManagePayroll" method="post" action="" >
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employeewise Payroll Reports</h3>    
               <button type="submit" name="Submit" value="Submit" class="mylink btn btn-primary" style="float: right; padding-left: 10px;"> Download Report</button>  
               <button  class="mylink btn btn-primary" onclick="printCon('printtable');" style="float: right; padding-left: 10px;margin-right: 8px;"> Print Reports</button> 
            </div>
            <div class="box-body">

              <div class="input form-group">
              	<label style="width: 20%;" id="userLabel">Select User</label>
                <label style="width: 20%;">Start Date</label>
                <label style="width: 20%;">End Date</label>
              	<div class="input form-group">
                    
                    <input id="user_name" type="text" name="user_name" class="form-control" style="width: 20%;display: inline;" placeholder="Select user" autocomplete="nope" onchange="return getuserid(this.value);">
	    		          <input type="text" name="start_date" class="form-control start_date" value="<?php echo date('Y-m-d') ?>" style="width: 20%;display: inline;" onchange="return datechange();"> 
                    <input type="text" name="end_date" class="form-control end_date" value="<?php echo date('Y-m-d') ?>" style="width: 20%;display: inline;" onchange="return datechange();"> 
                  


	            		</div>

              <input type="hidden" value="<?php echo $_SESSION['varStore']; ?>" name="sesstion_id" id="sesstion_id"/>
              <input type="hidden" value="<?php echo $date1; ?>" name="this_week" id="this_week"/>
              <input type="hidden" value="" name="emplid" id="emplid"/>
              
	    		         <?php /* ?><label>Select Week Start Date:</label>
	    		          <select class="form-control" name="fk_week_start" id="fk_week_start" tabindex="1" required="true" onchange="return datechange(this.value);">
	    		          	<option>--Select Date--</option>
	    		           <?php 
						  		$result = $oEmp_DA->Emp_Select_payroll();
								while($row = mysqli_fetch_assoc($result))
								{ 
						   ?>
	    		          		<option value="<?php echo $row['week_start_date']; ?>"<?php if($row['week_start_date'] == $date1){echo "selected";} ?>><?php echo $row['week_start_date']; ?></option>
	    		          	
	    		          <?php } ?>
	    		          </select> <?php */?>
	            		</div>
	            		 <div class="table-responsive">
                              <table id="example1" class="table table-bordered table-striped">
                              	<thead>
                                  <tr>
                                        <th>Emp Name</th>
                                        <th>Before 6am</th>
                                        <th>After 10pm</th>
                                        <th>Saturday Hours</th>
                                        <th>Sunday Hours</th>
                                        <th>Public Holiday Hours</th>
                                        <th>Sick Leave Hours</th>
                                        <th>Annual Leave Hours</th>
                                        <th>Normal Total Hours</th>
                                        <th>Weekly Total Hours</th>
                                        <th>Number Of Shifts</th>
                                  </tr>
                                </thead>
                                <tbody>
                               
                         		</tbody>
                                <tfoot>
                                	
                                </tfoot>
                         	</table>
         	            </div>
         </div>
     </div>
 </div>
</div>
</form>
<!-- print -->
	 <div class="box-body" id="printtable">
						<div class="hiddenn"><h1>
							<!--<img src="logo.png" width="80" height="60" /> </h1>-->
									
						</div><div class="hiddenn" style="float:right;"> Payroll Report</div>
              	 <table id="examplePrint" class="table table-bordered table-striped">
                              	<thead>
                                  <tr>
                                        <th>Emp Name</th>
                                        <th>Before 6am</th>
                                        <th>After 10pm</th>
                                        <th>Saturday Hours</th>
                                        <th>Sunday Hours</th>
                                        <th>Public Holiday Hours</th>
                                        <th>Sick Leave Hours</th>
                                        <th>Annual Leave Hours</th>
                                        <th>Normal Total Hours</th>
                                        <th>Weekly Total Hours</th>
                                        <th>Number Of Shifts</th>
                                  </tr>
                                </thead>
                                <tbody>
                               
                         		</tbody>
                                <tfoot>
                                	
                                </tfoot>
                         	</table>

              <div id="footerr" class="hiddenn"></div>
            </div>
<!-- print -->
</section>
<style>.hiddenn{ visibility:hidden; height:0px;}  th.tbl_data_dtl {  text-align: center;} #printtable{display:none}</style>
<script language="javascript" >
	function printCon(el)
	{
		var restorepage = document.body.innerHTML;
		var printcon = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcon;
		window.print();
		//document.body.innerHTML = restorepage;
		window.location.reload();
		
	}
</script>	
<script language="javascript">
var i = 1;
function change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}
function resign_change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=res&eid="+eid+"&ems="+empstatus+"";
}

 $(function() { datechange("0"); });
 
function Emp_select(uid){
	document.getElementById("emplid").value = uid;
	datechange();
}
function datechange(datas)
{
  document.getElementById("userLabel").style.color = "black";
  var strtDate = $(".start_date").val();
  var endDate = $(".end_date").val();

	var table = $('#example1').DataTable();
	table.destroy();
	var ptable = $('#examplePrint').DataTable();
	ptable.destroy();
	var storeid = $("#selectedstore").val();
	document.getElementById("sesstion_id").value = storeid;
	var emplid = $("#emplid").val();
  if(datas == '0')
  {
    storeid = 0;
  }else{
    $valuetest = $("#user_name").val();
    //alert($valuetest);
    if(emplid == '' || $valuetest == '')
    {
      document.getElementById("userLabel").style.color = "red";
      storeid = 0;
    }
  }
	
	//alert(emplid);
	
	$('#example1').DataTable( {
		"processing": true,
		"serverSide": true,
		"stateSave": true,
		"bRetrieve": true,
		"oLanguage": {
      "sLengthMenu": 'Show <select>'+
        '<option value="10">10</option>'+
        '<option value="20">20</option>'+
        '<option value="30">30</option>'+
        '<option value="40">40</option>'+
        '<option value="100">100</option>'+
        '<option value="-1">All</option>'+
        '</select> Entries Per Page'
    },
		"ajax":{
			url :"datatable-ajax-payrollEmp.php",
			data : {ms:strtDate , storeid:storeid, empid:emplid ,endDate:endDate},
			type: "post",
			error: function(){
				$(".table-grid-error").html("");
				$("#example1").append('<tbody class="table-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#example1_processing").css("display","none");
			}
		}
	} );
	$('#examplePrint').DataTable( {
		"processing": true,
		"serverSide": true,
		"stateSave": true,
		"bRetrieve": true,
		"oLanguage": {
      "sLengthMenu": 'Show <select>'+
        '<option value="10">10</option>'+
        '<option value="20">20</option>'+
        '<option value="30">30</option>'+
        '<option value="40">40</option>'+
        '<option value="100">100</option>'+
        '<option value="-1">All</option>'+
        '</select> Entries Per Page'
    },
		"ajax":{
			url :"datatable-ajax-payrollEmp.php",
      data : {ms:strtDate , storeid:storeid, empid:emplid ,endDate:endDate},
			type: "post",
			error: function(){
				$(".table-grid-error").html("");
				$("#example1").append('<tbody class="table-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
				$("#example1_processing").css("display","none");
			}
		}
	} );

}
</script>
<script>
function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
		res = arr[i].split(" ");
		//alert(res[1]);
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase() || res[1].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
			  var fields = inp.value.split('-');
			  if(fields[1] > 0 && fields[1] != null){
			  		$("#fk_employee_id").val(fields[1]);
					Emp_select(fields[1]);
			  }
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

var userName = [];
var storeid = $("#selectedstore").val();
document.getElementById("sesstion_id").value = storeid;

$.ajax({
    type: "POST",
    data : { storeid:storeid},
    url: "getEmployeeUsername.php",
    success: function (response) {
		var alldata = JSON.parse(response);
		//alert(JSON.stringify(alldata))
		alldata.forEach(myFunction);
		
		function myFunction(item, index) {
		  userName.push( item.name );
		}
		//alert(JSON.stringify(userName))
	}
});

/*initiate the autocomplete function on the "user_name" element, and pass along the userName array as possible autocomplete values:*/
autocomplete(document.getElementById("user_name"), userName);

</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>