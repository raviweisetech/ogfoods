<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OgFoods Admin Panel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!--print -->
     <link rel="stylesheet" href="css/print.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<script src='../libs/jquery.js' type='text/javascript'></script>
	<script src="../libs/jSignature.min.js"></script>
    <script src="../libs/modernizr.js"></script>
    
    <script type="text/javascript" src="../libs/flashcanvas.js"></script>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  .dataTable > thead > tr > th[class*="sort"]:after{
    content: "" !important;
	}
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="Profile.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini"><b>A</b>LT</span> -->
      <!-- logo for regular state and mobile devices -->
      <!-- <span class="logo-lg"><b>Admin</b>LTE</span> -->
      Admin Panel
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a> 
      	<div style="float:right; margin:10px;"> 
        <?php 
			if($is_store != 'yes')
			{
		?>
        
        <table width="100%">
        <tr>
        	<td width="55%" style="color:white;border: 0px solid #000;
    width: 50%;background-color: #f9f9f900;">   <label>Change Store </label></td>
        	<td style="border: 0px solid #000;
    width: 50%;background-color: #f9f9f900;"><select name="store" id="selectedstore" class="form-control" onchange="return storechange(this.value)">
            <?php  
            
				$oStore_DA = new clsStore_DA();
				$results = $oStore_DA->All_Store();
				
				
			  while($row = mysqli_fetch_assoc($results))
				 {
					 $result = $oStore_DA->Select_Store_User($_SESSION['varUserID']);
	            	 while($rows = mysqli_fetch_assoc($result))
	            		 {
							 if($row['id'] == $rows['fk_store_id'])
							 {
					 ?>
            				<option value="<?php echo $row['id'];?>" <?php if($row['id'] == $_SESSION['varStoreSel'] ){ echo "selected"; } ?> ><?php echo $row['st_name'];?></option>
             <?php  }}}?> 
            </select></td></tr></table>
            
            <?php }?>
        </div>
    </nav>
  </header>
  <script language="javascript" >
  	
	function storechange(value)
	{
		window.location.href="Activeemployee.php?mode=sst&eid="+value+"";
	}
  	
	
  </script>
<?php   include('left.php'); ?>
<div class="content-wrapper">
