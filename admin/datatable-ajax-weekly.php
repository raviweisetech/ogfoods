<?php

ob_start();
session_start();

require_once("include/clsInclude.php");

$requestData= $_REQUEST;

if($requestData['ms'] == '0')
{
	$filterQuery = '';
}
else
{
	
	$to_date = strtotime($requestData['ms']);
	
	$date1 = $requestData['ms'];
	$date2 = date('Y-m-d', strtotime("+1 day",$to_date));
	$date3 = date('Y-m-d', strtotime("+2 day",$to_date));
	$date4 = date('Y-m-d', strtotime("+3 day",$to_date));
	$date5 = date('Y-m-d', strtotime("+4 day",$to_date));
	$date6 = date('Y-m-d', strtotime("+5 day",$to_date));
	$date7 = date('Y-m-d', strtotime("+6 day",$to_date));
	
	$endDate = date('Y-m-d', strtotime("+6 day",$to_date));

	$filterQuery = " where fk_store_id = '".$requestData['storeid']."' and week_start_date = '".$requestData['ms']."' and week_end_date = '".$endDate."' and `payroll_paid` = '0' ";
}

$sql_total = "SELECT * from tbl_employee_timesheet_payroll where 1=1"; 
$res_total = execute_query($sql_total,$link) or die(mysqli_error($link));
$totalData = mysqli_num_rows($res_total);
$totalFiltered = $totalData;


		$sql_select_all = "Select DISTINCT `fk_employee_id` from tbl_employee_timesheet ";
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$sql_user = " SELECT GROUP_CONCAT(`em_id`) FROM `employee_master` WHERE ( `em_family_name` LIKE '%".$requestData['search']['value']."%' OR `em_first_name` LIKE '%".$requestData['search']['value']."%')";
			
			$sql_user_id = execute_query($sql_user,$link) or die(mysqli_error($link));
			$userids = mysqli_fetch_array($sql_user_id);
			$sql_select_all.=" where fk_employee_id IN (".$userids['GROUP_CONCAT(`em_id`)'].") ";
			
		}
		
		
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		$totalFiltered = mysqli_num_rows($res);
		$i = 0;
		$data = array();
		
		while($userId = mysqli_fetch_array($res))
		{
			$user_week_report = array();
			$sql_timesheet = $sql_sheet_check =  mysqli_query($link, "Select * from tbl_employee_timesheet where fk_store_id = '".$requestData['storeid']."' and fk_employee_id = '".$userId['fk_employee_id']."' and timesheet_date between '".$requestData['ms']."' and '".$endDate."'");
			$sql_num_sheet_add = mysqli_num_rows($sql_timesheet);
			while($data_timesheet = mysqli_fetch_array($sql_timesheet)){
				
				$user_week_report[$data_timesheet['timesheet_date']] = $data_timesheet;
			}
			if(!empty($user_week_report))
			{
				$all_user_records[] = $user_week_report;
				$emp[$i]= $userId['fk_employee_id'];
				$i++;
			}
			
		}

	$result = $all_user_records;

	
	//$data = $all_user_records;
	
	$normalTotalTue = 0; $afterTotalTue = 0; $beforeTotalTue = 0; $totalHourTue = 0;
	$normalTotalWed = 0; $afterTotalWed = 0; $beforeTotalWed = 0; $totalHourWed = 0;
	$normalTotalThu = 0; $afterTotalThu = 0; $beforeTotalThu = 0; $totalHourThu = 0;
	$normalTotalFri = 0; $afterTotalFri = 0; $beforeTotalFri = 0; $totalHourFri = 0;
	$normalTotalSat = 0; $afterTotalSat = 0; $beforeTotalSat = 0; $totalHourSat = 0;
	$normalTotalSun = 0; $afterTotalSun = 0; $beforeTotalSun = 0; $totalHourSun = 0;
	$normalTotalMon = 0; $afterTotalMon = 0; $beforeTotalMon = 0; $totalHourMon = 0;
	$j = 0;
		foreach($result as $row)
                	{
						$isHoliday1 = 'No';
						$isHoliday2 = 'No';
						$isHoliday3 = 'No';
						$isHoliday4 = 'No';
						$isHoliday5 = 'No';
						$isHoliday6 = 'No';
						$isHoliday7 = 'No';
						
						$isSickLeave1 = 'No';
						$isSickLeave2 = 'No';
						$isSickLeave3 = 'No';
						$isSickLeave4 = 'No';
						$isSickLeave5 = 'No';
						$isSickLeave6 = 'No';
						$isSickLeave7 = 'No';

						$isAnnualLeave1 = 'No';
						$isAnnualLeave2 = 'No';
						$isAnnualLeave3 = 'No';
						$isAnnualLeave4 = 'No';
						$isAnnualLeave5 = 'No';
						$isAnnualLeave6 = 'No';
						$isAnnualLeave7 = 'No';
						
						
						$user_name = get_emp_name($emp[$j]);
                		$nestedData=array(); 
                    // TUE -->
                    
					    $nestedData[] =  $user_name['em_first_name']; 
                        $nestedData[] =  $user_name['em_family_name']; 
                        
					    $nestedData[] =  $row[$date1]['rostered_time']."-".$row[$date1]['rostered_time_end'];
					    $nestedData[] =  $row[$date1]['actual_time']."-".$row[$date1]['actual_time_end'];
					    $nestedData[] =  $row[$date1]['meal_break_time']."-".$row[$date1]['meal_break_time_end'];
						
						if($row[$date1]['public_holiday'] == '1')
						{
							$isHoliday1 = 'Yes';
						}
						$nestedData[] =  $isHoliday1;
						
						if($row[$date1]['sick_leave'] == '1')
						{
							$isSickLeave1 = 'Yes';
						}
						if($row[$date1]['anual_leave'] == '1')
						{
							$isAnnualLeave1 = 'Yes';
						}

						$nestedData[] =  $isSickLeave1;

						$nestedData[] =  $isAnnualLeave1;
						
						$nestedData[] =  $row[$date1]['before_6am']; $beforeTotalTue = $beforeTotalTue + $row[$date1]['before_6am'];
					    $nestedData[] =  $row[$date1]['after_10pm']; $afterTotalTue = $afterTotalTue + $row[$date1]['after_10pm'];
                        $nestedData[] =  $row[$date1]['normal_hours']; $normalTotalTue = $normalTotalTue + $row[$date1]['normal_hours'];
                        $nestedData[] =  $row[$date1]['total_hours']; $totalHourTue = $totalHourTue + $row[$date1]['total_hours']; 
                        
                     // WED -->
                        
					    $nestedData[] =  $row[$date2]['rostered_time']."-".$row[$date2]['rostered_time_end'];
					    $nestedData[] =  $row[$date2]['actual_time']."-".$row[$date2]['actual_time_end'];
					    $nestedData[] =  $row[$date2]['meal_break_time']."-".$row[$date2]['meal_break_time_end'];
						
						if($row[$date2]['public_holiday'] == '1')
						{
							$isHoliday2 = 'Yes';
						}
						$nestedData[] =  $isHoliday2;
						
						if($row[$date2]['sick_leave'] == '1')
						{
							$isSickLeave2 = 'Yes';
						}

						if($row[$date2]['anual_leave'] == '1')
						{
							$isAnnualLeave2 = 'Yes';
						}

						$nestedData[] =  $isSickLeave2;

						$nestedData[] =  $isAnnualLeave2;
						
					    $nestedData[] =  $row[$date2]['before_6am']; $beforeTotalWed = $beforeTotalWed + $row[$date2]['before_6am'];
					    $nestedData[] =  $row[$date2]['after_10pm']; $afterTotalWed = $afterTotalWed + $row[$date2]['after_10pm'];
                        $nestedData[] =  $row[$date2]['normal_hours']; $normalTotalWed = $normalTotalWed + $row[$date2]['normal_hours']; 
                        $nestedData[] =  $row[$date2]['total_hours']; $totalHourWed = $totalHourWed + $row[$date2]['total_hours'];
                     
                     // THU -->
                        
                        $nestedData[] =  $row[$date3]['rostered_time']."-".$row[$date3]['rostered_time_end'];
					    $nestedData[] =  $row[$date3]['actual_time']."-".$row[$date3]['actual_time_end'];
					    $nestedData[] =  $row[$date3]['meal_break_time']."-".$row[$date3]['meal_break_time_end'];
						
						if($row[$date3]['public_holiday'] == '1')
						{
							$isHoliday3 = 'Yes';
						}
						$nestedData[] =  $isHoliday3;
						
						if($row[$date3]['sick_leave'] == '1')
						{
							$isSickLeave3 = 'Yes';
						}

						if($row[$date3]['anual_leave'] == '1')
						{
							$isAnnualLeave3 = 'Yes';
						}

						$nestedData[] =  $isSickLeave3;
						$nestedData[] =  $isAnnualLeave3;
						
					    $nestedData[] =  $row[$date3]['before_6am']; $beforeTotalThu = $beforeTotalThu + $row[$date3]['before_6am'];
					    $nestedData[] =  $row[$date3]['after_10pm']; $afterTotalThu = $afterTotalThu + $row[$date3]['after_10pm'];
                        $nestedData[] =  $row[$date3]['normal_hours']; $normalTotalThu = $normalTotalThu + $row[$date3]['normal_hours']; 
                        $nestedData[] =  $row[$date3]['total_hours']; $totalHourThu = $totalHourThu + $row[$date3]['total_hours'];
                     
                     // FRI -->
                        
                        $nestedData[] =  $row[$date4]['rostered_time']."-".$row[$date4]['rostered_time_end'];
					    $nestedData[] =  $row[$date4]['actual_time']."-".$row[$date4]['actual_time_end'];
					    $nestedData[] =  $row[$date4]['meal_break_time']."-".$row[$date4]['meal_break_time_end'];
						
						if($row[$date4]['public_holiday'] == '1')
						{
							$isHoliday4 = 'Yes';
						}
						$nestedData[] =  $isHoliday4;
						
						if($row[$date4]['sick_leave'] == '1')
						{
							$isSickLeave4 = 'Yes';
						}

						if($row[$date4]['anual_leave'] == '1')
						{
							$isAnnualLeave4 = 'Yes';
						}

						$nestedData[] =  $isSickLeave4;

						$nestedData[] =  $isAnnualLeave4;

						
					    $nestedData[] =  $row[$date4]['before_6am']; $beforeTotalFri = $beforeTotalFri + $row[$date4]['before_6am'];
					    $nestedData[] =  $row[$date4]['after_10pm']; $afterTotalFri = $afterTotalFri + $row[$date4]['after_10pm'];
                        $nestedData[] =  $row[$date4]['normal_hours']; $normalTotalFri = $normalTotalFri + $row[$date4]['normal_hours'];
                        $nestedData[] =  $row[$date4]['total_hours']; $totalHourFri = $totalHourFri + $row[$date4]['total_hours'];
                     
                     // SAT -->
                        
                        $nestedData[] =  $row[$date5]['rostered_time']."-".$row[$date5]['rostered_time_end'];
					    $nestedData[] =  $row[$date5]['actual_time']."-".$row[$date5]['actual_time_end'];
					    $nestedData[] =  $row[$date5]['meal_break_time']."-".$row[$date5]['meal_break_time_end'];
						
						if($row[$date5]['public_holiday'] == '1')
						{
							$isHoliday5 = 'Yes';
						}
						$nestedData[] =  $isHoliday5;
						
						if($row[$date5]['sick_leave'] == '1')
						{
							$isSickLeave5 = 'Yes';
						}

						if($row[$date5]['anual_leave'] == '1')
						{
							$isAnnualLeave5 = 'Yes';
						}

						$nestedData[] =  $isSickLeave5;

						$nestedData[] =  $isAnnualLeave5;						
						
					    $nestedData[] =  $row[$date5]['before_6am']; $beforeTotalSat = $beforeTotalSat + $row[$date5]['before_6am'];
					    $nestedData[] =  $row[$date5]['after_10pm']; $afterTotalSat = $afterTotalSat + $row[$date5]['after_10pm'];
                        $nestedData[] =  $row[$date5]['normal_hours']; $normalTotalSat = $normalTotalSat + $row[$date5]['normal_hours'];
                        $nestedData[] =  $row[$date5]['total_hours']; $totalHourSat = $totalHourSat + $row[$date5]['total_hours'];
                     
                     // SUN -->
                        
                        $nestedData[] =  $row[$date6]['rostered_time']."-".$row[$date6]['rostered_time_end'];
					    $nestedData[] =  $row[$date6]['actual_time']."-".$row[$date6]['actual_time_end'];
					    $nestedData[] =  $row[$date6]['meal_break_time']."-".$row[$date6]['meal_break_time_end'];
						
						if($row[$date6]['public_holiday'] == '1')
						{
							$isHoliday6 = 'Yes';
						}
						$nestedData[] =  $isHoliday6;
						
						if($row[$date6]['sick_leave'] == '1')
						{
							$isSickLeave6 = 'Yes';
						}

						if($row[$date6]['anual_leave'] == '1')
						{
							$isAnnualLeave6 = 'Yes';
						}

						$nestedData[] =  $isSickLeave6;

						$nestedData[] =  $isAnnualLeave6;
						
					    $nestedData[] =  $row[$date6]['before_6am']; $beforeTotalSun = $beforeTotalSun + $row[$date6]['before_6am'];
					    $nestedData[] =  $row[$date6]['after_10pm']; $afterTotalSun = $afterTotalSun + $row[$date6]['after_10pm'];
                        $nestedData[] =  $row[$date6]['normal_hours']; $normalTotalSun = $normalTotalSun + $row[$date6]['normal_hours']; 
                        $nestedData[] =  $row[$date6]['total_hours']; $totalHourSun = $totalHourSun + $row[$date6]['total_hours'];
                        
                     // MON -->
                        
                        $nestedData[] =  $row[$date7]['rostered_time']."-".$row[$date7]['rostered_time_end'];
					    $nestedData[] =  $row[$date7]['actual_time']."-".$row[$date7]['actual_time_end'];
					    $nestedData[] =  $row[$date7]['meal_break_time']."-".$row[$date7]['meal_break_time_end'];
						
						if($row[$date7]['public_holiday'] == '1')
						{
							$isHoliday7 = 'Yes';
						}
						$nestedData[] =  $isHoliday7;
						
						if($row[$date7]['sick_leave'] == '1')
						{
							$isSickLeave7 = 'Yes';
						}
						if($row[$date7]['anual_leave'] == '1')
						{
							$isAnnualLeave7 = 'Yes';
						}
						$nestedData[] =  $isSickLeave7;

						$nestedData[] =  $isAnnualLeave7;
						
					    $nestedData[] =  $row[$date7]['before_6am']; $beforeTotalMon = $beforeTotalMon + $row[$date7]['before_6am']; 
					    $nestedData[] =  $row[$date7]['after_10pm']; $afterTotalMon = $afterTotalMon + $row[$date7]['after_10pm'];
                        $nestedData[] =  $row[$date7]['normal_hours']; $normalTotalMon = $normalTotalMon + $row[$date7]['normal_hours'];
                        $nestedData[] =  $row[$date7]['total_hours']; $totalHourMon = $totalHourMon + $row[$date7]['total_hours'];
						
						$data[] = $nestedData;
						$j++;
              }
			  if($j > 0){
				 	$totalRecord[] = "Total";
                    $totalRecord[] = "";
                    $totalRecord[] = "";                    
					$totalRecord[] = "";
                    $totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] =  $beforeTotalTue; 
					$totalRecord[] =  $afterTotalTue; 
                    $totalRecord[] =  $normalTotalTue; 
                    $totalRecord[] =  $totalHourTue; 
                    
					$totalRecord[] = "";
					$totalRecord[] = "";
                    $totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] =  $beforeTotalWed; 
					$totalRecord[] =  $afterTotalWed; 
                    $totalRecord[] =  $normalTotalWed; 
                    $totalRecord[] =  $totalHourWed; 
                    
					$totalRecord[] = "";
					$totalRecord[] = "";
                    $totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] =  $beforeTotalThu; 
					$totalRecord[] =  $afterTotalThu; 
                    $totalRecord[] =  $normalTotalThu; 
                    $totalRecord[] =  $totalHourThu; 
                    
                    $totalRecord[] = "";
					$totalRecord[] = "";
                    $totalRecord[] = "";
                    $totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] =  $beforeTotalFri; 
					$totalRecord[] =  $afterTotalFri; 
                    $totalRecord[] =  $normalTotalFri; 
                    $totalRecord[] =  $totalHourFri; 
                    
                    $totalRecord[] = "";
					$totalRecord[] = "";
                    $totalRecord[] = "";
                    $totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] =  $beforeTotalSat; 
					$totalRecord[] =  $afterTotalSat; 
                    $totalRecord[] =  $normalTotalSat; 
                    $totalRecord[] =  $totalHourSat; 
                    
                    $totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
                    $totalRecord[] = "";
					$totalRecord[] =  $beforeTotalSun; 
					$totalRecord[] =  $afterTotalSun; 
                    $totalRecord[] =  $normalTotalSun; 
                    $totalRecord[] =  $totalHourSun; 
                    
                    $totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
					$totalRecord[] = "";
                    $totalRecord[] = "";
					$totalRecord[] =  $beforeTotalMon; 
					$totalRecord[] =  $afterTotalMon; 
                    $totalRecord[] =  $normalTotalMon; 
                    $totalRecord[] =  $totalHourMon; 
                    
					$data[] = $totalRecord;
			  }
	
	
		
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalData),
			"recordsFiltered" => intval( $totalFiltered),
			"data"            => $data
			);

echo json_encode($json_data);

?>