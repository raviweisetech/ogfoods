<?php 

ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
require_once("include/clsInclude.php");
$oCompany_CDO = new clsCompany_CDO();
$oCompany_DA = new clsCompany_DA();


if(fnRequestParam('mode') == 'Delete') {
	$oCompany_CDO->id = fnRequestParam('id');
	$oCompany_DA->Company_Delete($oCompany_CDO);
	header("Location:Managecompany.php?msg=Del");
	exit;	
}
include('header.php'); 

?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Manage Company </h1> </section>
<section class="col-md-6 center" align="right" style="margin-top: 10px"><a href="CreateCompany.php"><button class="btn-primary" >Create Company</button></a></section>
</div>
<br><br>
<section class="content">
	<form name="frmManageUser" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
              			<h3 class="box-title">Company</h3>
            		</div>
            		<div class="box-body">
            			<table id="example1" class="table table-bordered table-striped">
            				<thead>
			                  <tr>
			                    <th align="left" class="tbl_design1">Company Name</th>
								<th align="left" class="tbl_design1">ABN Number</th>
								<th align="left" class="tbl_design1">Edit</th>
								<th align="left" class="tbl_design1">Delete</th>
			                  </tr>
			                </thead>
			                <tbody>
			                	<?php
									if(isset($_REQUEST['search_submit'])) {
										$oCompany_CDO->Username = trim(fnRequestParam('txtName'));
										$oCompany_CDO->Userstatus = trim(fnRequestParam('searchstatus'));
										$oCompany_CDO->Employeestatus = trim(fnRequestParam('search_employe'));
										$oCompany_DA->User_Search($oCompany_CDO);
						  			}
									else {
										$oCompany_DA->Company_Select();
							    	}

									for($i=0; $i<$oCompany_DA->totalRows; $i++) {
										$id = $oCompany_DA->id[$i];
									?>
									<tr>
										<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oCompany_DA->company_name[$i];?></td>
										<td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oCompany_DA->abn_number[$i];?></td>
										<td class="tbl_data_dtl" align="center"><a href="CompanyEdit.php?id=<?php echo $id;?>" class="mylink">Edit</a></td>
										<td class="tbl_data_dtl" align="center"><a href="Managecompany.php?id=<?php echo $id;?>&amp;mode=Delete" onclick="return confirm('Are you sure?')" class="mylink">Delete</a></td>
									</tr>
									<?php } ?>
			                </tbody>
            			</table>
            		</div>
				</div>
			</div>
		</div>
	</form>
</section>
<?php include('footer.php'); ?>
<?php ob_flush();?>