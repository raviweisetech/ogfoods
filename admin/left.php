<?php
require_once("include/clsInclude.php");
/* if(basename($_SERVER['PHP_SELF']) != "User.php")
{
$oUser_CDO = new clsUser_CDO();
$oUser_DA = new clsUser_DA();
}
$getcount = explode("==",$oUser_DA->GetUserClientcount());
 */
?>
<style>
.submenu{
	padding-left:0 !important;
    margin-left: 12pt;
}
.submenu span {
    padding: 5px;
}
</style>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <!-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
        </div>
        <!-- <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div> -->
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Admin Management</li>
        <?php
        if($_SESSION['U_Type'] == 0)
        {
        ?>
        <li>
          <a href="Home.php">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <li>
          <a href="Profile.php">
            <i class="fa fa-th"></i> <span>Profile</span>
          </a>
        </li>
	      <li>
          <a href="User.php">
            <i class="fa fa-users"></i> <span>Manage Users</span>
          </a>
        </li>
        <li>
          <a href="Managecompany.php">
            <i class="fa fa-users"></i> <span>Manage Company</span>
          </a>
        </li>
<!--         <li>
          <a href="Offer.php">
            <i class="fa fa-users"></i> <span>Manage Offer</span>
          </a>
        </li>
        <li>
          <a href="Pendingemployee.php">
            <i class="fa fa-spinner"></i> <span>Pending Employee</span>
          </a>
        </li>
        <li>
        <li>
          <a href="Activeemployee.php">
            <i class="fa fa-check-square-o"></i> <span>Active Employee</span>
          </a>
        </li>
         <li>
          <a href="Inactiveemployee.php">
            <i class="fa fa-ban"></i> <span>Inctive Employee</span>
          </a>
        </li>
         <li>
          <a href="employeeofferletter.php">
            <i class="fa fa-file-text-o"></i> <span>Create Employee Offer Letter</span>
          </a>
        </li>
         <li>
          <a href="uploademployeeofferletter.php">
            <i class="fa fa-file-text-o"></i> <span>Upload Employee Offer Letter</span>
          </a>
        </li>-->
        <li>
          <a href="Store.php">
            <i class="fa fa-list-ol"></i> <span>Store Management</span>
          </a>
        </li>
        <?php
        }
        ?>
        <li>
          <a href="#">
            <i class="fa fa-list"></i> <span>Manage Employee</span> </a>
            <a href="Employeement.php">
			<ul class="submenu"><i class="fa fa-user"></i> <span>Add New Employee</span></ul></a>
            <a href="Pendingemployee.php">
			<ul class="submenu"><i class="fa fa-spinner"></i> <span>Pending Employee</span></ul></a>
            <?php
            if($_SESSION['U_Type'] == 0)
            {
            ?> 
            <a href="Activeemployee.php">
			<ul class="submenu"><i class="fa fa-check-square-o"></i> <span>Active Employee</span></ul></a>
			<?php
            }
            if($_SESSION['U_Type'] != 2)
            {
            ?>
            <a href="Inactiveemployee.php">
			<ul class="submenu"><i class="fa fa-ban"></i> <span>Inactive Employee</span></ul></a>
			<?php
            }
			?>
			 <a href="employeeofferletter.php">
			<ul class="submenu"><i class="fa fa-envelope-o"></i> <span>Create Employee Offer Letter</span></ul></a>
			 <a href="uploademployeeofferletter.php">
			<ul class="submenu"><i class="fa fa-envelope"></i> <span>Upload Employee Offer Letter</span></ul></a>
			
        </li>
        <li>
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Timesheet</span> </a>
            <a href="EmpTimesheet33.php">
			<ul class="submenu"><i class="fa fa-calendar-o"></i> <span>add Timesheet</span></ul></a>
			 <a href="weeklytimesheet2.php">
			<ul class="submenu"><i class="fa fa-calendar"></i> <span>Weekly Timesheet</span></ul></a>
			 <a href="payrollreport2.php">
			<ul class="submenu"><i class="fa fa-money"></i> <span>Payroll Reports</span></ul></a>
      <?php if($_SESSION['U_Type'] == 0) { ?>
      <a href="payrollreportEmp.php">
      <ul class="submenu"><i class="fa fa-money"></i> <span>Employee payroll Report</span></ul></a>
			<?php } ?>
        </li>
        <!-- <li>
          <a href="weeklytimesheet.php">
            <i class="fa fa-list-ol"></i> <span>Weekly Timesheet</span>
          </a>
        </li>
        <li>
          <a href="payrollreport.php">
            <i class="fa fa-list-ol"></i> <span>Payroll Report</span>
          </a>
        </li>-->
        <li>
          <a href="ChangePassword.php">
            <i class="fa fa-key"></i> <span>Change Password</span>
          </a>
        </li>
        <li>
          <a href="Managesettings.php">
            <i class="fa fa-users"></i> <span>Manage Settings</span>
          </a>
        </li>
	       <li>
          <a href="Logout.php">
            <i class="fa fa-power-off"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
					<link rel="stylesheet" type="text/css" media="all" href="../calendar/skins/aqua/theme.css" title="win2k-cold-1" />
                    <script type="text/javascript" src="../calendar/calendar.js"></script>
                    <!-- language for the calendar -->
                    <script type="text/javascript" src="../calendar/lang/calendar-en.js"></script>
                    <!-- the following script defines the Calendar.setup helper function, which makes
      				 adding a calendar a matter of 1 or 2 lines of code. -->
                    <script type="text/javascript" src="../calendar/calendar-setup.js"></script>
                    <link type="text/css" rel="stylesheet" href="../css/admin_style.css" />
               