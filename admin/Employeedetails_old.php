<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();
					//	print_r($oUser_DA->User_Select());exit();

if(isset($_REQUEST['id']))
{
	$eid = $_REQUEST['id'];
	$employee_master = $oEmp_DA->Employee_Master($_REQUEST['id']);
	$employee_work_history = $oEmp_DA->Employee_History($_REQUEST['id']);
	$employee_info = $oEmp_DA->Employee_Info($_REQUEST['id']);
	$employee_apply_for = $oEmp_DA->Employee_Applyfor($_REQUEST['id']);
	
}

//Edit user start
if(fnRequestParam('mode') == 'es') {
	$oEmp_CDO->id = fnRequestParam('eid');
	$oEmp_CDO->us = fnRequestParam('ems');
	$oEmp_DA->Emp_Status($oEmp_CDO);
	header("Location:Employeedetails.php?id=$oEmp_CDO->id");
	exit;
}
/*Edit user End*/

/*Delete user Data start*/


/*Delete user data end*/
if(isset($_REQUEST['search_submit'])) {
	unset($_REQUEST['msg']);
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> View Employee Details </h1> </section>
</div>
<br><br>
<section class="content">
<form name="frm" method="post" action="" enctype="multipart/form-data"  >
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User</h3>
              <br>
	            <div class="box-body col-md-6">
	              <table id="example" class="table table-bordered table-striped">
						<tr>
							<td class="col-md-3"><b>Family Name</b></td><td class="col-md-3"><?php echo $employee_master['em_family_name'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Other Name</b></td><td class="col-md-3"><?php echo $employee_master['em_other_name'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Address</b></td><td class="col-md-3"><?php echo $employee_master['em_address'] ?></td>
						</tr>						
						<tr>
							<td class="col-md-3"><b>Postcode</b></td><td class="col-md-3"><?php echo $employee_master['em_postcode'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Phone No </b></td><td class="col-md-3"><?php echo $employee_master['em_phone_no'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Mobile No</b></td><td class="col-md-3"><?php echo $employee_master['em_mobile_no'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>legally entitled to work in Australia</b></td><td class="col-md-3"><?php if($employee_master['em_entitled_austrailia'] == "1"){ echo "Yes"; }else { echo "No"; } ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Identification to verify </b></td><td class="col-md-3"><?php echo $employee_master['em_identity_doc'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Currently Study </b></td><td class="col-md-3"><?php if($employee_master['em_current_study_status'] == "0" ){ echo "No"; }else { echo "Yes";}  ?></td>
						</tr>
                        <td class="col-md-3"><b>Study Type </b></td><td class="col-md-3"><?php $new=explode("#",$employee_master['em_study_type']); echo $new[0]." , ".$new[1]; ?></td>
						</tr>
                        <td class="col-md-3"><b>Institution attend </b></td><td class="col-md-3"><?php $new=explode("#",$employee_master['em_institute_type']); foreach($new as $ne){  if ($ne==""){}else {echo $ne.", ";} } ?></td>
						</tr>
                        <td class="col-md-3"><b>Name Of highschool attend </b></td><td class="col-md-3"><?php echo $employee_master['em_hs_name'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Highest Grade/level passed </b></td><td class="col-md-3"><?php echo $employee_master['em_hs_grade'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Passed Year </b></td><td class="col-md-3"><?php echo $employee_master['em_hs_year'] ?></td>
						</tr>
                        <td class="col-md-3"><b>When commenced </b></td><td class="col-md-3"><?php $new=explode("#", $employee_master['em_ps_commenced']); echo $new[0]." , ".$new[1]." , ".$new[2]; ?></td>
						</tr>
                        <td class="col-md-3"><b>Year completed post secondary </b></td><td class="col-md-3"><?php $new= explode("#",$employee_master['em_ps_year']); echo $new[0]." , ".$new[1]." , ".$new[2]; ?></td>
						</tr>
                        <td class="col-md-3"><b>Post secondary qualifications obtained </b></td><td class="col-md-3"><?php $new=explode("#",$employee_master['em_qualification']); foreach($new as $ne){ if ($ne==""){}else {echo $ne.", ";} } ?></td>
						</tr>
                        <td class="col-md-3"><b>Employee Signature </b></td><td id="displaySignature" class="col-md-3"></td>
                    
						</tr>
                        <td class="col-md-3"><b>Employee Signature Date </b></td><td class="col-md-3"><?php echo $employee_master['em_signature_date'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Terms And Conditions </b></td><td class="col-md-3"><?php if($employee_master['em_terms_con']=="1"){ echo "Checked";}else { echo "Not checked";} ?></td>
						</tr>
                        <td class="col-md-3"><b>Date Of submition </b></td><td class="col-md-3"><?php echo $employee_master['created_date'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Job Title </b></td><td class="col-md-3"><?php $new=explode("#", $employee_apply_for['em_job_title']);foreach($new as $ne){  if ($ne==""){}else {echo $ne.", ";} } ?></td>
						</tr>
                         <td class="col-md-3"><b>Status of position apply for </b></td><td class="col-md-3"><?php $new=explode("#",$employee_apply_for['em_position_status']); foreach($new as $ne){  if ($ne==""){}else {echo $ne.", ";} } ?></td>
						</tr>
                         <td class="col-md-3"><b>Any circumstances </b></td><td class="col-md-3"><?php $new=explode("#",$employee_apply_for['em_limit_availability']); foreach($new as $ne){  if ($ne==""){}else {echo $ne.", ";} } ?></td>
						</tr>
                         <td class="col-md-3"><b>Monday Detail </b></td><td class="col-md-3"><?php echo $employee_apply_for['em_monday_detail'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Tuesday Detail </b></td><td class="col-md-3"><?php echo $employee_apply_for['em_tuesday_detail'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Wednesday Detail </b></td><td class="col-md-3"><?php echo $employee_apply_for['em_wednesday_detail'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Thursday Detail </b></td><td class="col-md-3"><?php echo $employee_apply_for['em_thursday_detail'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Friday Detail </b></td><td class="col-md-3"><?php echo $employee_apply_for['em_friday_detail'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Saturday Detail </b></td><td class="col-md-3"><?php echo $employee_apply_for['em_saturday_detail'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Sunday Detail </b></td><td class="col-md-3"><?php echo $employee_apply_for['em_sunday_detail'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Employer Name /address/ telephone </b></td><td class="col-md-3"><?php echo $employee_work_history['em_employer_name'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Work From / to</b></td><td class="col-md-3"><?php echo $employee_work_history['em_from_to'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Last job position </b></td><td class="col-md-3"><?php echo $employee_work_history['em_position'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Reason for Leaving </b></td><td class="col-md-3"><?php echo $employee_work_history['em_reason'] ?></td>
						</tr>
                        <td class="col-md-3"><b>Refrence Name and Phone </b></td><td class="col-md-3"><?php echo $employee_work_history['em_reference'] ?></td>
						</tr>
                        <td class="col-md-3"><b>List of skills/knowlegde or experience </b></td><td class="col-md-3"><?php echo $employee_info['em_skill'] ?></td>
						</tr>
                         <td class="col-md-3"><b>List of Hobby </b></td><td class="col-md-3"><?php echo $employee_info['em_hobby'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Prepared Attend  medical examination </b></td><td class="col-md-3"><?php if($employee_info['em_prep_medical'] == "1"){echo "Yes";}else {echo "No";} ?></td>
						</tr>
                         <td class="col-md-3"><b>Status Of physical or mental disability </b></td><td class="col-md-3"><?php echo $employee_info['em_physical_disability_status'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Nature Of provide to us </b></td><td class="col-md-3"><?php echo $employee_info['em_nature_of_work'] ?></td>
						</tr> <td class="col-md-3"><b>Injury Status </b></td><td class="col-md-3"><?php echo $employee_info['em_injury_status'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Criminal Status </b></td><td class="col-md-3"><?php echo $employee_info['em_criminal_status'] ?></td>
						</tr>
                       <td class="col-md-3"><b>Charges Status</b></td><td class="col-md-3"><?php echo $employee_info['em_charges_status'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Additional info by employee </b></td><td class="col-md-3"><?php echo $employee_info['em_additional_info'] ?></td>
						</tr>
                         <td class="col-md-3"><b>Confidentiality Agreement </b></td><td class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_con_agreement']);if($new[3]==""){ echo "#";}else{ echo "upload/Confidentiality_Agreement/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?>t</a></td>
						</tr>
                         <td class="col-md-3"><b>Crew Member Acknowledgment </b></td><td class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_crewm_Ack']); if($new[3]==""){ echo "#";}else{ echo "upload/Crew_Member_Acknowledgment/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <td class="col-md-3"><b>Employee Cash Drawer Policy </b></td><td class="col-md-3"><a  href="<?php $new=explode("/",$employee_info['em_cashdrawer_policy']); if($new[3]==""){ echo "#";}else{ echo "upload/Employee_Cash_Drawer_Policy/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <td class="col-md-3"><b>Crew/Staff letter off Employment offer </b></td><td class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_cre_letteroff']); if($new[3]==""){ echo "#";}else{ echo "upload/CrewStaff_letter_off_Employment_offer/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <td class="col-md-3"><b>Fair Work Information Statement </b></td><td class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_fair_workstat']); if($new[3]==""){ echo "#";}else{ echo "upload/Fair_Work_Information_Statement/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                        <td class="col-md-3"><b>WHS FORM </b></td><td class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_whs_form']); if($new[3]==""){ echo "#";}else{ echo "upload/WHS_FORM/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                        <td class="col-md-3"><b>WHS Responsibility </b></td><td class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_whs_responsibility']); if($new[3]==""){ echo "#";}else{ echo "upload/WHS_Responsibility/".$new[3]; } ?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <td class="col-md-3"><b>Employee Status </b></td><td class="col-md-3">
                         <select name="status"  onchange="return change_status(<?php echo $eid ;?>,this.value)">
                         	<option value="0"<?php if($employee_master['employee_status']== "0"){ echo"selected";}?>>Pending</option>
                            <option value="1"<?php if($employee_master['employee_status']== "1"){ echo"selected";}?>>Approve</option>
                            <option value="2"<?php if($employee_master['employee_status']== "2"){ echo"selected";}?>>Decline</option>
                         </select></td>
						</tr>
	              </table>
	            </div>
                    
                   
                       <script>
                             $(document).ready(function(data){
                               var i = new Image()
                               var signature ='<?php echo $employee_master['em_signature']; ?>';
                        //Here signatureDataFromDataBase is the string that you saved earlier
                                i.src = 'data:' + signature;
                                $(i).appendTo('#displaySignature')
                               })
                       </script>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<script language="javascript">

function change_status(eid,empstatus)
{
		
		window.location.href="Employeedetails.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}


</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>