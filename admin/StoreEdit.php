<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
require_once("include/clsInclude.php");
$oStore_DA = new clsStore_DA();
					//	print_r($oUser_DA->User_Select());exit();
if(isset($_POST['submit']))
{
	$store_data = $oStore_DA->Store_Edit($_POST);

	if($store_data)
	{
		header('Location: Store.php');	
	}
	else
	{
		echo "Your data can't Updated";
	}
}
else
{
	if($_GET['id'])
	{
		$store_detail = $oStore_DA->Store_Detail($_GET['id']);
	}
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<!-- <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<section class="content-header col-md-6"> <h1> Edit Store </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	<div class="form-group">
	    		          <label>Store Name:</label>
			              <input name="st_name" type="text" id="st_name" class="input form-control" tabindex="1" value="<?php echo $store_detail['st_name']; ?>" required="true">
	            		</div>
	            		<div class="form-group">
	    		          <label>Email Address:</label>
			              <input name="store_email_address" type="text" id="store_email_address" class="input form-control" tabindex="2" value="<?php echo $store_detail['store_email_address']; ?>" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>Store Address:</label>
			              <textarea name="address" id="address" class="input form-control" rows="3" tabindex="3" required="true"><?php echo $store_detail['st_address']; ?></textarea>
	            		</div>
		            	<div class="input form-group">
	    		          <label>Country:</label>
	    		          <select class="form-control" name="country" id="country" tabindex="6" required="true">
	    		          	<option>Select country</option>
	    		          	<?php 
	    		          	$countries = $oStore_DA->getCountries();
	    		          	while ($row = mysqli_fetch_assoc($countries)) {
	    		          		?>
	    		          		<option value="<?php echo $row['id']; ?>"
	    		          			<?php if($row['id'] == $store_detail['st_country'])
	    		          				 	echo "selected=true";
	    		          			 ?>> <?php echo $row['name']; ?> </option>
	    		          		<?php
	    		          		}
	    		          	?>
	    		          </select>
	            		</div>
	            		<div class="input form-group">
	    		          <label>State:</label>
	    		          <select class="form-control" name="state" id="state" tabindex="6" required="true">
	    		  <?php /*?>        	<option value="<?php echo $store_detail['st_state']; ?>"><?php echo $oStore_DA->getState($store_detail['st_state']); ?></option>				<?php */?>		
                            <option value="266" <?php if($store_detail['st_state'] == '266'){ echo 'selected';} ?>> New South Wales </option>
                            <option value="269" <?php if($store_detail['st_state'] == '269'){ echo 'selected';} ?>> Queensland </option>
                            <option value="270" <?php if($store_detail['st_state'] == '270'){ echo 'selected';} ?>> South Australia </option>
                            <option value="273" <?php if($store_detail['st_state'] == '273'){ echo 'selected';} ?>> Victoria </option>	
                            <option value="275" <?php if($store_detail['st_state'] == '275'){ echo 'selected';} ?>> Western Australia </option>
	    		          </select>
	            		</div>
	            	   	<div class="input form-group">
	    		          <label>City:</label>
	    		          <select class="form-control" name="city" id="city" tabindex="8" required="true">
							<option value="<?php echo $store_detail['st_city']; ?>" selected><?php echo $oStore_DA->getCity($store_detail['st_city']); ?></option>
	    		          </select>
	            		</div>
	            		<div class="input form-group">
	    		          <label>Select Store Admin:</label>
	    		          <select class="form-control" name="st_admin" id="st_admin" tabindex="9" required="true">
	    		          	<option>--Select Store Admin--</option>
	    		          	<?php 
	    		          	$user = $oStore_DA->getStoreAdmin();
	    		          	while ($row = mysqli_fetch_assoc($user)) {
	    		          		?>
	    		          		<option value="<?php echo $row['id']; ?>" 
	    		          			<?php if($row['id'] == $store_detail['st_owner'])
	    		          				echo "selected=true";  ?>> <?php echo $row['first_name']." ".$row['last_name']; ?> </option>
	    		          		<?php
	    		          	}
	    		          	?>
	    		          </select>
	            		</div>
	            		<div class="form-group">
	    		          <label>Store Status:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="st_status" value="1" id="st_status"
	    		          	<?php if($store_detail['st_status'] == '1')
	    		          			echo "checked = true";
	    		          	 ?>
	    		          	> Active
	    		          	&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="st_status" value="0" id="st_status"
	    		          	<?php if($store_detail['st_status'] == '0')
	    		          			echo "checked = true";
	    		          	 ?>
	    		          	> Deactive
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Update Store">
	            		</div>
	            		<input type="hidden" name="id" id="id" value="<?php echo $_GET['id'] ?>">
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
  <script type="text/javascript">

 
  	$(document).ready(function(){	
	
        	var stateID = document.getElementById('state').value;
        	if(stateID){
            	$.ajax({
        	        type:'POST',
    	            url:'ajaxData.php',
               	    data:'state_id='+stateID,
                	success:function(html){
                    $('#city').html(html);
            	    }
            	}); 
        	}else{
            	$('#city').html('<option value="">Select state first</option>'); 
        	}
    	
	

		$('#country').on('change',function(){

			var CountryId = $(this).val();
			if(CountryId){
				$.ajax({
					type:'POST',
					url:'ajaxData.php',
					data:'country_id='+CountryId,
					success:function(html){
	                    $('#state').html(html);
	                    $('#city').html('<option value="">Select state first</option>'); 
	                }
				});
			}
			else
			{
				$('#state').html('<option value="">Select country first</option>');
            	$('#city').html('<option value="">Select state first</option>'); 
			}
		})

		$('#state').on('change',function(){
        	var stateID = $(this).val();
        	if(stateID){
            	$.ajax({
        	        type:'POST',
    	            url:'ajaxData.php',
               	    data:'state_id='+stateID,
                	success:function(html){
                    $('#city').html(html);
            	    }
            	}); 
        	}else{
            	$('#city').html('<option value="">Select state first</option>'); 
        	}
    	});
	});
  </script>
<?php include('footer.php'); ?>
<?php ob_flush();?>
