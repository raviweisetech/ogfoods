<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();
$is_store = 'yes';

$date = new DateTime();
$week_strt_day = constant("week_start_day");
$yy = date('l');
if($yy == $week_strt_day ){
	$to_date = strtotime(date('Y-m-d'));
	$date1 = date('Y-m-d', strtotime("today",$to_date)); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));
}else{
	$date1 = date('Y-m-d', strtotime("last ".constant("week_start_day")));
	$to_date = strtotime($date1); 
	$date2 = date('Y-m-d', strtotime("+1 day", $to_date));
	$date3 = date('Y-m-d', strtotime("+2 day", $to_date));
	$date4 = date('Y-m-d', strtotime("+3 day", $to_date));
	$date5 = date('Y-m-d', strtotime("+4 day", $to_date));
	$date6 = date('Y-m-d', strtotime("+5 day", $to_date));
	$date7 = date('Y-m-d', strtotime("+6 day", $to_date));	
}
include('header.php');

?>
<style>

td,
th {
  border: 1px solid #000;
  width: 100px;
}
td:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:#f9f9f9;
}
th:first-child {
  position:sticky;
  left:0;
  z-index:1;
  background-color:#fff;
}
td:nth-child(2),th:nth-child(2)  { 
  position:sticky;
  left:75px;
  z-index:1;
  background-color:white;
}
td:nth-child(2),th:nth-child(2)  { 
  z-index:3;
}

th:first-child, th:last-child {z-index:2;}

</style>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Employees Timesheet Report </h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box"  style="overflow: scroll; overflow: auto;">
            <div class="box-header">
              <h3 class="box-title">Employees Timesheet Report</h3>          
            </div>
            <div class="box-body"><div>
              <table id="example1" class="table table-bordered table-striped" >
              <thead>
                  <tr>
                    <th align="center" class="tbl_design1">Week End</th>
                    <th align="center" class="tbl_design1"><?php echo $date7; ?></th>
                    <th align="center" class="tbl_design1 text-center" colspan="7" >TUE<?php echo " ( ".$date1." )";?></th>
                    <th align="center" class="tbl_design1 text-center" colspan="7" >WED<?php echo " ( ".$date2." )";?></th>
                    <th align="center" class="tbl_design1 text-center" colspan="7" >THU<?php echo " ( ".$date3." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="7" >FRI<?php echo " ( ".$date4." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="7" >SAT<?php echo " ( ".$date5." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="7" >SUN<?php echo " ( ".$date6." )";?></th> 
                    <th align="center" class="tbl_design1 text-center" colspan="7" >MON<?php echo " ( ".$date7." )";?></th>                                    
                  </tr>
                </thead>
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">Name</th>
                    <th align="left" class="tbl_design1">SurName</th>
					<th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
					<th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
					<th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    <th align="left" class="tbl_design1">Rostered Time</th>
					<th align="left" class="tbl_design1">Actual Time</th>
                    <th align="left" class="tbl_design1">Meal Break Time</th>
					<th align="left" class="tbl_design1">Before 6AM</th>
					<th align="left" class="tbl_design1">After 10PM</th>
                    <th align="left" class="tbl_design1">Normal</th>
                    <th align="left" class="tbl_design1">Total</th>
                    
                    
                  </tr>
                </thead>
                <tbody>
                <?php
                	//print_r(mysqli_fetch_assoc($oEmp_DA->Emp_Select()));die;
                	$result = $oEmp_DA->Emp_Select_week_timesheet($date1 , $date7);
					$i = 0;
					$normalTotalTue = 0; $afterTotalTue = 0; $beforeTotalTue = 0; $totalHourTue = 0;
					$normalTotalWed = 0; $afterTotalWed = 0; $beforeTotalWed = 0; $totalHourWed = 0;
					$normalTotalThu = 0; $afterTotalThu = 0; $beforeTotalThu = 0; $totalHourThu = 0;
					$normalTotalFri = 0; $afterTotalFri = 0; $beforeTotalFri = 0; $totalHourFri = 0;
					$normalTotalSat = 0; $afterTotalSat = 0; $beforeTotalSat = 0; $totalHourSat = 0;
					$normalTotalSun = 0; $afterTotalSun = 0; $beforeTotalSun = 0; $totalHourSun = 0;
					$normalTotalMon = 0; $afterTotalMon = 0; $beforeTotalMon = 0; $totalHourMon = 0;
					foreach($result as $row)
                	{
						//print_r($row[$date2]);
						//print("<pre>".print_r($row[$date2],true)."</pre>");
						$user_name = get_emp_name($row[$date1]['fk_employee_id']);
						
					?>
                	<tr>
                    <!-- TUE -->
                    
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $user_name['em_first_name']; ?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $user_name['em_family_name']; ?></td>
                        
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date1]['rostered_time'].'-'.$row[$date1]['rostered_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date1]['actual_time'].'-'.$row[$date1]['actual_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date1]['meal_break_time'].'-'.$row[$date1]['meal_break_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date1]['before_6am']; $beforeTotalTue = $beforeTotalTue + $row[$date1]['before_6am'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date1]['after_10pm']; $afterTotalTue = $afterTotalTue + $row[$date1]['after_10pm'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date1]['normal_hours']; $normalTotalTue = $normalTotalTue + $row[$date1]['normal_hours'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date1]['total_hours']; $totalHourTue = $totalHourTue + $row[$date1]['total_hours']; ?></td>
                        
                     <!-- WED -->
                        
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date2]['rostered_time'].'-'.$row[$date2]['rostered_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date2]['actual_time'].'-'.$row[$date2]['actual_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date2]['meal_break_time'].'-'.$row[$date2]['meal_break_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date2]['before_6am']; $beforeTotalWed = $beforeTotalWed + $row[$date2]['before_6am'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date2]['after_10pm']; $afterTotalWed = $afterTotalWed + $row[$date2]['after_10pm'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date2]['normal_hours']; $normalTotalWed = $normalTotalWed + $row[$date2]['normal_hours']; ?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date2]['total_hours']; $totalHourWed = $totalHourWed + $row[$date2]['total_hours'];?></td>
                     
                     <!-- THU -->
                        
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date3]['rostered_time'].'-'.$row[$date3]['rostered_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date3]['actual_time'].'-'.$row[$date3]['actual_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date3]['meal_break_time'].'-'.$row[$date3]['meal_break_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date3]['before_6am']; $beforeTotalThu = $beforeTotalThu + $row[$date3]['before_6am'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date3]['after_10pm']; $afterTotalThu = $afterTotalThu + $row[$date3]['after_10pm'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date3]['normal_hours']; $normalTotalThu = $normalTotalThu + $row[$date3]['normal_hours']; ?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date3]['total_hours']; $totalHourThu = $totalHourThu + $row[$date3]['total_hours'];?></td>
                     
                     <!-- FRI -->
                        
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date4]['rostered_time'].'-'.$row[$date4]['rostered_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date4]['actual_time'].'-'.$row[$date4]['actual_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date4]['meal_break_time'].'-'.$row[$date4]['meal_break_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date4]['before_6am']; $beforeTotalFri = $beforeTotalFri + $row[$date4]['before_6am'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date4]['after_10pm']; $afterTotalFri = $afterTotalFri + $row[$date4]['after_10pm'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date4]['normal_hours']; $normalTotalFri = $normalTotalFri + $row[$date4]['normal_hours'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date4]['total_hours']; $totalHourFri = $totalHourFri + $row[$date4]['total_hours'];?></td>
                     
                     <!-- SAT -->
                        
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date5]['rostered_time'].'-'.$row[$date5]['rostered_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date5]['actual_time'].'-'.$row[$date5]['actual_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date5]['meal_break_time'].'-'.$row[$date5]['meal_break_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date5]['before_6am']; $beforeTotalSat = $beforeTotalSat + $row[$date5]['before_6am'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date5]['after_10pm']; $afterTotalSat = $afterTotalSat + $row[$date5]['after_10pm'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date5]['normal_hours']; $normalTotalSat = $normalTotalSat + $row[$date5]['normal_hours'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date5]['total_hours']; $totalHourSat = $totalHourSat + $row[$date5]['total_hours'];?></td>
                     
                     <!-- SUN -->
                        
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date6]['rostered_time'].'-'.$row[$date6]['rostered_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date6]['actual_time'].'-'.$row[$date6]['actual_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date6]['meal_break_time'].'-'.$row[$date6]['meal_break_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date6]['before_6am']; $beforeTotalSun = $beforeTotalSun + $row[$date6]['before_6am'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date6]['after_10pm']; $afterTotalSun = $afterTotalSun + $row[$date6]['after_10pm'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date6]['normal_hours']; $normalTotalSun = $normalTotalSun + $row[$date6]['normal_hours']; ?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date6]['total_hours']; $totalHourSun = $totalHourSun + $row[$date6]['total_hours'];?></td>
                        
                     <!-- MON -->
                        
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date7]['rostered_time'].'-'.$row[$date7]['rostered_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date7]['actual_time'].'-'.$row[$date7]['actual_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date7]['meal_break_time'].'-'.$row[$date7]['meal_break_time_end'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date7]['before_6am']; $beforeTotalMon = $beforeTotalMon + $row[$date7]['before_6am']; ?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date7]['after_10pm']; $afterTotalMon = $afterTotalMon + $row[$date7]['after_10pm'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date7]['normal_hours']; $normalTotalMon = $normalTotalMon + $row[$date7]['normal_hours'];?></td>
                        <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row[$date7]['total_hours']; $totalHourMon = $totalHourMon + $row[$date7]['total_hours'];?></td>
                       
						</tr>
             <?php } ?>
         		</tbody>
                <tfoot>
                  <tr>
                    <th align="left" class="tbl_design1">Total</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>                    
					<th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1"><?php echo $beforeTotalTue; ?></th>
					<th align="left" class="tbl_design1"><?php echo $afterTotalTue; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $normalTotalTue; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $totalHourTue; ?></th>
                    
					<th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1"><?php echo $beforeTotalWed; ?></th>
					<th align="left" class="tbl_design1"><?php echo $afterTotalWed; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $normalTotalWed; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $totalHourWed; ?></th>
                    
					<th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1"><?php echo $beforeTotalThu; ?></th>
					<th align="left" class="tbl_design1"><?php echo $afterTotalThu; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $normalTotalThu; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $totalHourThu; ?></th>
                    
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1"><?php echo $beforeTotalFri; ?></th>
					<th align="left" class="tbl_design1"><?php echo $afterTotalFri; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $normalTotalFri; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $totalHourFri; ?></th>
                    
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1"><?php echo $beforeTotalSat; ?></th>
					<th align="left" class="tbl_design1"><?php echo $afterTotalSat; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $normalTotalSat; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $totalHourSat; ?></th>
                    
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1"><?php echo $beforeTotalSun; ?></th>
					<th align="left" class="tbl_design1"><?php echo $afterTotalSun; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $normalTotalSun; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $totalHourSun; ?></th>
                    
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1">&nbsp;</th>
                    <th align="left" class="tbl_design1">&nbsp;</th>
					<th align="left" class="tbl_design1"><?php echo $beforeTotalMon; ?></th>
					<th align="left" class="tbl_design1"><?php echo $afterTotalMon; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $normalTotalMon; ?></th>
                    <th align="left" class="tbl_design1"><?php echo $totalHourMon; ?></th>
                    
                    
                  </tr>
                </tfoot>
         	</table></div>
         </div>
     </div>
 </div>
</div>
</section>
<script language="javascript">
var i = 1;
function change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}
function resign_change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=res&eid="+eid+"&ems="+empstatus+"";
}
function datechange(datas)
{
	if(i == 1)
	{
		var del=confirm("Are you sure want to resign this employee?");
		if (del==true){
			window.location.href="Activeemployee.php?mode=res&eid="+datas.id+"&ems=2&resign_date="+datas.value;
		 //  alert ("Activeemployee.php?mode=res&eid="+datas.id+"&ems=2&resign_date="+datas.value)
		}else{
			//break;
		}
	}else if(i == 3)
	{
		i = 0;	
	}
	i++;
}
</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>