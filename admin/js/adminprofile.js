function check()
{
	var uname = document.getElementById('txtuname').value;
    if(trim(uname)=='')
    { 
    	alert('Please Enter Username.'); 
		document.getElementById('txtuname').focus();
		return false;	 
    }
  
    if(uname.length < 5 &&  uname.length > 20)
	{
		alert('Username must have 5 to 20 characters.');
		document.getElementById('txtuname').focus();
		return false;
	}
	
	var pass = document.getElementById('txtpass').value;
    if(trim(pass)=='')
    { 
    	alert('Please Enter Password.'); 
		document.getElementById('txtpass').focus();
		return false;	 
    }
	
    if(pass.length < 5 &&  pass.length > 16)
    { 
    	alert('Password must have 5 to 16 characters.'); 
		document.getElementById('txtpass').focus();
		return false;	 
    }

  	if(trim(document.getElementById('txtemail').value)=='')
  	{ 
   		alert('Please Enter Email Address.'); 
		document.getElementById('txtemail').focus();
		return false;	 
    }
	
    if(!emailcheck(document.getElementById("txtemail").value))
    {
		alert('Please Enter Valid Email Address.');
		document.getElementById('txtemail').focus();		
		return false;		 
    }
}
function trim(stringToTrim) 
{
  return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function emailcheck(str)
{
	emailF = false;
	if(str!="")
		{
			var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			var ans = str.match(emailExp);
			if(ans == str)
			{
				emailF = true;
			}
		}	
	return emailF;
}
