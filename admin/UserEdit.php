<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oUser_CDO = new clsUser_CDO();
$oUser_DA = new clsUser_DA();
					//	print_r($oUser_DA->User_Select());exit();
if(isset($_POST['submit']))
{
	$user_data = $oUser_DA->User_Edit($_POST);
	//echo $user_data;exit;
	if($user_data)
	{
		header('Location: User.php');	
	}	
	else
	{
		echo "Your data can't Updated";
	}
}
else
{
	if($_GET['id'])
	{
		$user_detail = $oUser_DA->User_Detail($_GET['id']);
		//print_r($user_detail);exit;
	}
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Update User Detail </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	<div class="form-group">
	    		          <label>First Name:</label>
			              <input name="f_name" type="text" id="f_name" class="input form-control" tabindex="1" value="<?php echo $user_detail['first_name'] ?>" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>Last Name:</label>
			              <input name="l_name" type="text" id="l_name" class="input form-control" tabindex="2" value="<?php echo $user_detail['last_name'] ?>" required="true">
	            		</div>
	            		<div class="form-group">
	    		          <label>Username:</label>
			              <input name="username" type="text" id="username" class="input form-control" tabindex="3" value="<?php echo $user_detail['username'] ?>" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>E-mail:</label>
			              <input name="email" type="email" id="email" class="input form-control" tabindex="4" value="<?php echo $user_detail['email_address'] ?>" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>Password:</label>
			              <input name="password" type="text" id="password" class="input form-control" value="<?php echo $user_detail['password'] ?>" tabindex="5" required="true">
	            		</div>
		            	<div class="input form-group">
	    		          <label>User Type:</label>
	    		          <select class="form-control" name="u_type" id="u_type" tabindex="6" required="true">
	    		          	<option value="0" <?php if($user_detail['user_type'] == '0') {?> selected="true" <?php } ?> >Super Admin</option>
	    		          	<option value="1"  <?php if($user_detail['user_type'] == '1') {?> selected="true" <?php } ?> >Store Manager</option>
	    		          	<option value="2"  <?php if($user_detail['user_type'] == '2') {?> selected="true" <?php } ?>>Assistant Manager</option>
	    		          </select>
	            		</div>
	            		<div class="form-group">
	    		          <label>User Status:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="u_status" id="active" value="1"  <?php if($user_detail['user_status'] == '1') {?> checked <?php } ?> id="u_status"  > Active
	    		          	&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="u_status" id="deactive" value="0"  <?php if($user_detail['user_status'] == '0') {?> checked <?php } ?> id="u_status"  > Deactive
	            		</div>
	            		<div class="form-group">
	            			<label>Store to Allow:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	            			<?php  
								$oStore_DA = new clsStore_DA();
								 $result1 = $oStore_DA->Select_Store_User($user_detail['id']);
								while ($row_user_store = mysqli_fetch_array($result1)) {
									//print_r($row_user_store);
									$user_store[] = $row_user_store['fk_store_id'];
								}
								$result = $oStore_DA->All_Store();
								 while($row = mysqli_fetch_assoc($result))
								 {
								 	 
								 	if (in_array($row['id'], $user_store))
									  {
									 ?>
									 	<input type="checkbox"  name="store[]" id="store" value="<?php echo $row['id'];?>"checked><?php echo " ".$row['st_name'];?>
									 <?php 
									  }
									else
									  {
									 ?>
									 	<input type="checkbox"  name="store[]" id="store" value="<?php echo $row['id'];?>"><?php echo " ".$row['st_name'];?>
									 <?php 
									  }

								 	?>
								 <?php }?>
								
                          
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Update User" onclick="return chk(this.form);">
	            		</div>
	            		<input type="hidden" name="id" id="id" value="<?php echo $user_detail['id'] ?>">

		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>	
<script language ="JavaScript">
			function chk(form){
			
			if ( ( form.u_status[0].checked == false ) && ( form.u_status[1].checked == false ) )
			{
			alert ( "Please Select User Status" );
			return false;
			}			
			 
			}
</script>
		
<?php include('footer.php'); ?>
<?php ob_flush();?>