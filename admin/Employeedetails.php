<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

$oStore_DA = new clsStore_DA();
					//	print_r($oUser_DA->User_Select());exit();

if(isset($_REQUEST['token']))
{
		$result = $oEmp_DA->Emp_Select_Id($_REQUEST['token']);
		
		$_REQUEST['id'] = $result;				
}

if(isset($_REQUEST['id']))
{
	$eid = $_REQUEST['id'];
	$employee_master = $oEmp_DA->Employee_Master($_REQUEST['id']);
  $store = $oStore_DA->Store_Detail($employee_master['store_id']);
	$employee_work_history = $oEmp_DA->Employee_History($_REQUEST['id']);
	$employee_info = $oEmp_DA->Employee_Info($_REQUEST['id']);
	$employee_apply_for = $oEmp_DA->Employee_Applyfor($_REQUEST['id']);
	$employee_qualification = $oEmp_DA->Employee_qualification($_REQUEST['id']);
	$employee_bank = $oEmp_DA->Employee_Bank($_REQUEST['id']);
	$employee_extra_forms = $oEmp_DA->Emp_extra_form($_REQUEST['id']);
}

//Edit user start
if(fnRequestParam('mode') == 'es') {
	$oEmp_CDO->id = fnRequestParam('eid');
	$oEmp_CDO->us = fnRequestParam('ems');
	$oEmp_DA->Emp_Status($oEmp_CDO);
	header("Location:Employeedetails.php?id=$oEmp_CDO->id");
	exit;
}
/*Edit user End*/

/*Delete user Data start*/


/*Delete user data end*/
if(isset($_REQUEST['search_submit'])) {
	unset($_REQUEST['msg']);
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> View Employee Details </h1> </section>
</div>
<br><br>
<section class="content">
<form name="frm" method="post" action="" enctype="multipart/form-data"  >
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">User</h3>
              <br>
	            <div class="box-body col-md-6">
	              <table id="example" class="table table-bordered table-striped">
                <tr>
              <td class="col-md-3"><b>Store Name</b></td><td colspan="3" class="col-md-3"><?php  echo $store['st_name'] ?></td>
            </tr>
						<tr>
							<td class="col-md-3"><b>Family Name</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_family_name'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>First Name</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_first_name'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Address</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_address'] ?></td>
						</tr>						
						<tr>
							<td class="col-md-3"><b>Postcode</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_postcode'] ?></td>
						</tr>
                        <tr>
							<td class="col-md-3"><b>Birthdate</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_birthdate'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Phone No </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_phone_no'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Mobile No</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_mobile_no'] ?></td>
						</tr>
                         <tr>
                          <td class="col-md-3"><b>Kin name</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_kin_name'] ?></td>
						</tr>
                         <tr>
                          <td class="col-md-3"><b>Kin relation</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_kin_relation'] ?></td>
						</tr>
                         <tr>
                          <td class="col-md-3"><b>Kin addres</b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_kin_address'] ?></td>
						</tr>
                         <tr>
                          <td class="col-md-3"><b>Kin Contact </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_kin_contact'] ?></td>
						</tr>
                         <tr>
                          <td class="col-md-3"><b>Kin  email </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_kin_email'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>legally entitled to work in Australia</b></td><td colspan="3" class="col-md-3"><?php if($employee_master['em_entitled_austrailia'] == "1"){ echo "Yes"; }else { echo "No"; } ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Identification to verify </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_identity_doc'] ?></td>
						</tr>
						<tr>
							<td class="col-md-3"><b>Currently Study </b></td><td colspan="3" class="col-md-3"><?php if($employee_master['em_current_study_status'] == "0" ){ echo "No"; }else { echo "Yes";}  ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Study Type </b></td><td colspan="3" class="col-md-3"><?php $new=explode("#",$employee_master['em_study_type']); if($new[1] == ''){ $tag = '.'; }else{ $tag = ',';} echo $new[0].$tag.$new[1]; ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Institution attend </b></td><td colspan="3" class="col-md-3"><?php $new=explode("#",$employee_master['em_institute_type']); $tag=''; foreach($new as $ne){   if ($ne==""){ }else {echo $tag.$ne; $tag=' , ';} } ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Name Of highschool attend </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_hs_name'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Highest Grade/level passed </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_hs_grade'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Passed Year </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_hs_year'] ?></td>
						</tr>
                        <tr>
                          <td rowspan="4" class="col-md-3"><b>Employee Qualifications </b></td><td class="col-md-3"><b>When commenced </b></td>
                          <td class="col-md-3"><b>Year completed</b></td>
                          <td class="col-md-3"><b>Qualifications</b></td>
						</tr>
                    
                        
                      <?php 
					  $newc=explode("#", $employee_master['em_ps_commenced']);
					   $newy= explode("#",$employee_master['em_ps_year']);
					   $newq=explode("#",$employee_master['em_qualification']);
					  /*?>  
                        
                          <td class="col-md-3"><?php $new=explode("#", $employee_master['em_ps_commenced']); echo $new[0]." , ".$new[1]." , ".$new[2]; ?></td>
                          <td class="col-md-3"><?php $new= explode("#",$employee_master['em_ps_year']); echo $new[0]." , ".$new[1]." , ".$new[2]; ?></td>
                          <td class="col-md-3"><?php $new=explode("#",$employee_master['em_qualification']); foreach($new as $ne){ if ($ne==""){}else {echo $ne.", ";} } ?></td>
                          
                          
                        </tr><?php */?>
                       
                        <?php 
						if( $employee_qualification->totalRows == 0)
						{for($i=0;$i<3;$i++) {?>
						
                        	<tr>
                         
                          <td class="col-md-3"><?php echo $newc[$i]; ?></td>
                          <td class="col-md-3"><?php echo  $newy[$i]; ?></td>
                          <td class="col-md-3"><?php echo $newq[$i]; ?></td>
                         </tr>
                        <?php	}
						}else{
						for($i=0;$i<$employee_qualification->totalRows;$i++) {?>
                          <tr>
                         
                          <td class="col-md-3"><?php echo $employee_qualification->year_commenced[$i]; ?></td>
                          <td class="col-md-3"><?php echo $employee_qualification->year_completed[$i]; ?></td>
                          <td class="col-md-3"><?php echo $employee_qualification->employe_qulification[$i]; ?></td>
                         </tr>
                          <?php }} ?>
                        
                    <?php /*?>    <tr>
                          <td class="col-md-3"><b>Year completed post secondary </b></td><td colspan="3" class="col-md-3"><?php $new= explode("#",$employee_master['em_ps_year']); echo $new[0]." , ".$new[1]." , ".$new[2]; ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Post secondary qualifications obtained </b></td><td colspan="3" class="col-md-3"><?php $new=explode("#",$employee_master['em_qualification']); foreach($new as $ne){ if ($ne==""){}else {echo $ne.", ";} } ?></td>
						</tr><?php */?>
                        <tr>
                          <td class="col-md-3"><b>Employee Signature </b></td><td colspan="3" class="col-md-3" id="displaySignature"></td>
                    
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Employee Signature Date </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['em_signature_date'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Terms And Conditions </b></td><td colspan="3" class="col-md-3"><?php if($employee_master['em_terms_con']=="1"){ echo "Checked";}else { echo "Not checked";} ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Date Of submition </b></td><td colspan="3" class="col-md-3"><?php echo $employee_master['created_date'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Job Title </b></td><td colspan="3" class="col-md-3"><?php $new=explode("#", $employee_apply_for['em_job_title']); $tag=' '; foreach($new as $ne){  if ($ne==""){}else {echo $tag.$ne; $tag=" , ";} } ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Status of position apply for </b></td><td colspan="3" class="col-md-3"><?php $new=explode("#",$employee_apply_for['em_position_status']);$tag=' '; foreach($new as $ne){  if ($ne==""){}else {echo $tag.$ne; $tag=" , ";} } ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Employee working hours </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_working_hours']; ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Any circumstances </b></td><td colspan="3" class="col-md-3"><?php $new=explode("#",$employee_apply_for['em_limit_availability']); $tag=' '; foreach($new as $ne){  if ($ne==""){}else {echo $tag.$ne; $tag=" , ";} } ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Monday Detail </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_monday_detail'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Tuesday Detail </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_tuesday_detail'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Wednesday Detail </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_wednesday_detail'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Thursday Detail </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_thursday_detail'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Friday Detail </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_friday_detail'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Saturday Detail </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_saturday_detail'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Sunday Detail </b></td><td colspan="3" class="col-md-3"><?php echo $employee_apply_for['em_sunday_detail'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Employer Name /address/ telephone </b></td><td colspan="3" class="col-md-3"><?php echo $employee_work_history['em_employer_name'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Work From / to</b></td><td colspan="3" class="col-md-3"><?php echo $employee_work_history['em_from_to'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Last job position </b></td><td colspan="3" class="col-md-3"><?php echo $employee_work_history['em_position'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Reason for Leaving </b></td><td colspan="3" class="col-md-3"><?php echo $employee_work_history['em_reason'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>Refrence Name and Phone </b></td><td colspan="3" class="col-md-3"><?php echo $employee_work_history['em_reference'] ?></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>List of skills/knowlegde or experience </b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_skill'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>List of Hobby </b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_hobby'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Prepared Attend  medical examination </b></td><td colspan="3" class="col-md-3"><?php if($employee_info['em_prep_medical'] == "1"){echo "Yes";}else {echo "No";} ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Status Of physical or mental disability </b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_physical_disability_status'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Nature Of provide to us </b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_nature_of_work'] ?></td>
						</tr> 
                         <tr>
                           <td class="col-md-3"><b>Injury Status </b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_injury_status'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Criminal Status </b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_criminal_status'] ?></td>
						</tr>
                       <tr>
                         <td class="col-md-3"><b>Charges Status</b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_charges_status'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Additional info by employee </b></td><td colspan="3" class="col-md-3"><?php echo $employee_info['em_additional_info'] ?></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Confidentiality Agreement </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_con_agreement']);if($new[3]==""){ echo "#";}else{ echo "upload/Confidentiality_Agreement/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?>t</a></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Crew Member Acknowledgment </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_crewm_Ack']); if($new[3]==""){ echo "#";}else{ echo "upload/Crew_Member_Acknowledgment/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Employee Cash Drawer Policy </b></td><td colspan="3" class="col-md-3"><a  href="<?php $new=explode("/",$employee_info['em_cashdrawer_policy']); if($new[3]==""){ echo "#";}else{ echo "upload/Employee_Cash_Drawer_Policy/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Crew/Staff letter off Employment offer </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_cre_letteroff']); if($new[3]==""){ echo "#";}else{ echo "upload/CrewStaff_letter_off_Employment_offer/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <tr>
                           <td class="col-md-3"><b>Fair Work Information Statement </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_fair_workstat']); if($new[3]==""){ echo "#";}else{ echo "upload/Fair_Work_Information_Statement/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>WHS FORM </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_whs_form']); if($new[3]==""){ echo "#";}else{ echo "upload/WHS_FORM/".$new[3]; }?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                        <tr>
                          <td class="col-md-3"><b>WHS Responsibility </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['em_whs_responsibility']); if($new[3]==""){ echo "#";}else{ echo "upload/WHS_Responsibility/".$new[3]; } ?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <td class="col-md-3"><b>Employee superannuation choice form </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['emp_super_choice_form']); if($new[3]==""){ echo "#";}else{ echo "upload/Employee_superannuation_choice_form/".$new[3]; } ?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                         <td class="col-md-3"><b>Uniform Deduction </b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['emp_uniform_dedu']); if($new[3]==""){ echo "#";}else{ echo "upload/Uniform_Deduction/".$new[3]; } ?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                        
                         <td class="col-md-3"><b>Employee Uniform Policy Form</b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['emp_uniform_policy_form']); if($new[3]==""){ echo "#";}else{ echo "upload/Uniform_policy/".$new[3]; } ?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr>
                        
                         <td class="col-md-3"><b>Employee Tax Declaration Form</b></td><td colspan="3" class="col-md-3"><a href="<?php $new=explode("/",$employee_info['emp_tfn_dec_form']); if($new[3]==""){ echo "#";}else{ echo "upload/Tax_declaration_form/".$new[3]; } ?>" target="_blank"><?php if($new[3]==""){ echo "No Document Atteched";}else { echo "View Document";} ?></a></td>
						</tr> 
                        <?php 
						for($i=0;$i<$employee_extra_forms->totalRows;$i++) {?>
                          <tr>
                         <td class="col-md-3"><strong>Extra Form</strong></td>
                          <td class="col-md-3" colspan="3"><a href=<?php echo $employee_extra_forms->form_url[$i]; ?> target="_blank">View Document</a></td>
                         </tr>
						<?php } ?>
                        
                         <td class="col-md-3"><b>&nbsp; </b></td><td colspan="3" class="col-md-3">Bank Details ( PRIMARY )</td>
						</tr>
                         <td class="col-md-3"><b>Bank Name </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[0]['bank_name']; ?></td>
						</tr>
                         <td class="col-md-3"><b>Branch </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[0]['branch_name']; ?></td>
						</tr>
                         <td class="col-md-3"><b>Account Name </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[0]['account_name']; ?></td>
						</tr>
                         <td class="col-md-3"><b>BSB Number </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[0]['bsb_number']; ?></td>
						</tr>
                         <td class="col-md-3"><b>Account Number </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[0]['account_number']; ?></td>
						</tr>
                        
                          <td class="col-md-3"><b>&nbsp; </b></td><td colspan="3" class="col-md-3">Bank Details ( SECONDARY )</td>
						</tr>
                         <td class="col-md-3"><b>Bank Name </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[1]['bank_name']; ?></td>
						</tr>
                         <td class="col-md-3"><b>Branch </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[1]['branch_name']; ?></td>
						</tr>
                         <td class="col-md-3"><b>Account Name </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[1]['account_name']; ?></td>
						</tr>
                         <td class="col-md-3"><b>BSB Number </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[1]['bsb_number']; ?></td>
						</tr>
                         <td class="col-md-3"><b>Account Number </b></td><td colspan="3" class="col-md-3"><?php echo $employee_bank[1]['account_number']; ?></td>
						</tr>
                        
                        
                        
                         <tr>
                           <td class="col-md-3"><b>Employee Status </b></td><td colspan="3" class="col-md-3">
                         <select name="status"  onchange="return change_status(<?php echo $eid ;?>,this.value)">
                         	<option value="0"<?php if($employee_master['employee_status']== "0"){ echo"selected";}?>>Pending</option>
                            <option value="1"<?php if($employee_master['employee_status']== "1"){ echo"selected";}?>>Approve</option>
                            <option value="2"<?php if($employee_master['employee_status']== "2"){ echo"selected";}?>>Decline</option>
                         </select></td>
						</tr>
	              </table>
	            </div>
                    
                   
                       <script>
                             $(document).ready(function(data){
                               var i = new Image()
                               var signature ='<?php echo $employee_master['em_signature']; ?>';
                        //Here signatureDataFromDataBase is the string that you saved earlier
                                i.src = 'data:' + signature;
                                $(i).appendTo('#displaySignature')
                               })
                       </script>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<script language="javascript">

function change_status(eid,empstatus)
{
		
		window.location.href="Employeedetails.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}


</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>