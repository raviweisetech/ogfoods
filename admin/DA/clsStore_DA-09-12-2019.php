<?php

class clsStore_DA
{
	function getStoreAdmin()
	{
		global $link;

		$query = " SELECT id,first_name,last_name FROM tbl_user WHERE user_type='1' AND user_status='1' ";
		$result = mysqli_query($link,$query) or die(mysqli_error());

		return $result;
	}
	function getStoreManager()
	{
		global $link;

		$query = " SELECT * FROM tbl_user  ";
		$result = mysqli_query($link,$query) or die(mysqli_error());

		return $result;
	}
	function getCountries()
	{
		global $link;

		$country_query = " SELECT * FROM countries ";
		$country_result = mysqli_query($link,$country_query) or die(mysqli_error());

		return $country_result;
	}

	function Store_Create($data)
	{
		global $link;

		$create_store = " INSERT INTO tbl_store( st_name, st_address, st_country, st_state, st_city , st_status, st_owner, created_date, modified_date) VALUES ( '".$data['st_name']."', '".$data['address']."', '".$data['country']."', '".$data['state']."', '".$data['city']."', '".$data['st_status']."', '".$data['st_admin']."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."') ";

		$res_create = mysqli_query($link,$create_store) or die(mysqli_error($link));
		if($res_create)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function Select_Store($strr)
	{
		global $link;
		
		$all_store = " SELECT * FROM tbl_store where id = '".$strr."' ";
		
		$res = execute_query($all_store,$link) or die(mysqli_error($link));
		return $res;
	}
	function Select_Store_User($strr)
	{
		global $link;
		
		$all_store = " SELECT * FROM tbl_user_store where fk_user_id = '".$strr."' ";
		
		$res = execute_query($all_store,$link) or die(mysqli_error($link));
		return $res;
	}
	
	
	function All_Store($strr)
	{
		global $link;

		$all_store = " SELECT * FROM tbl_store ";
		
		$res = execute_query($all_store,$link) or die(mysqli_error($link));
		return $res;
	}

	function getCountry($id)
	{
		global $link;

		$query = " SELECT name FROM countries WHERE id = '".$id."' ";
		$result = mysqli_query($link,$query) or die(mysqli_error($link));
		$row = mysqli_fetch_assoc($result);

		return $row['name'];
	}

	function getState($id)
	{
		global $link;

		$query = " SELECT name FROM states WHERE id = '".$id."' ";
		$result = mysqli_query($link,$query) or die(mysqli_error($link));
		$row = mysqli_fetch_assoc($result);

		return $row['name'];
	}

	function getCity($id)
	{
		global $link;

		$query = " SELECT name FROM cities WHERE id = '".$id."' ";
		$result = mysqli_query($link,$query) or die(mysqli_error($link));
		$row = mysqli_fetch_assoc($result);

		return $row['name'];
	}

	function getStoreOwner($id)
	{
		global $link;

		$query = " SELECT first_name,last_name FROM tbl_user WHERE id = '".$id."' ";
		$result = mysqli_query($link,$query) or die(mysqli_error($link));
		$row = mysqli_fetch_assoc($result);

		return $row['first_name']." ".$row['last_name'];
	}

	function Store_Status($stid, $ss)
	{
		global $link;
		$sql = "UPDATE `tbl_store` SET `st_status` = '".$ss."' WHERE `id` ='".$stid."'";							   
		$res=mysqli_query($link,$sql) or die(mysqli_error($link));
	}

	function Store_Delete($stid)
	{
		global $link;
		$sqlDel = "delete from tbl_store where id = '".$stid."'";
		$resDel = execute_query($sqlDel,$link) or die(mysqli_error($link));
	}

	function Store_Detail($id)
	{
		global $link;
		$sql_detail = "SELECT * FROM tbl_store WHERE id = '".$id."'";
		$res_detail = mysqli_query($link,$sql_detail) or die(mysqli_error($link));
		$row_detail = mysqli_fetch_array($res_detail);
		if($row_detail)
		{
			return $row_detail;
		}
		else
		{
			return null;
		}
	}

	function Store_Edit($update_data)
	{
		global $link;
		$sql_update =  " UPDATE tbl_store SET st_name='".$update_data['st_name']."', st_address='".$update_data['address']."', st_country='".$update_data['country']."',st_state='".$update_data['state']."',st_city='".$update_data['city']."',st_status='".$update_data['st_status']."',st_owner='".$update_data['st_admin']."' ,modified_date='".date("Y-m-d H:i:s")."' WHERE id='".$update_data['id']."' ";

		$res_edit = mysqli_query($link,$sql_update) or die(mysqli_error($link));

		if($res_edit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

?>