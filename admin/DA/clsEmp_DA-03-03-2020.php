<?php
class clsEmp_DA
{
	function Emp_Select()
	{
		global $link;
		$sql_select_all = "SELECT em_id, store_id, em_family_name, em_first_name, em_address, em_postcode, em_phone_no, em_mobile_no,employee_status,employee_resign_date FROM employee_master where `employee_delete_status` IN('1','2')";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}
	function Emp_Select_payroll()
	{
		global $link;
		$sql_select_payroll = "SELECT DISTINCT `week_start_date` FROM `tbl_employee_timesheet_payroll`";
		$res = execute_query($sql_select_payroll,$link) or die(mysqli_error($link));
		return $res;
	}
	function Emp_extra_form_list($empId)
	{
		global $link;
		$sql_extra_form = "SELECT * FROM `employee_extra_form` where em_id='".$empId."'";
		$res = execute_query($sql_extra_form,$link) or die(mysqli_error($link));
		return $res;
	}
	
	function Emp_Timesheet( $userIds , $datafirst , $datalast)
	{
		global $link;
		$sql_select_all = "Select * from tbl_employee_timesheet where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$userIds."' and timesheet_date between '".$datafirst."' and '".$datalast."' ";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}
	
	function Emp_Timesheet_payroll( $userIds , $datafirst , $datalast)
	{
		global $link;
		$sql_select_all = "Select * from tbl_employee_timesheet_payroll where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$userIds."' and week_start_date = '".$datafirst."' and week_end_date = '".$datalast."' ";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}

	function Emp_Select_delete()
	{
		global $link;
		$sql_select_all = "SELECT em_id, store_id, em_family_name, em_first_name, em_address, em_postcode, em_phone_no, em_mobile_no,employee_status FROM employee_master where `employee_delete_status` = '0'";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}
	
	function Submit_Form($access_token)
	{
		global $link;
		$num = 0;
		$sql_select_all2 = mysqli_query($link,"SELECT * from `employee_master` where form_access_token = '".$access_token."'") or die(mysqli_error($link));
		$num = mysqli_num_rows($sql_select_all2);
		if($num > 0){ return 1;}else {return 0;}
	}
	
	function Emp_Select_Id($access_token)
	{
		global $link;
		$sql_id = mysqli_query($link,"SELECT em_id from `employee_master` where form_access_token = '".$access_token."'") or die(mysqli_error($link));
		$ids = mysqli_fetch_array($sql_id);
		return $ids['em_id'];
	}
	
	
	function Emp_Select_Detail()
	{
		global $link;
		$sql_select_all = "SELECT * from `employment_detail` order by id desc";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}
	
	function Emp_extra_form($id)
	{
		global $link;
		$sql_select_all = "SELECT * from `employee_extra_form` where em_id = '".$id."'";
		$res_quali = mysqli_query($link,$sql_select_all) or die(mysqli_error($link));

		$i=0;
		while($emp_exForm = mysqli_fetch_array($res_quali))
		{
			$this->form_url[$i] = $emp_exForm['form_url'];
			$i++;
		}
		$this->totalRows=$i;
		return $this;;
	}
	
	function Emp_Detail($id)
	{
		global $link;
		$sql_detail = "SELECT em_id, store_id, em_family_name, em_first_name, em_address, em_postcode, em_phone_no, em_mobile_no FROM employee_master WHERE em_id='".$id."'";
		$res_detail = mysqli_query($link,$sql_detail) or die(mysqli_error($link));
		$row_detail = mysqli_fetch_array($res_detail);
		if($row_detail)
		{
			return $row_detail;
		}
		else
		{
			return null;
		}
	}

	function Emp_Create($data)
	{
		global $link;
		$sql_create = "INSERT INTO employee_master(em_id, store_id, em_family_name, em_first_name, em_address, em_postcode, em_phone_no, em_mobile_no, em_entitled_austrailia, em_identity_doc, em_current_study_status, em_study_type, em_institute_type, em_hs_name, em_hs_grade, em_hs_year, em_ps_commenced, em_ps_year, em_qualification, em_signature, em_signature_date, created_date, updated_date) VALUES ('','','".$data['f_name']."','".$data['o_name']."','".$data['address']."','".$data['postcode']."','".$data['ph_no']."','".$data['mo_no']."','".null."','".null."','".null."', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."') ";

		$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
		if($res_create)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function Emp_all_payroll($date1 , $date7){
		global $link;
		$sql_select_all = "Select * from tbl_employee_timesheet_payroll where fk_store_id = '".$_SESSION['varStore']."' and week_start_date = '".$date1."' and week_end_date = '".$date7."' and `payroll_paid` = '0' ";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}
	function Emp_Select_week_timesheet($date1 , $date7)
	{
		global $link;
		$sql_select_all = "Select DISTINCT `fk_employee_id` from tbl_employee_timesheet";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		$i = 0;
		while($userId = mysqli_fetch_array($res))
		{
			$user_week_report = array();
			$sql_timesheet = $sql_sheet_check =  mysqli_query($link, "Select * from tbl_employee_timesheet where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$userId['fk_employee_id']."' and timesheet_date between '".$date1."' and '".$date7."'");
			$sql_num_sheet_add = mysqli_num_rows($sql_timesheet);
			while($data_timesheet = mysqli_fetch_array($sql_timesheet)){
				
				$user_week_report[$data_timesheet['timesheet_date']] = $data_timesheet;
			}
			if(!empty($user_week_report))
			{
				$all_user_records[] = $user_week_report;
			}
		}
		//print_r($all_user_records[1]);exit;
		return $all_user_records;
	}
	
	function Emp_Create_timesheet($data)
	{
		global $link;
		
		
		for ($x = 1; $x <= 7; $x++) {
			//echo $data['datepicker'.$x];
			if($data['totalHour'.$x] > 0){
				
				$sql_sheet_check =  mysqli_query($link, "Select * from tbl_employee_timesheet where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$data['fk_employee_id']."' and timesheet_date = '".$data['datepicker'.$x]."'");
				$sql_num_sheet_add = mysqli_num_rows($sql_sheet_check);
				
				if($sql_num_sheet_add < 1)
				{
					$holiday = 0;
					$sickLeave = 0;
					if($data['public_holiday'.$x] == 'on')
					{
						$holiday = '1';
					}
					if($data['sick_leave'.$x] == 'on')
					{
						$sickLeave = '1';
					}
					
						$sql_create = "INSERT INTO tbl_employee_timesheet( fk_store_id, fk_employee_id, timesheet_date, rostered_time, rostered_time_end, actual_time, actual_time_end, meal_break_time, meal_break_time_end, public_holiday, sick_leave, before_6am, after_10pm, normal_hours, total_hours, modified_date) VALUES ('".$_SESSION['varStore']."','".$data['fk_employee_id']."','".$data['datepicker'.$x]."','".$data['rostered_time'.$x]."','".$data['rostered_time_end'.$x]."','".$data['actual_time'.$x]."', '".$data['actual_time_end'.$x]."','".$data['meal_break_time'.$x]."', '".$data['meal_break_time_end'.$x]."','".$holiday."','".$sickLeave."','".$data['before6am'.$x]."','".$data['after10pm'.$x]."','".$data['normalHour'.$x]."','".$data['totalHour'.$x]."', '".date("Y-m-d H:i:s")."') ";
					//echo $sql_create;exit;
					$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
					
					//for total calculation
		
					$before6am_total_hours = 0;
					$after10pm_total_hours = 0;
					$saturday_hours = 0;
					$normal_total_hours = 0;
					$weekly_total_hours = 0;
					$normal_total_hours_week = 0;
					$sunday_hours = 0;
					$number_of_shifts = 1;
					$public_holiday_hours = 0;
					$sick_leave_hours = 0;
					
					$i=0;
					$sql_timesheet_total =  mysqli_query($link, "Select * from tbl_employee_timesheet where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$data['fk_employee_id']."' and timesheet_date between '".$data['datepicker1']."' and '".$data['datepicker7']."'");
					$sql_num_timesheet = mysqli_num_rows($sql_timesheet_total);
					while($rows_timesheet = mysqli_fetch_array($sql_timesheet_total))
					{
						$before6am_total_hours = $before6am_total_hours + $rows_timesheet['before_6am'];
						$after10pm_total_hours = $after10pm_total_hours + $rows_timesheet['after_10pm'];
						$weekly_total_hours = $weekly_total_hours + $rows_timesheet['total_hours'];
						
						$date = $rows_timesheet['timesheet_date'];
						$nameOfDay = date('D', strtotime($date));
						
						if($nameOfDay == 'Sun'){
							$sunday_hours = $rows_timesheet['total_hours'];
						}else if( $nameOfDay == 'Sat'){
							$saturday_hours = $rows_timesheet['total_hours'];
						}else if($rows_timesheet['public_holiday'] == 1){
							$public_holiday_hours = $public_holiday_hours + $rows_timesheet['total_hours'];
						}else if($rows_timesheet['sick_leave'] == 1){
							$sick_leave_hours = $sick_leave_hours + $rows_timesheet['total_hours'];
						}else{
							$normal_total_hours = $normal_total_hours +  $rows_timesheet['normal_hours'];
						}
						
						
					}		
					
				//end total calculation	
					
					$sql_check = mysqli_query($link, "Select * from tbl_employee_timesheet_payroll where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$data['fk_employee_id']."' and week_start_date = '".$data['datepicker1']."' and week_end_date = '".$data['datepicker7']."'");
					$sql_num_rows = mysqli_num_rows($sql_check);
					
					if($sql_num_rows > 0)
					{				
						$dataPayroll = mysqli_fetch_array($sql_check);	
						$number_of_shifts = $dataPayroll['number_of_shifts'] + $number_of_shifts ;
						
						$sql_update = mysqli_query($link, "UPDATE `tbl_employee_timesheet_payroll` SET
																	 `before6am_total_hours`='".$before6am_total_hours."',
																	`after10pm_total_hours`='".$after10pm_total_hours."',
																	`saturday_hours`='".$saturday_hours."',
																	`sunday_hours`='".$sunday_hours."',
																	`public_holiday_hours`='".$public_holiday_hours."',
																	`sick_leave_hours` ='".$sick_leave_hours."',
																	`normal_total_hours`='".$normal_total_hours."',																	
																	`weekly_total_hours`= '".$weekly_total_hours."',
																	`number_of_shifts`='".$number_of_shifts."',
																	`modified_date`= '".date("Y-m-d H:i:s")."' WHERE id = '".$dataPayroll['id']."'");
						
					}else{			
						$sql_update = mysqli_query($link,"INSERT INTO `tbl_employee_timesheet_payroll`( `fk_store_id`, `fk_employee_id`, `week_start_date`, `week_end_date`, `before6am_total_hours`, `after10pm_total_hours`, `saturday_hours`, `sunday_hours`, `public_holiday_hours`,`sick_leave_hours`,`normal_total_hours`, `weekly_total_hours`, `number_of_shifts`, `modified_date`) VALUES ( '".$_SESSION['varStore']."','".$data['fk_employee_id']."','".$data['datepicker1']."','".$data['datepicker7']."','".$before6am_total_hours."','".$after10pm_total_hours."','".$saturday_hours."','".$sunday_hours."','".$public_holiday_hours."','".$sick_leave_hours."','".$normal_total_hours."','".$weekly_total_hours."','1','".date("Y-m-d H:i:s")."' )");
						
					}
				}else{
					
					if( $data['curentDate'] == $data['datepicker'.$x] || $data['previDate'] == $data['datepicker'.$x] || $_SESSION['U_Type'] == 0)
					{
						
						
						$sql_timesheet_delete =  mysqli_query($link, "delete from tbl_employee_timesheet where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$data['fk_employee_id']."' and timesheet_date = '".$data['datepicker'.$x]."' ");
						
					$holiday = 0;
					$sickLeave = 0;
					
					if($data['public_holiday'.$x] == 'on')
					{
						$holiday = '1';
					}
					if($data['sick_leave'.$x] == 'on')
					{
						$sickLeave = '1';
					}
						$sql_create = "INSERT INTO tbl_employee_timesheet( fk_store_id, fk_employee_id, timesheet_date, rostered_time, rostered_time_end, actual_time, actual_time_end, meal_break_time, meal_break_time_end, public_holiday, sick_leave, before_6am, after_10pm, normal_hours, total_hours, modified_date) VALUES ('".$_SESSION['varStore']."','".$data['fk_employee_id']."','".$data['datepicker'.$x]."','".$data['rostered_time'.$x]."','".$data['rostered_time_end'.$x]."','".$data['actual_time'.$x]."', '".$data['actual_time_end'.$x]."','".$data['meal_break_time'.$x]."', '".$data['meal_break_time_end'.$x]."','".$holiday."','".$sickLeave."','".$data['before6am'.$x]."','".$data['after10pm'.$x]."','".$data['normalHour'.$x]."','".$data['totalHour'.$x]."', '".date("Y-m-d H:i:s")."') ";
					
						
					$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
					
					//for total calculation
		
					$before6am_total_hours = 0;
					$after10pm_total_hours = 0;
					$saturday_hours = 0;
					$normal_total_hours = 0;
					$weekly_total_hours = 0;
					$normal_total_hours_week = 0;
					$sunday_hours = 0;
					$number_of_shifts = 1;
					$public_holiday_hours = 0;
					$sick_leave_hours = 0;
					
					$i=0;
					$sql_timesheet_total =  mysqli_query($link, "Select * from tbl_employee_timesheet where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$data['fk_employee_id']."' and timesheet_date between '".$data['datepicker1']."' and '".$data['datepicker7']."'");
					$sql_num_timesheet = mysqli_num_rows($sql_timesheet_total);
					while($rows_timesheet = mysqli_fetch_array($sql_timesheet_total))
					{
						$before6am_total_hours = $before6am_total_hours + $rows_timesheet['before_6am'];
						$after10pm_total_hours = $after10pm_total_hours + $rows_timesheet['after_10pm'];
						$weekly_total_hours = $weekly_total_hours + $rows_timesheet['total_hours'];
						
						$date = $rows_timesheet['timesheet_date'];
						$nameOfDay = date('D', strtotime($date));
						
						if($nameOfDay == 'Sun'){
							$sunday_hours = $rows_timesheet['total_hours'];
						}else if( $nameOfDay == 'Sat'){
							$saturday_hours = $rows_timesheet['total_hours'];
						}else if($rows_timesheet['public_holiday'] == 1){
							$public_holiday_hours = $public_holiday_hours + $rows_timesheet['total_hours'];
						}else if($rows_timesheet['sick_leave'] == 1){
							$sick_leave_hours = $sick_leave_hours + $rows_timesheet['total_hours'];
						}else{
							$normal_total_hours = $normal_total_hours +  $rows_timesheet['normal_hours'];
						}
						
						
					}		
						
					$sql_check = mysqli_query($link, "Select * from tbl_employee_timesheet_payroll where fk_store_id = '".$_SESSION['varStore']."' and fk_employee_id = '".$data['fk_employee_id']."' and week_start_date = '".$data['datepicker1']."' and week_end_date = '".$data['datepicker7']."'");
					$sql_num_rows = mysqli_num_rows($sql_check);
					
					if($sql_num_rows > 0)
					{				
						$dataPayroll = mysqli_fetch_array($sql_check);	
						$number_of_shifts = $dataPayroll['number_of_shifts'] ;
						
						$sql_update = mysqli_query($link, "UPDATE `tbl_employee_timesheet_payroll` SET
																	 `before6am_total_hours`='".$before6am_total_hours."',
																	`after10pm_total_hours`='".$after10pm_total_hours."',
																	`saturday_hours`='".$saturday_hours."',
																	`sunday_hours`='".$sunday_hours."',
																	`public_holiday_hours`='".$public_holiday_hours."',
																	`sick_leave_hours` ='".$sick_leave_hours."',
																	`normal_total_hours`='".$normal_total_hours."',																	
																	`weekly_total_hours`= '".$weekly_total_hours."',
																	`number_of_shifts`='".$number_of_shifts."',
																	`modified_date`= '".date("Y-m-d H:i:s")."' WHERE id = '".$dataPayroll['id']."'");
						
					}else{			
						$sql_update = mysqli_query($link,"INSERT INTO `tbl_employee_timesheet_payroll`( `fk_store_id`, `fk_employee_id`, `week_start_date`, `week_end_date`, `before6am_total_hours`, `after10pm_total_hours`, `saturday_hours`, `sunday_hours`, `public_holiday_hours`,`sick_leave_hours`,`normal_total_hours`, `weekly_total_hours`, `number_of_shifts`, `modified_date`) VALUES ( '".$_SESSION['varStore']."','".$data['fk_employee_id']."','".$data['datepicker1']."','".$data['datepicker7']."','".$before6am_total_hours."','".$after10pm_total_hours."','".$saturday_hours."','".$sunday_hours."','".$public_holiday_hours."','".$sick_leave_hours."','".$normal_total_hours."','".$weekly_total_hours."','1','".date("Y-m-d H:i:s")."' )");
						
					}
						
						
					}
				}
			}
		}
	
		
	}
	
	

	function Emp_Edit($update_data)
	{
		global $link;
		$sql_update = "UPDATE employee_master SET em_family_name='".$update_data['f_name']."', em_first_name='".$update_data['o_name']."', em_address='".$update_data['address']."', em_postcode='".$update_data['postcode']."', em_phone_no='".$update_data['ph_no']."', em_mobile_no='".$update_data['mo_no']."', updated_date='".date("Y-m-d H:i:s")."' WHERE em_id='".$update_data['id']."'  ";

		$res_edit = mysqli_query($link,$sql_update) or die(mysqli_error($link));

		if($res_edit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	############################## Delete Record ###############################
	
	function Emp_Delete($oEmp_CDO)
	{
		global $link;
		$sqlDel = "Update employee_master SET `employee_delete_status` = '0' where em_id = '".$oEmp_CDO->id."'";
		$resDel = execute_query($sqlDel,$link) or die(mysqli_error($link));
	}
	function Emp_extraFrom_Delete($oEmp_CDO)
	{
		global $link;
		$sqlDel = "delete from employee_extra_form where id = '".$oEmp_CDO->id."'";
		$resDel = execute_query($sqlDel,$link) or die(mysqli_error($link));
	}
	############################ End Delete Record ##############################
	
	
	############################# Delete employee From pending ####################
	function Pending_Emp_Delete($oEmp_CDO)
	{
		global $link;
		$sqlDel = "Update employee_master SET `employee_delete_status` = '0' where form_access_token = '".$oEmp_CDO->token."'";
		$resDel = execute_query($sqlDel,$link) or die(mysqli_error($link));
		
		$sqlDelDet = "DELETE FROM employment_detail where form_access_token = '".$oEmp_CDO->token."'";
		$resDelDet = execute_query($sqlDelDet,$link) or die(mysqli_error($link));
		
		$sqlDelTok = "DELETE FROM employment_token where token = '".$oEmp_CDO->token."'";
		$resDelTok = execute_query($sqlDelTok,$link) or die(mysqli_error($link));
		
	}
	
	
	############################# End Delete employee From pending ####################
	
	function add_employment($detail)
	{
		global $link;
		
		
		$check = "SELECT * from employment_detail where email =  '".$detail['email']."'";
		
		$check_result = mysqli_query($link,$check) or die(mysqli_error($link));
		
		$norow = mysqli_num_rows($check_result);
		
	
		
		if(!empty($norow) || $detail['hidden_st_name'] == '')
		{
			if($detail['hidden_st_name'] == '')
			{
				return 0;
			}else{ return 3;}
			
		}else
		{
			if($detail['hidden_st_name'] == '')
			{
				$str_id = $_SESSION['varStoreSel']; 
				$sql_store = mysqli_query($link,"select * from `tbl_store` where id = '".$str_id."'");
				$store_name = mysqli_fetch_array($sql_store);
				$str_name = $store_name['st_name'];
			}else
			{
				$str_name = $detail['hidden_st_name'];	
			}
		
		$sql_create = "INSERT INTO employment_detail( email, message, form_link, store_id, form_access_token, sent_date) VALUES ( '".$detail['email']."', '".$detail['msg']."', '".$detail['link']."','".$detail['st_id']."', '".$detail['token']."', '".date("Y-m-d H:i:s")."')";

		$sql_token = " INSERT INTO employment_token (token) VALUES ('".$detail['token']."') ";

		$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
		$res_token = mysqli_query($link,$sql_token) or die(mysqli_error($link));
		
		}
		
		$body="<table width='50%' align='center'   style='border-style:dotted;' bordercolor='#CCC' >
					  <tr>
						<td width='7%' height='50px' align='left'>From </td>
						<td width='86%'>:: ".$detail['hidden_st_name']."</td>
						<td width='7%'></td>
					  </tr>
					  <tr>
						<td align='left'>Subject </td>
						<td>:: Employment Forms</td>
						<td width='7%'></td>
					  </tr>
					  <tr>
					  	<td colspan='3'  align='center'>Employment Forms Details Mail</td>
					  </tr>
					  <tr>
						<td align='right' height='150px'></td>
						<td rowspan='2' align='justify'><br>Hello ".$detail['fullname'].",<br><br>".$detail['msg']."<br><br>Please<a href='".$detail['link']."' style='text-decoration: blink;'> Click  Here </a>To Complete Employment Forms</td>
						<td></td>
					  </tr>
					  <tr>
					  	<td></td>
					  	<td ></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td align='left'><br>Thank you,<br>ogfoods<br><br></td>
						<td>&nbsp;</td>
					  </tr>
					   <tr>
						<td colspan='2' align='center'>&nbsp;</td>
					  </tr>
					</table>
				";

			/*$msg = wordwrap($body,70);

			// send email
			
			$mail = mail($detail['email'],"Employment Form",$msg);*/
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: '".$detail['hidden_st_name']."' <'OGFOODS'>\r\n";
			mail($detail['email'],"Employment Forms",$body,$headers);
		
			return 1;
		
	}
	
	function Employee_Master($id)
	{
		global $link;
		$sql_detail1 = "SELECT * FROM employee_master WHERE em_id='".$id."'";
		$res_detail1 = mysqli_query($link,$sql_detail1) or die(mysqli_error($link));
		$emp_mas = mysqli_fetch_array($res_detail1);
		
		return $emp_mas;
	}
	function Employee_Applyfor($id)
	{
		global $link;
		$sql_detail2 = "SELECT * FROM employee_applied_for WHERE em_id='".$id."'";
		$res_detail2 = mysqli_query($link,$sql_detail2) or die(mysqli_error($link));
		$emp_app = mysqli_fetch_array($res_detail2);
	
		return $emp_app;
	}
	function Employee_Info($id)
	{	
		global $link;
		$sql_detail3 = "SELECT * FROM employee_general_info WHERE em_id='".$id."'";
		$res_detail3 = mysqli_query($link,$sql_detail3) or die(mysqli_error($link));
		$emp_gen = mysqli_fetch_array($res_detail3);
		
		return $emp_gen;
	}
	function Employee_History($id)
	{	
		global $link;
		$sql_detail = "SELECT * FROM employee_work_history WHERE em_id='".$id."'";
		$res_detail = mysqli_query($link,$sql_detail) or die(mysqli_error($link));
		$emp_his = mysqli_fetch_array($res_detail);
	
		return $emp_his;
		
	}
	function Employee_Bank($id)
	{	
		global $link;
		$sql_detailb = "SELECT * FROM tbl_user_bank_detail WHERE fk_employee_id='".$id."' order by id asc";
		$res_detailb = mysqli_query($link,$sql_detailb) or die(mysqli_error($link));
		$i = 0;
		while($emp_bank = mysqli_fetch_array($res_detailb))
		{
			$bank_data[$i] = $emp_bank;
			$i++;
		}
	
		return $bank_data;
		
	}
	function Employee_qualification($id)
	{
		global $link;
		$sql_quali = "SELECT * FROM employee_qualification WHERE employee_id='".$id."' order by id asc";
		$res_quali = mysqli_query($link,$sql_quali) or die(mysqli_error($link));
	//	$emp_quali = mysqli_fetch_array($res_quali);
		$i=0;
		while($emp_quali = mysqli_fetch_array($res_quali))
		{
			$this->year_commenced[$i] = $emp_quali['year_commenced'];
			$this->year_completed[$i] = $emp_quali['year_completed'];
			$this->employe_qulification[$i] = $emp_quali['employe_qulification'];
			$i++;
		}
		$this->totalRows=$i;
		return $this;;
	}
	function Emp_Status($oEmp_CDO)
	{
			global $link;
			
			$sql_updat = "UPDATE employee_master SET employee_status='".$oEmp_CDO->us."' where em_id = '".$oEmp_CDO->id."'";
			
			$res_d = mysqli_query($link,$sql_updat) or die(mysqli_error($link));		
	}
	function Emp_resign_Status($oEmp_CDO)
	{
			global $link;
			
	
			$sql_updat = "UPDATE employee_master SET employee_delete_status= '".$oEmp_CDO->us."',`employee_resign_date` ='".$oEmp_CDO->resign_date."' where em_id = '".$oEmp_CDO->id."'";
			
			$res_d = mysqli_query($link,$sql_updat) or die(mysqli_error($link));		
	}
	function Update_Offer_Letter($data)
	{
			global $link;
			
			$token = rand(99999,1000000);

			$offer = $_FILES['offer_letter']['name'];
			if($offer != "")
			{
				$path="./upload/CrewStaff_letter_off_Employment_offer/";
			//	$spath="../upload/CrewStaff_letter_off_Employment_offer/";
				$npath=$path.$token.$offer;
				move_uploaded_file($_FILES['offer_letter']['tmp_name'],$npath);
				$offer_letter = $npath;
			}else
			{
				$offer_letter = "not selected";
			}
		//	print_r($_FILES);exit;
			$sql_update_offer_letter = mysqli_query($link,"UPDATE employee_general_info SET em_cre_letteroff = '".$offer_letter."',`updated_date` = '".date('Y-m-d H:i:s')."' where em_id = '".$data['employee_id']."'") or die(mysqli_error($link));
			
			if($sql_update_offer_letter)
			{
				return 1;	
			}else
			{
				return 0;
			}		
	}
	function Update_extra_form($data)
	{
			global $link;
			
			foreach($_FILES["extra_form"]["tmp_name"] as $key=>$tmp_name)
			{
				
					$token = rand(99999,1000000);
				//	echo $data['emp_id'];
					$extra = $_FILES['extra_form']['name'][$key];
					if($extra != "")
					{
						$path="./upload/Extra_Forms/";
					//	$spath="../upload/CrewStaff_letter_off_Employment_offer/";
						$npath=$path.$token.$extra;
						move_uploaded_file($_FILES['extra_form']['tmp_name'][$key],$npath);
						$extra_form = $npath;
					}else
					{
						$extra_form = "not selected";
					}
				//	print_r($_FILES);exit;
					$sql_update_offer_letter = mysqli_query($link,"INSERT INTO `employee_extra_form`(`em_id`,`extra_form_name`, `form_url`) VALUES ('".$data['emp_id']."','".$data['extra_form_name']."','".$extra_form."')") or die(mysqli_error($link));
					
			}
		
	}
}
?>