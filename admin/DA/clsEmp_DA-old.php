<?php
class clsEmp_DA
{
	function Emp_Select()
	{
		global $link;
		$sql_select_all = "SELECT em_id, sh_master_id, em_family_name, em_other_name, em_address, em_postcode, em_phone_no, em_mobile_no,employee_status FROM employee_master";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}

	function Emp_Detail($id)
	{
		global $link;
		$sql_detail = "SELECT em_id, sh_master_id, em_family_name, em_other_name, em_address, em_postcode, em_phone_no, em_mobile_no FROM employee_master WHERE em_id='".$id."'";
		$res_detail = mysqli_query($link,$sql_detail) or die(mysqli_error($link));
		$row_detail = mysqli_fetch_array($res_detail);
		if($row_detail)
		{
			return $row_detail;
		}
		else
		{
			return null;
		}
	}

	function Emp_Create($data)
	{
		global $link;
		$sql_create = "INSERT INTO employee_master(em_id, sh_master_id, em_family_name, em_other_name, em_address, em_postcode, em_phone_no, em_mobile_no, em_entitled_austrailia, em_identity_doc, em_current_study_status, em_study_type, em_institute_type, em_hs_name, em_hs_grade, em_hs_year, em_ps_commenced, em_ps_year, em_qualification, em_signature, em_signature_date, created_date, updated_date) VALUES ('','','".$data['f_name']."','".$data['o_name']."','".$data['address']."','".$data['postcode']."','".$data['ph_no']."','".$data['mo_no']."','".null."','".null."','".null."', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."') ";

		$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
		if($res_create)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function Emp_Edit($update_data)
	{
		global $link;
		$sql_update = "UPDATE employee_master SET em_family_name='".$update_data['f_name']."', em_other_name='".$update_data['o_name']."', em_address='".$update_data['address']."', em_postcode='".$update_data['postcode']."', em_phone_no='".$update_data['ph_no']."', em_mobile_no='".$update_data['mo_no']."', updated_date='".date("Y-m-d H:i:s")."' WHERE em_id='".$update_data['id']."'  ";

		$res_edit = mysqli_query($link,$sql_update) or die(mysqli_error($link));

		if($res_edit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	############################## Delete Record ###############################
	
	function Emp_Delete($oEmp_CDO)
	{
		global $link;
		$sqlDel = "delete from employee_master where em_id = '".$oEmp_CDO->id."'";
		$resDel = execute_query($sqlDel,$link) or die(mysqli_error($link));
	}
	
	############################ End Delete Record ##############################
	
	function add_employment($detail)
	{
		global $link;
		
		
		$check = "SELECT * from employment_detail where email =  '".$detail['email']."'";
		
		$check_result = mysqli_query($link,$check) or die(mysqli_error($link));
		
		$norow = mysqli_num_rows($check_result);
		
	
		
		if(!empty($norow))
		{
				
			
		}else
		{
				$str_id = $_SESSION['varStoreSel']; 
		$sql_create = "INSERT INTO employment_detail( email, message, form_link, store_id, sent_date) VALUES ( '".$detail['email']."', '".$detail['msg']."', '".$detail['link']."','".$str_id."', '".date("Y-m-d H:i:s")."')";

		$sql_token = " INSERT INTO employment_token (token) VALUES ('".$detail['token']."') ";

		$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
		$res_token = mysqli_query($link,$sql_token) or die(mysqli_error($link));
		
		}
		
		$body="<table width='50%' align='center'   style='border-style:dotted;' bordercolor='#CCC' >
					  <tr>
						<td width='7%' height='50px' align='left'>From </td>
						<td width='86%'>:: OGFOODS</td>
						<td width='7%'></td>
					  </tr>
					  <tr>
						<td align='left'>Subject </td>
						<td>:: Employment Form</td>
						<td width='7%'></td>
					  </tr>
					  <tr>
					  	<td colspan='3'  align='center'>Employment Form Details Mail</td>
					  </tr>
					  <tr>
						<td align='right' height='150px'></td>
						<td rowspan='2' align='justify'><br>Hello ".$_SESSION['varUserName'].",<br><br>".$detail['msg']."<br><br>Please<a href='".$detail['link']."' style='text-decoration: blink;'> Click  Here </a>To Complete Employment Form</td>
						<td></td>
					  </tr>
					  <tr>
					  	<td></td>
					  	<td ></td>
					  </tr>
					  <tr>
						<td>&nbsp;</td>
						<td align='left'><br>Thank you,<br>ogfoods<br><br></td>
						<td>&nbsp;</td>
					  </tr>
					   <tr>
						<td colspan='2' align='center'>&nbsp;</td>
					  </tr>
					</table>
				";

			/*$msg = wordwrap($body,70);

			// send email
			
			$mail = mail($detail['email'],"Employment Form",$msg);*/
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: 'OGFOODS' <'OGFOODS'>\r\n";
			mail($detail['email'],"Employment Form",$body,$headers);
			
			return 1;
		
	}
	
	function Employee_Master($id)
	{
		global $link;
		$sql_detail1 = "SELECT * FROM employee_master WHERE em_id='".$id."'";
		$res_detail1 = mysqli_query($link,$sql_detail1) or die(mysqli_error($link));
		$emp_mas = mysqli_fetch_array($res_detail1);
		
		return $emp_mas;
	}
	function Employee_Applyfor($id)
	{
		global $link;
		$sql_detail2 = "SELECT * FROM employee_applied_for WHERE em_id='".$id."'";
		$res_detail2 = mysqli_query($link,$sql_detail2) or die(mysqli_error($link));
		$emp_app = mysqli_fetch_array($res_detail2);
	
		return $emp_app;
	}
	function Employee_Info($id)
	{	
		global $link;
		$sql_detail3 = "SELECT * FROM employee_general_info WHERE em_id='".$id."'";
		$res_detail3 = mysqli_query($link,$sql_detail3) or die(mysqli_error($link));
		$emp_gen = mysqli_fetch_array($res_detail3);
		
		return $emp_gen;
	}
	function Employee_History($id)
	{	
		global $link;
		$sql_detail = "SELECT * FROM employee_work_history WHERE em_id='".$id."'";
		$res_detail = mysqli_query($link,$sql_detail) or die(mysqli_error($link));
		$emp_his = mysqli_fetch_array($res_detail);
	
		return $emp_his;
		
	}
	function Emp_Status($oEmp_CDO)
	{
			global $link;
			
			$sql_updat = "UPDATE employee_master SET employee_status='".$oEmp_CDO->us."' where em_id = '".$oEmp_CDO->id."'";
			
			$res_d = mysqli_query($link,$sql_updat) or die(mysqli_error($link));
			
			
	}
}
?>