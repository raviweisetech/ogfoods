<?php

require_once("include/clsInclude.php");

		$now = new DateTime('00:00');
		$end = clone $now;
		$end->modify("+24 hours");

		while ($now < $end) {
			$_response[]=$now->format('g:i a');
			$now->modify('+15 minutes');
		}
		if(empty($_response)){ $_response = 0; }
echo json_encode($_response);
?>