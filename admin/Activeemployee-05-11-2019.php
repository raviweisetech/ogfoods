<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

/*Delete user Data start*/
if(fnRequestParam('mode') == 'Delete') {
	$oEmp_CDO->id = fnRequestParam('id');
	$oEmp_DA->Emp_Delete($oEmp_CDO);
	header("Location:Activeemployee.php?msg=Del");
	exit;	
}
if(fnRequestParam('mode') == 'es') {
	$oEmp_CDO->id = fnRequestParam('eid');
	$oEmp_CDO->us = fnRequestParam('ems');
	$oEmp_DA->Emp_Status($oEmp_CDO);
	header("Location:Activeemployee.php?id=$oEmp_CDO->id");
	exit;
}
if(fnRequestParam('mode') == 'res') {
	$oEmp_CDO->id = fnRequestParam('eid');
	$oEmp_CDO->resign_date = fnRequestParam('resign_date');
	$oEmp_CDO->us = fnRequestParam('ems');
	$oEmp_DA->Emp_resign_Status($oEmp_CDO);
	header("Location:Activeemployee.php?id=$oEmp_CDO->id");
	exit;
}
if(fnRequestParam('mode') == 'sst') {
	
	$_SESSION['varStoreSel'] = fnRequestParam('eid');
	header("Location:Profile.php");
}
include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Active Employees </h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
		<a href="Employeement.php" style="margin-right: 10px"><button class="btn-primary" >New employment</button></a>
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employees</h3>          
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">Name</th>
					<th align="left" class="tbl_design1">First Name</th>
					<th align="left" class="tbl_design1">Address</th>
                    <th align="left" class="tbl_design1">Postcode</th>
					<th align="left" class="tbl_design1">Phon No.</th>
					<th align="left" class="tbl_design1">Mo.No.</th>
                    <th align="left" class="tbl_design1">Status</th>
                    <th align="left" class="tbl_design1">Resign Status</th>
                    <th align="left" class="tbl_design1">Upload Extra Forms</th>
					<th align="left" class="tbl_design1">View Full Detail</th>
					<th align="left" class="tbl_design1">Delete</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                	//print_r(mysqli_fetch_assoc($oEmp_DA->Emp_Select()));die;
                	$result = $oEmp_DA->Emp_Select();
                	while($row = mysqli_fetch_assoc($result))
                	{ 
						if($row['employee_status']== "1")
						{
					?>
                		<tr>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_family_name'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_first_name'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_address'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_postcode'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_phone_no'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_mobile_no']; ?></td>
                        <td class="tbl_data_dtl" align="left">
                        <select name="status" onchange= "return change_status(<?php echo $row['em_id'];?>,this.value)">
                        	<option value="0" <?php if($row['employee_status']== "0"){ echo "selected";} ?>>Pending</option>
                        	<option value="1"<?php if($row['employee_status']== "1") { echo "selected";} ?>>Approve</option>
                            <option value="2"<?php if($row['employee_status']== "2") { echo "selected";} ?>>Decline</option>
                        </select>
                        </td>
                        <td class="tbl_data_dtl" align="left">
                        <input type="text" name="resign_date" class="resign_date" id="<?php echo $row['em_id']; ?>" value="<?php if($row['employee_resign_date'] != ''){ echo $row['employee_resign_date'];} ?>" onchange="return datechange(this);" />
                        </td>
                         <td class="tbl_data_dtl" align="center"><a href="uploadExtraForm.php?id=<?php echo $row['em_id'];?>" class="mylink">Click here</a></td>
					    <td class="tbl_data_dtl" align="center"><a href="Employeedetails.php?id=<?php echo $row['em_id'];?>" class="mylink">View Full Detail</a></td>
					    <td class="tbl_data_dtl" align="center"><a href="Activeemployee.php?id=<?php echo $row['em_id'];?>&amp;mode=Delete" class="mylink" onClick="if (! confirm('Are you sure want to delete this employee?')) { return false; }" >Delete</a></td>
						</tr>
             <?php  }} ?>
         		</tbody>
         	</table>
         </div>
     </div>
 </div>
</div>
</section>
<script language="javascript">
var i = 1;
function change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=es&eid="+eid+"&ems="+empstatus+"";
}
function resign_change_status(eid,empstatus)
{
		
		window.location.href="Activeemployee.php?mode=res&eid="+eid+"&ems="+empstatus+"";
}
function datechange(datas)
{
	if(i == 1)
	{
		var del=confirm("Are you sure want to resign this employee?");
		if (del==true){
			window.location.href="Activeemployee.php?mode=res&eid="+datas.id+"&ems=2&resign_date="+datas.value;
		 //  alert ("Activeemployee.php?mode=res&eid="+datas.id+"&ems=2&resign_date="+datas.value)
		}else{
			//break;
		}
	}else if(i == 3)
	{
		i = 0;	
	}
	i++;
}
</script>
<?php include('footer.php'); ?>
<?php ob_flush();?>