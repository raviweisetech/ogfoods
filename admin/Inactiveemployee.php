<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
$oEmp_CDO = new clsEmp_CDO();

include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Inactive Employees </h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
		<a href="Employeement.php" style="margin-right: 10px"><button class="btn-primary" >New employment</button></a>
<?php /*?>		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
<?php */?>	</section>
</div>
<br><br>
<section class="content">
  	<div class="row">
	
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employees</h3>          
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">Name</th>
					<th align="left" class="tbl_design1">First Name</th>
					<th align="left" class="tbl_design1">Address</th>
                    <th align="left" class="tbl_design1">Postcode</th>
					<th align="left" class="tbl_design1">Phon No.</th>
					<th align="left" class="tbl_design1">Mo.No.</th>
					<th align="left" class="tbl_design1">View Full Detail</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                	//print_r(mysqli_fetch_assoc($oEmp_DA->Emp_Select()));die;
                	$result = $oEmp_DA->Emp_Select_delete();
                	while($row = mysqli_fetch_assoc($result))
                	{ 
						
					?>
                		<tr>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_family_name'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_first_name'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_address'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_postcode'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_phone_no'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_mobile_no'];?></td>
                      
					    <td class="tbl_data_dtl" align="center"><a href="Employeedetails.php?id=<?php echo $row['em_id'];?>" class="mylink">View Full Detail</a></td>
					  
						</tr>
             <?php  } ?>
         		</tbody>
         	</table>
         </div>
     </div>
 </div>
</div>
</section>

<?php include('footer.php'); ?>
<?php ob_flush();?>