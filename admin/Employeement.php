<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
//$token = rand(99999,1000000);
$token = base64_encode(uniqid());
if(isset($_POST['submit']))
{
	$resp = $oEmp_DA->add_employment($_POST);
	if($resp == 1)
	{
			$msg = "<label class='alert-info' style='width: 100%;height: 35px;text-align: -webkit-center;'>Email Send Succesfully...</label>" 	;
	}else
	{	
		$erro = 'Email Already Exist';
		if($resp == 0)
		{	
			$erro = 'Store is Not Selected';
		}
			$msg = "<label class='alert-error' style='width: 100%;height: 35px;text-align: -webkit-center;'>Fail To Send Email...Due to '".$erro."'</label>" 	;
	}
	
}

?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Employment Form Detail </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br><?php echo $msg; ?>
               <form role="form" method="POST">
		            <div class="box-body">
                    <div class="form-group">
                        
	    		          <label>Current Store :: </label><?php 
						  	$oStore_DA = new clsStore_DA();
							 $result = $oStore_DA->Select_Store($_SESSION['varStoreSel']);
	            			 $rows = mysqli_fetch_assoc($result);
							 echo " ".$rows['st_name'];
							 
						   ?>
			              <input type="hidden" name="hidden_st_name" id="hidden_st_name" value="<?php echo $rows['st_name']; ?>">
	            		</div>
                        <div class="form-group">
	    		          <label>Full Name:</label>
			              <input name="fullname" type="text" id="fullname" class="input form-control" tabindex="1" required>
	            		</div>
		            	<div class="form-group">
                        
	    		          <label>Email:</label>
			              <input name="email" type="email" id="email" class="input form-control" tabindex="2" required>
	            		</div>
		            	<div class="form-group">
	    		          <label>Message:</label>
			              <textarea name="msg" id="msg" class="input form-control" rows="3" tabindex="3" required></textarea>
	            		</div>
		            	<div class="form-group">
	    		          <label>Employment Form Link:</label>
			              <input name="link" type="text" id="link" class="input form-control"
			              value=<?php echo "http://smartdatakeep.com/admin/Employeement_val.php?id=".$token; ?> tabindex="3" readonly>
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Send link to Employee">
	            		</div>
	            		<input type="hidden" name="token" id="token" value="<?php echo $token; ?>">
                        <input type="hidden" name="st_id" id="st_id" value="<?php echo $_SESSION['varStoreSel']; ?>">
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<?php include('footer.php'); ?>
<?php ob_flush();?>
?>