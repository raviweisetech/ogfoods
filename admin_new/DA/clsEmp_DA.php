<?php
/**
* 
*/
class clsEmp_DA
{
	function Emp_Select()
	{
		global $link;
		$sql_select_all = "SELECT em_id, sh_master_id, em_family_name, em_other_name, em_address, em_postcode, em_phone_no, em_mobile_no FROM employee_master";
		$res = execute_query($sql_select_all,$link) or die(mysqli_error($link));
		return $res;
	}

	function Emp_Detail($id)
	{
		global $link;
		$sql_detail = "SELECT em_id, sh_master_id, em_family_name, em_other_name, em_address, em_postcode, em_phone_no, em_mobile_no FROM employee_master WHERE em_id='".$id."'";
		$res_detail = mysqli_query($link,$sql_detail) or die(mysqli_error($link));
		$row_detail = mysqli_fetch_array($res_detail);
		if($row_detail)
		{
			return $row_detail;
		}
		else
		{
			return null;
		}
	}

	function Emp_Create($data)
	{
		global $link;
		$sql_create = "INSERT INTO employee_master(em_id, sh_master_id, em_family_name, em_other_name, em_address, em_postcode, em_phone_no, em_mobile_no, em_entitled_austrailia, em_identity_doc, em_current_study_status, em_study_type, em_institute_type, em_hs_name, em_hs_grade, em_hs_year, em_ps_commenced, em_ps_year, em_qualification, em_signature, em_signature_date, created_date, updated_date) VALUES ('','','".$data['f_name']."','".$data['o_name']."','".$data['address']."','".$data['postcode']."','".$data['ph_no']."','".$data['mo_no']."','".null."','".null."','".null."', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."') ";

		$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
		if($res_create)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function Emp_Edit($update_data)
	{
		global $link;
		$sql_update = "UPDATE employee_master SET em_family_name='".$update_data['f_name']."', em_other_name='".$update_data['o_name']."', em_address='".$update_data['address']."', em_postcode='".$update_data['postcode']."', em_phone_no='".$update_data['ph_no']."', em_mobile_no='".$update_data['mo_no']."', updated_date='".date("Y-m-d H:i:s")."' WHERE em_id='".$update_data['id']."'  ";

		$res_edit = mysqli_query($link,$sql_update) or die(mysqli_error($link));

		if($res_edit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>