<?php
class clsUser_DA
{
	############################## Select  Record ##############################
	function User_Select()
	{
		global $link;

		//$link= mysqli_connect('127.0.0.1', 'root', '','ogfoods') or  die("Could not connect: " . mysqli_error($link));
//	$pg = new PaggingClass();  
		// count the number of record for that query just give the query after from clause
		$count_query="tbl_user";
		// set the number of record to display
		//$recordPerPage=1000;
		// get the class name
		$className = "LinkFont";
		// return the pagging...
//	$this->returned = $pg->pagging($recordPerPage,$count_query,$className);						
		//$sql = "SELECT * from   tbl_Users  where Category_ID=".$_REQUEST['C_ID']." order by n_order Asc "; 
		$sql = "SELECT * from tbl_user order by id desc"; 
		$res = execute_query($sql,$link) or die(mysqli_error($link));
		$i=0;
	
		while($row=mysqli_fetch_array($res))
		{	
			$this->id[$i] = $row['id'];
			$this->first_name[$i] = $row['first_name'];
			$this->last_name[$i] = $row['last_name'];
			$this->email_address[$i] = $row['email_address'];
			$this->user_status[$i] = $row['user_status'];
			$i++;
		}
		$this->totalRows=$i;

		return $this;
	}

        ############################## User status ###############################
	function User_Status($oUser_CDO)
	{	
		  global $link;
		  $sql_edit = "UPDATE `tbl_user` SET `user_status` = '".$oUser_CDO->us."' WHERE `id` ='".$oUser_CDO->id."'";							   
		  $res_select=mysqli_query($link,$sql_edit) or die(mysqli_error($link));
	}
	############################ End User status ###############################
	############################## Delete Record ###############################
	function User_Delete($oUser_CDO)
	{
		global $link;
		$sqlDel = "delete from tbl_user where id = '".$oUser_CDO->id."'";
		$resDel = execute_query($sqlDel,$link) or die(mysqli_error($link));
	}
	############################ End Delete Record ##############################
	function User_Detail($id)
	{
		global $link;
		$sql_detail = "select * from tbl_user where id = '".$id."'";
		$res_detail = mysqli_query($link,$sql_detail) or die(mysqli_error($link));
		$row_detail = mysqli_fetch_array($res_detail);
		if($row_detail)
		{
			return $row_detail;
		}
		else
		{
			return null;
		}
	}

	function User_Create($data)
	{
		global $link;
		$sql_create = "INSERT INTO `tbl_user`(`id`, `first_name`, `last_name`, `email_address`, `password`, `user_type`, `user_status`, `create_date`, `modified_date`) VALUES ('','".$data['f_name']."','".$data['l_name']."','".$data['email']."','".$data['password']."','".$data['u_type']."','".$data['u_status']."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";

		$res_create = mysqli_query($link,$sql_create) or die(mysqli_error($link));
		if($res_create)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function User_Edit($update_data)
	{
		global $link;
		$sql_update = "UPDATE tbl_user SET first_name='".$update_data['f_name']."',last_name='".$update_data['l_name']."',email_address='".$update_data['email']."',password='".$update_data['password']."',user_type='".$update_data['u_type']."',user_status='".$update_data['u_status']."',modified_date='".date("Y-m-d H:i:s")."' WHERE id='".$update_data['id']."' ";

		$res_edit = mysqli_query($link,$sql_update) or die(mysqli_error($link));

		if($res_edit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>