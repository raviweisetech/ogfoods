<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oUser_CDO = new clsUser_CDO();
$oUser_DA = new clsUser_DA();
					//	print_r($oUser_DA->User_Select());exit();

//Edit user start
if(fnRequestParam('mode') == 'us') {
	$oUser_CDO->id = fnRequestParam('uid');
	$oUser_CDO->us = fnRequestParam('us');
	$oUser_DA->User_Status($oUser_CDO);
	header("Location:User.php?msg=us");
	exit;
}
/*Edit user End*/

/*Delete user Data start*/
if(fnRequestParam('mode') == 'Delete') {
	$oUser_CDO->id = fnRequestParam('id');
	$oUser_DA->User_Delete($oUser_CDO);
	header("Location:User.php?msg=Del");
	exit;	
}

?>

<?php include('header.php');?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Manage Users </h1> </section>
<section class="col-md-6 center" align="right" style="margin-top: 10px"><a href="UserCreate.php"><button class="btn-primary" >Create User</button></a></section>
</div>
<br><br>
<section class="content">
	<form name="frmManageUser" method="post" enctype="multipart/form-data">
  	<div class="row">
<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users</h3>
              
            </div>

            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">User Name</th>
					<th align="left" class="tbl_design1">Email ID</th>
					<th align="left" class="tbl_design1">Status</th>
                    <th align="left" class="tbl_design1">View User Detail</th>
					<th align="left" class="tbl_design1">Edit</th>
					<th align="left" class="tbl_design1">Delete</th>
                  </tr>
                </thead>
                <tbody>
	            <?php
					if(isset($_REQUEST['search_submit'])) {
						$oUser_CDO->Username = trim(fnRequestParam('txtName'));
						$oUser_CDO->Userstatus = trim(fnRequestParam('searchstatus'));
						$oUser_CDO->Employeestatus = trim(fnRequestParam('search_employe'));
						$oUser_DA->User_Search($oUser_CDO);
		  			}
					else {
						$oUser_DA->User_Select();
			    	}

					for($i=0; $i<$oUser_DA->totalRows; $i++) {
						$id = $oUser_DA->id[$i];
					?>
					<tr>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oUser_DA->first_name[$i].' '.$oUser_DA->last_name[$i];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $oUser_DA->email_address[$i];?></td>
					    <td class="tbl_data_dtl" align="center">&nbsp;
						<select name="changestatus" onchange="return change_status(<?php echo $id?>,this.value)">
						   <option value="0" <?php if($oUser_DA->user_status[$i] =='0') echo "selected='selected'";?>>Active</option>
						   <option value="1" <?php if($oUser_DA->user_status[$i] =='1') echo "selected='selected'";?>>Deactive</option>
						</select>
					    </td>
					    <td class="tbl_data_dtl" align="center"><a href="UserDetail.php?id=<?php echo $id;?>" class="mylink">View User Detail</a></td>
					    <td class="tbl_data_dtl" align="center"><a href="UserEdit.php?id=<?php echo $id;?>" class="mylink">Edit</a></td>
					    <td class="tbl_data_dtl" align="center"><a href="User.php?id=<?php echo $id;?>&amp;mode=Delete" class="mylink">Delete</a></td>
					</tr>
				<?php } ?>
            	</tbody>
              </table>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>

<script language="javascript">
function trim(stringToTrim)
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function change_status(uid,userstatus)
{
	window.location.href="User.php?mode=us&uid="+uid+"&us="+userstatus+"";
}

function check(mode)
{
	if(trim(document.getElementById("txtfname").value)=='')
	{
		alert("Please Enter Your First Name");
		document.getElementById("txtfname").focus();
		return false;
	}
	
	if(trim(document.getElementById("txtlname").value)=='')
	{
		alert("Please Enter Your Last Name");
		document.getElementById("txtlname").focus();
		return false;
	}
	
	if(trim(document.getElementById("txtemail").value)=='')
	{
		alert("Please Enter Your Email Address");
		document.getElementById("txtemail").focus();
		return false;
	}
	
	if(!emailcheck(document.getElementById("txtemail").value))
    {
		alert('Please Enter Valid Email Address.');
		document.getElementById('txtemail').focus();		
		return false;		 
    }
}

function emailcheck(str)
{
	emailF = false;
	if(str!="")
		{
			var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			var ans = str.match(emailExp);
			if(ans == str)
			{
				emailF = true;
			}
		}	
	return emailF;
}
</script>		
<?php include('footer.php'); ?>
<?php ob_flush();?>