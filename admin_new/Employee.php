<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();
include('header.php');

?>
<div class="col-md-12">
	<section class="content-header col-md-6"> 
		<h1> Manage Employees </h1> 
	</section>
	<section class="col-md-6 center" align="right" style="margin-top: 10px">
		<a href="EmployeeCreate.php"><button class="btn-primary" >Create Employee</button></a>
	</section>
</div>
<br><br>
<section class="content">
  	<div class="row">
	<?php
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'already') {
			$message = "<div class='msg1 alert alert-info alert-dismissible'>Record Already Exist</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'succ') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Inserted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Edit') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Updated Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'Del') {
			$message = "<div class='msg alert alert-info alert-dismissible'>Record Deleted Successfully</div>";
		}
		if(isset($_REQUEST['msg']) && trim($_REQUEST['msg']) == 'us') {
			$message = "<div class='msg alert alert-info alert-dismissible'>User status changed successfully.</div>";
		}
	?>
	<div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Employees</h3>          
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              	<thead>
                  <tr>
                    <th align="left" class="tbl_design1">Name</th>
					<th align="left" class="tbl_design1">Other Name</th>
					<th align="left" class="tbl_design1">Address</th>
                    <th align="left" class="tbl_design1">Postcode</th>
					<th align="left" class="tbl_design1">Phon No.</th>
					<th align="left" class="tbl_design1">Mo.No.</th>
					<th align="left" class="tbl_design1">Edit</th>
					<th align="left" class="tbl_design1">Delete</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                	//print_r(mysqli_fetch_assoc($oEmp_DA->Emp_Select()));die;
                	$result = $oEmp_DA->Emp_Select();
                	while($row = mysqli_fetch_assoc($result))
                	{ ?>
                		<tr>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_family_name'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_other_name'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_address'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_postcode'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_phone_no'];?></td>
					    <td class="tbl_data_dtl" align="left">&nbsp;<?php echo $row['em_mobile_no'];?></td>
					    <td class="tbl_data_dtl" align="center"><a href="EmployeeEdit.php?id=<?php echo $row['em_id'];?>" class="mylink">Edit</a></td>
					    <td class="tbl_data_dtl" align="center"><a href="Employee.php?id=<?php echo $id;?>&amp;mode=Delete" class="mylink">Delete</a></td>
						</tr>
             <?php  } ?>
         		</tbody>
         	</table>
         </div>
     </div>
 </div>
</div>
</section>
<?php include('footer.php'); ?>
<?php ob_flush();?>