<?php
include_once("../common/global.inc.php");
include_once("../common/common.php");
include_once("../common/clspagging.php");
include_once("../common/sendmail_smtp.php");

include_once("CDO/clsLogin_CDO.php");
include_once("DA/clsLogin_DA.php");

include_once("CDO/clsAdmin_CDO.php");
include_once("DA/clsAdmin_DA.php");

include_once("CDO/clsUser_CDO.php");
include_once("DA/clsUser_DA.php");

include_once("CDO/clsCar_CDO.php");
include_once("DA/clsCar_DA.php");

include_once("CDO/clsContest_CDO.php");
include_once("DA/clsContest_DA.php");

include_once("CDO/clsMedia_CDO.php");
include_once("DA/clsMedia_DA.php");

include_once("CDO/clsComment_CDO.php");
include_once("DA/clsComment_DA.php");

include_once("CDO/clsTournament_CDO.php");
include_once("DA/clsTournament_DA.php");

include_once("DA/clsEmp_DA.php");
?>