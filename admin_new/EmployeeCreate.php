<?php
ob_start();
session_start();

if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}

require_once("include/clsInclude.php");
$oEmp_DA = new clsEmp_DA();


if(isset($_POST['submit']))
{
	$emp_data = $oEmp_DA->Emp_Create($_POST);

	if($emp_data)
	{
		header('Location: Employee.php');	
	}
	else
	{
		echo "Your data can't Updated";
	}
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Create Employee Detail </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	<div class="form-group">
	    		          <label>Family Name:</label>
			              <input name="f_name" type="text" id="f_name" class="input form-control" tabindex="1" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>Other Name:</label>
			              <input name="o_name" type="text" id="o_name" class="input form-control" tabindex="2" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>Address:</label>
			              <textarea name="address" id="address" class="input form-control" rows="3" tabindex="3" required="true"></textarea>
	            		</div>
		            	<div class="form-group">
	    		          <label>Postcode:</label>
			              <input name="postcode" type="Number" id="postcode" class="input form-control" max="99999999" tabindex="4" required="true">
	            		</div>
	            		<div class="form-group">
	    		          <label>Phone Number:</label>
			              <input name="ph_no" type="Number" id="ph_no" class="input form-control" min="999999" max="9999999999" tabindex="4" >
	            		</div>
	            		<div class="form-group">
	    		          <label>Mobile Number:</label>
			              <input name="mo_no" type="text" id="mo_no" class="input form-control"  min="999999" max="9999999999" tabindex="4" required="true">
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Create User">
	            		</div>
	            		<input type="hidden" name="id" id="id" value="<?php echo $emp_detail['em_id'] ?>">
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<?php include('footer.php'); ?>
<?php ob_flush();?>
?>