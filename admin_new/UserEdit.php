<?php
ob_start();
session_start();
//ini_set("display_errors",1);
//error_reporting(2);
if(!isset($_SESSION['varUserName'])) {
	header('Location:Login.php');
}
//$_SESSION['ref'] = $_SERVER['PHP_SELF']."?C_ID=".$_REQUEST['C_ID'];
require_once("include/clsInclude.php");
$oUser_CDO = new clsUser_CDO();
$oUser_DA = new clsUser_DA();
					//	print_r($oUser_DA->User_Select());exit();
if(isset($_POST['submit']))
{
	$user_data = $oUser_DA->User_Edit($_POST);

	if($user_data)
	{
		header('Location: User.php');	
	}
	else
	{
		echo "Your data can't Updated";
	}
}
else
{
	if($_GET['id'])
	{
		$user_detail = $oUser_DA->User_Detail($_GET['id']);
	}
}
?>

<?php include('header.php'); ?>
<div class="col-md-12">
<section class="content-header col-md-6"> <h1> Update User Detail </h1> </section>
</div>
<br><br>
<section class="content">
  	<div class="row">		
		<div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
               <form role="form" method="POST">
		            <div class="box-body">
		            	<div class="form-group">
	    		          <label>First Name:</label>
			              <input name="f_name" type="text" id="f_name" class="input form-control" tabindex="1" value="<?php echo $user_detail['first_name'] ?>" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>Last Name:</label>
			              <input name="l_name" type="text" id="l_name" class="input form-control" tabindex="2" value="<?php echo $user_detail['last_name'] ?>" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>E-mail:</label>
			              <input name="email" type="email" id="email" class="input form-control" tabindex="3" value="<?php echo $user_detail['email_address'] ?>" required="true">
	            		</div>
		            	<div class="form-group">
	    		          <label>Password:</label>
			              <input name="password" type="text" id="password" class="input form-control" value="<?php echo $user_detail['password'] ?>" tabindex="4" required="true">
	            		</div>
		            	<div class="input form-group">
	    		          <label>User Type:</label>
	    		          <select class="form-control" name="u_type" id="u_type" tabindex="5" required="true">
	    		          	 
	    		          	<option value="0" <?php if($user_detail['user_type'] == '0') {?> selected="true" <?php } ?> >0</option>
	    		          	<option value="1"  <?php if($user_detail['user_type'] == '1') {?> selected="true" <?php } ?> >1</option>
	    		          	<option value="2"  <?php if($user_detail['user_type'] == '2') {?> selected="true" <?php } ?>>2</option>
	    		          	<option value="3"  <?php if($user_detail['user_type'] == '3') {?> selected="true" <?php } ?>>3</option>
	    		          </select>
	            		</div>
	            		<div class="form-group">
	    		          <label>User Status:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="u_status" value="0"  <?php if($user_detail['user_status'] == '0') {?> checked <?php } ?> id="u_status"> Active
	    		          	&nbsp;&nbsp;&nbsp;
	    		          	<input type="radio" name="u_status" value="1"  <?php if($user_detail['user_status'] == '1') {?> checked <?php } ?> id="u_status"> Deactive
	            		</div>
	            		<div class="form-group" align="center">
	            			<input type="submit" name="submit" id="submit" value="Update User">
	            		</div>
	            		<input type="hidden" name="id" id="id" value="<?php echo $user_detail['id'] ?>">
		            </div>
		        </form>
            </div>
          </div>
        </div>
  </div>
  </form>
</section>		
<?php include('footer.php'); ?>
<?php ob_flush();?>