<?php
	//Required Files
	require_once '../includes/config.php';
	require_once '../includes/util.php';
	require_once '../includes/class.database.php';
	require_once '../includes/class.model.php';
	
	//Page Formation
	include '../header.php';
	include 'scripts/user_data.php';

	// users' listing 
	?>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="users_list">
		<tr>
			<th>Company Name</th>
			<th>Industry</th>
			<th>Website URL</th>
			<th>Email Address</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Action</th>
		</tr>

		<?php foreach($users as $user){ ?>
			<tr>
				<td><?php echo $user['company_name'] ; ?></td>
				<td><?php echo $user['industry'] ; ?></td>
				<td><?php echo $user['website_url'] ; ?></td>
				<td><?php echo $user['email_address'] ; ?></td>
				<td><?php echo $user['first_name'] ; ?></td>
				<td><?php echo $user['last_name'] ; ?></td>
				<td></td>
			</tr>
		<?php } ?>
	</table>
	<?php
	include '../footer.php';
?>

<script>
$(document).ready(function() {
	$('#users_list').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "scripts/user_data.php"
    });
});
</script>
