<?php
require_once("include/clsInclude.php");
if(basename($_SERVER['PHP_SELF']) != "User.php")
{
$oUser_CDO = new clsUser_CDO();
$oUser_DA = new clsUser_DA();
}
$getcount = explode("==",$oUser_DA->GetUserClientcount());

?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <!-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
        </div>
        <!-- <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div> -->
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Admin Management</li>
        <li>
          <a href="Admin.php">
            <i class="fa fa-th"></i> <span>Profile</span>
          </a>
        </li>
		<?php if($_SESSION['U_Type'] == '0'){ ?>

        <li>
          <a href="User.php">
            <i class="fa fa-users"></i> <span>Manage Users</span>
          </a>
        </li>
        <li>
          <a href="Clients.php">
            <i class="fa fa-th"></i> <span>Manage Clients</span>
          </a>
        </li>
        <li>
          <a href="Company.php">
            <i class="fa fa-th"></i> <span>Company User</span>
          </a>
        </li>
		<?php }elseif($_SESSION['U_Type'] == '1'){?>
			<li>
          <a href="Employee.php">
            <i class="fa fa-th"></i> <span>Manage Employee<?php echo $getcount[3];?></span>
          </a>
        </li>
		<?php }else{
				 
				}
				?>
        <li>
          <a href="Logout.php">
            <i class="fa fa-power-off"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
					<link rel="stylesheet" type="text/css" media="all" href="../calendar/skins/aqua/theme.css" title="win2k-cold-1" />
                    <script type="text/javascript" src="../calendar/calendar.js"></script>
                    <!-- language for the calendar -->
                    <script type="text/javascript" src="../calendar/lang/calendar-en.js"></script>
                    <!-- the following script defines the Calendar.setup helper function, which makes
      				 adding a calendar a matter of 1 or 2 lines of code. -->
                    <script type="text/javascript" src="../calendar/calendar-setup.js"></script>
                    <link type="text/css" rel="stylesheet" href="../css/admin_style.css" />
               