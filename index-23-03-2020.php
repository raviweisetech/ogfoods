<?php
require_once 'common/global.inc.php';
require_once 'common/common.php';

if($_REQUEST['token'] != '')
{
	$employee_sql = mysqli_query($link,"SELECT * from `employee_master` where `form_access_token` = '".$_REQUEST['token']."'");
	$employee_rows = mysqli_fetch_array($employee_sql);
	$email_address = $employee_rows['em_email_address'];
}
if(isset($_POST['submit']) && isset($_POST['terms']) )
{
	$isupdate = 0;
	$token = rand(99999,1000000);
	$store_id = 0;
	if($_REQUEST['token'] != '')
	{
		$sql_del = mysqli_query($link,"DELETE FROM employment_token WHERE token = '".$_REQUEST['token']."'");	
		$store_sql = mysqli_query($link,"SELECT * from `employment_detail` where `form_access_token` = '".$_REQUEST['token']."'");
		$stor = mysqli_fetch_array($store_sql);
		$store_id = $stor['store_id'];
		
		
		$employee_sql = mysqli_query($link,"SELECT * from `employee_master` where `form_access_token` = '".$_REQUEST['token']."'");
		$employee_rows = mysqli_fetch_array($employee_sql);
		$foreign_key = $employee_rows['em_id'];
		$email_address = $employee_rows['em_email_address'];
		$isupdate = 1;
	}else{
		$employee_sql = mysqli_query($link,"SELECT * from `employee_master` where `em_email_address` = '".$_POST['em_email_address']."' ");
		$empl_count= mysqli_num_rows($employee_sql);
		if($empl_count > 0)
		{
			$employee_rows = mysqli_fetch_array($employee_sql);
			if($employee_rows['employee_status'] == '3')
			{
				$isupdate = 1;
				$foreign_key = $employee_rows['em_id'];
			}else{
				echo "EMAIL ALREADY EXIST";exit;
			}

		}
	}

//PERSONAL DETAILS AND EDUCATION/TRAINING
	$cfn = $_FILES['em_con_agreement']['name'];
	$acnwdfn = $_FILES['em_crewm_Ack']['name'];
	$cpolicyfn = $_FILES['em_cashdrawer_policy']['name'];
	$ltroffn = $_FILES['em_cre_letteroff']['name'];
	$fworkfn = $_FILES['em_fair_workstat']['name'];
	$wformfn = $_FILES['em_whs_form']['name'];
	$wresfn = $_FILES['em_whs_responsibility']['name'];
	$esupchoform = $_FILES['em_superanu_choice_form']['name'];
	$unidec = $_FILES['em_uniform_deducation']['name'];
	
	$unipolic = $_FILES['em_uniform_policy']['name'];
	$tfndecform = $_FILES['em_tfn_dec__form']['name'];
	
	if($unipolic != "")
	{
		$path="admin/upload/Uniform_policy/";
		$npath=$path.$token.$unipolic;
		move_uploaded_file($_FILES['em_uniform_policy']['tmp_name'],$npath);
		$emp_uni_policy_form = $npath;
	}else
	{
		$emp_uni_policy_form = "not selected";
	}
	
	if($tfndecform != "")
	{
		$path="admin/upload/Tax_declaration_form/";
		$npath=$path.$token.$tfndecform;
		move_uploaded_file($_FILES['em_tfn_dec__form']['tmp_name'],$npath);
		$emp_tfn_dec_form = $npath;
	}else
	{
		$emp_tfn_dec_form = "not selected";
	}
	
	if($esupchoform != "")
	{
		$path="admin/upload/Employee_superannuation_choice_form/";
		$npath=$path.$token.$esupchoform;
		move_uploaded_file($_FILES['em_superanu_choice_form']['tmp_name'],$npath);
		$emp_super_choice_form = $npath;
	}else
	{
		$emp_super_choice_form = "not selected";
	}
	
	if($unidec != "")
	{
		$path="admin/upload/Uniform_Deduction/";
		$npath=$path.$token.$unidec;
		move_uploaded_file($_FILES['em_uniform_deducation']['tmp_name'],$npath);
		$emp_uniform_dedu = $npath;
	}else
	{
		$emp_uniform_dedu = "not selected";
	}
	
	if($cfn != "")
	{
		$path="admin/upload/Confidentiality_Agreement/";
		$npath=$path.$token.$cfn;
		move_uploaded_file($_FILES['em_con_agreement']['tmp_name'],$npath);
		$em_con_agreement = $npath;
	}else
	{
		$em_con_agreement = "not selected";
	}
	if($acnwdfn != "")
	{
		$path="admin/upload/Crew_Member_Acknowledgment/";
		$npath=$path.$token.$acnwdfn;
		move_uploaded_file($_FILES['em_crewm_Ack']['tmp_name'],$npath);
		$em_crewm_Ack = $npath;
	}else
	{
		$em_crewm_Ack = "not selected";
	}
	if($cpolicyfn != "")
	{
		$path="admin/upload/Employee_Cash_Drawer_Policy/";
		$npath=$path.$token.$cpolicyfn;
		move_uploaded_file($_FILES['em_cashdrawer_policy']['tmp_name'],$npath);
		$em_cashdrawer_policy = $npath;
	}else
	{
		$em_cashdrawer_policy = "not selected";
	}
	if($ltroffn != "")
	{
		$path="admin/upload/CrewStaff_letter_off_Employment_offer/";
		$npath=$path.$token.$ltroffn;
		move_uploaded_file($_FILES['em_cre_letteroff']['tmp_name'],$npath);
		$em_cre_letteroff = $npath ;
	}else
	{
		$em_cre_letteroff = "not selected";
	}
	if($fworkfn != "")
	{
		$path="admin/upload/Fair_Work_Information_Statement/";
		$npath=$path.$token.$fworkfn;
		move_uploaded_file($_FILES['em_fair_workstat']['tmp_name'],$npath);
		$em_fair_workstat = $npath;
	}else
	{
		$em_fair_workstat = "not selected";
	}
	if($wformfn != "")
	{
		$path="admin/upload/WHS_FORM/";
		$npath=$path.$token.$wformfn;
		move_uploaded_file($_FILES['em_whs_form']['tmp_name'],$npath);
		$em_whs_form = $npath;
	}else
	{
		$em_whs_form = "not selected";
	}
	if($wresfn != "")
	{
		$path="admin/upload/WHS_Responsibility/";
		$npath=$path.$token.$wresfn;
		move_uploaded_file($_FILES['em_whs_responsibility']['tmp_name'],$npath);
		$em_whs_responsibility = $npath ;
	}else
	{
		$em_whs_responsibility = "not selected";
	}
	
	

  $em_family_name = $_POST['em_family_name'];
  $em_first_name = $_POST['em_first_name'];
  $em_birthdate = $_POST['em_birthdate'];
  $em_address = $_POST['em_address'];
  $em_email_address = $_POST['em_email_address'];
  $em_postcode = $_POST['em_postcode'];
  $em_phone_no = $_POST['em_phone_no'];
  $em_mobile_no = $_POST['em_mobile_no'];
  
  $em_kin_name = $_POST['em_kin_name'];
  $em_kin_relation = $_POST['em_relation_to_kin'];
  $em_kin_address = $_POST['em_kin_address'];
  $em_kin_contact = $_POST['em_contact_kin_details'];
  $em_kin_email = $_POST['em_kin_email'];
  
  $em_entitled_australia = $_POST['em_entitled_australia_rd'];
  $em_identity_doc = ($_POST['em_identity_doc1'].','.$_POST['em_identity_doc2'].','.$_POST['em_identity_doc3']);
  $em_current_study_status =  implode('#', $_POST['em_current_study']);
  $em_study_type = implode('#', $_POST['em_study_type']);
  $em_institute_type = implode('#', $_POST['em_institute_type']);
  $em_hs_name = $_POST['em_hs_name'];
  $em_hs_grade = $_POST['em_hs_grade'];
  $em_hs_year = $_POST['em_hs_year'];
  $em_ps_commenced = implode('#', $_POST['em_ps_commenced']);
  $em_ps_year = implode('#', $_POST['em_ps_year']);
  $em_qualification = implode('#', $_POST['em_qualification']);
  
  $em_signature = $_POST['sign'];
  $em_sign_name = $_POST['sign_name'];
  $em_date = $_POST['em_date'];
	
  //POSITION APPLIED FOR

  $em_job_title ="";// implode('#', $_POST['em_job_title']);
  $em_position_status =""; //implode('#', $_POST['em_position_status_ch']);
  $em_working_hours =""; //$_POST['em_woking_hours'];
  $em_limit_availability = implode('#', $_POST['em_limit_availability']);
  $em_monday_detail = implode('#', $_POST['monday_detail']);
  $em_tuesday_detail = implode('#', $_POST['tuesday_detail']);
  $em_wednesday_detail = implode('#', $_POST['wednesday_detail']);
  $em_thursday_detail = implode('#', $_POST['thursday_detail']);
  $em_friday_detail = implode('#', $_POST['friday_detail']);
  $em_saturday_detail = implode('#', $_POST['saturday_detail']);
  $em_sunday_detail = implode('#', $_POST['sunday_detail']);

  //GENERAL INFO

  $em_skill = $_POST['em_skill'];
  $em_hobby = $_POST['em_hobby'];
  $em_prep_medical = $_POST['em_prep_medical'];
  $em_physical_disability_status = implode('#', array( $_POST['em_disability_status'],$_POST['em_disability_status_info'] ));
  $em_nature_of_work = $_POST['em_nature_of_work'];
  $em_injury_status = implode('#', array( $_POST['em_injury_status'],$_POST['em_injury_status_info'] ));
  $em_criminal_status =  implode('#', array( $_POST['em_criminal_status'],$_POST['em_criminal_status_info'] ));
  $em_charges_status =  implode('#', array( $_POST['em_charges_status'],$_POST['em_charges_status_info'] ));
  $em_additional_info = $_POST['em_additionl'];

  //WORK HISTORY

  $em_employer_name = implode('#', $_POST['em_employer_name']);
  $em_from_to = implode('#', $_POST['em_from_to']);  
  $em_position = implode('#', $_POST['em_position']);  
  $em_reason = implode('#', $_POST['em_reason']);  
  $em_reference = implode('#', $_POST['em_reference']);
	
  if($isupdate == 1 )
  {
  
  		$employee_master = mysqli_query($link,"UPDATE `employee_master` SET 
  				`em_family_name`='".$em_family_name."',
  				`em_first_name`='".$em_first_name."',
  				`em_address`='".$em_address."',
  				`em_postcode`='".$em_postcode."',
  				`em_birthdate`='".$em_birthdate."',
  				`em_phone_no`='".$em_phone_no."',
  				`em_mobile_no`='".$em_mobile_no."',
  				`em_entitled_austrailia`='".$em_entitled_australia."',
  				`em_identity_doc`='".$em_identity_doc."',
  				`em_current_study_status`='".$em_current_study_status."',
  				`em_study_type`='".$em_study_type."',
  				`em_institute_type`='".$em_institute_type."',
  				`em_hs_name`='".$em_hs_name."',
  				`em_hs_grade`='".$em_hs_grade."',
  				`em_hs_year`='".$em_hs_year."',
  				`em_ps_commenced`='".$em_ps_commenced."',
  				`em_ps_year`='".$em_ps_year."',
  				`em_qualification`='".$em_qualification."',
  				`em_kin_name`='".$em_kin_name."',
  				`em_kin_relation`='".$em_kin_relation."',
  				`em_kin_address`='".$em_kin_address."',
  				`em_kin_contact`='".$em_kin_contact."',
  				`em_kin_email`='".$em_kin_email."',
  				`em_signature`='".$em_signature."',
  				`name_of_sign`='".$em_sign_name."',
  				`em_signature_date`='".$em_date."',
  				`em_terms_con`='1',
  				`employee_status` = '0',
  				`updated_date`='".date("Y-m-d H:i:s")."' WHERE em_id = '".$foreign_key."'");

  }else{

	  $employee_master = mysqli_query($link,"INSERT INTO `employee_master`( `store_id`, `em_family_name`, `em_first_name`, `em_address`, `em_email_address`,`em_postcode`,`em_birthdate`, `em_phone_no`, `em_mobile_no`, `em_entitled_austrailia`, `em_identity_doc`, `em_current_study_status`, `em_study_type`, `em_institute_type`, `em_hs_name`, `em_hs_grade`, `em_hs_year`, `em_ps_commenced`, `em_ps_year`, `em_qualification`,`em_kin_name`,`em_kin_relation`,`em_kin_address`,`em_kin_contact`,`em_kin_email`, `em_signature`,`name_of_sign`, `em_signature_date`,`em_terms_con`, `form_access_token`,`created_date`, `updated_date`) VALUES ('".$store_id."','".$em_family_name."','".$em_first_name."','".$em_address."', '".$em_email_address."','".$em_postcode."','".$em_birthdate."','".$em_phone_no."','".$em_mobile_no."','".$em_entitled_australia."','".$em_identity_doc."','".$em_current_study_status."','".$em_study_type."','".$em_institute_type."','".$em_hs_name."','".$em_hs_grade."','".$em_hs_year."','".$em_ps_commenced."','".$em_ps_year."','".$em_qualification."','".$em_kin_name."','".$em_kin_relation."','".$em_kin_address."','".$em_kin_contact."','".$em_kin_email."','".$em_signature."','".$em_sign_name."','".$em_date."','1','".$_REQUEST['token']."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')") or die(mysqli_error($link));

	    $foreign_key = $link->insert_id;
}

	$i = 0;
	$em_ps_commencedd = explode('#' , $em_ps_commenced);
	$em_qualificationn =  explode('#' , $em_qualification);
	foreach($_POST['em_ps_year'] as $year )
  	{
	  	$wmployee_qualification = mysqli_query($link,"INSERT INTO `employee_qualification` ( `employee_id` ,`year_commenced` ,`year_completed` ,`employe_qulification` ,`modified_date`) VALUES ('".$foreign_key."','".$em_ps_commencedd[$i]."','".$year."','".$em_qualificationn[$i]."','".date("Y-m-d H:i:s")."')");
		
		$i++;
	}
	$k = 0;
	
  	foreach($_POST['em_bank_name'] as $bank)
	{
		if($k == 0){ $primary = 1; }else{ $primary = 0; }
		$employee_bank_details = mysqli_query($link ,"INSERT INTO `tbl_user_bank_detail` (`fk_employee_id`,`bank_name`,`branch_name`,`account_name`,`bsb_number`,`account_number`,`is_primary`,`create_date`,`modified_date`) VALUES( '".$foreign_key."', '".$_POST['em_bank_name'][$k]."', '".$_POST['em_branch'][$k]."', '".$_POST['em_account_name'][$k]."', '".$_POST['em_bsb_number'][$k]."', '".$_POST['em_account_num'][$k]."', '".$primary."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
		$k++;
	}
  
  
	
  $employee_applied_for = mysqli_query($link, "INSERT INTO  `employee_applied_for` (  `em_id` ,  `em_job_title` ,  `em_position_status` , `em_working_hours`, `em_limit_availability` ,  `em_monday_detail` ,  `em_tuesday_detail` , `em_wednesday_detail` ,  `em_thursday_detail` ,  `em_friday_detail` ,  `em_saturday_detail` ,  `em_sunday_detail` ,  `created_date` ,  `updated_date` ) VALUES ('".$foreign_key."' , '".$em_job_title."' , '".$em_position_status."' , '".$em_working_hours."','".$em_limit_availability."' , '".$em_monday_detail."' , '".$em_tuesday_detail."' , '".$em_wednesday_detail."' , '".$em_thursday_detail."' , '".$em_friday_detail."' , '".$em_saturday_detail."','".$em_sunday_detail."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."') ") or die(mysqli_error($link));

	
  $employee_general_info = mysqli_query($link, " INSERT INTO `employee_general_info`(`em_id`, `em_skill`, `em_hobby`, `em_prep_medical`, `em_physical_disability_status`, `em_nature_of_work`, `em_injury_status`, `em_criminal_status`, `em_charges_status`, `em_additional_info`,`em_con_agreement`,`em_crewm_Ack`,`em_cashdrawer_policy`,`em_cre_letteroff`,`em_fair_workstat`,`em_whs_form`,`em_whs_responsibility`,`emp_super_choice_form`,`emp_uniform_dedu`, `emp_uniform_policy_form`, `emp_tfn_dec_form`,`created_date`, `updated_date`) VALUES ('".$foreign_key."','".$em_skill."','".$em_hobby."','".$em_prep_medical."','".$em_physical_disability_status."','".$em_nature_of_work."','".$em_injury_status."','".$em_criminal_status."','".$em_charges_status."','".$em_additional_info."','".$em_con_agreement."','".$em_crewm_Ack."','".$em_cashdrawer_policy."','".$em_cre_letteroff."','".$em_fair_workstat."','".$em_whs_form."','".$em_whs_responsibility."','".$emp_super_choice_form."','".$emp_uniform_dedu."','".$emp_uni_policy_form."','".$emp_tfn_dec_form."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."') ");

    $employee_work_history = mysqli_query($link, " INSERT INTO `employee_work_history`(`em_id`, `em_employer_name`, `em_from_to`, `em_position`, `em_reason`, `em_reference`, `created_date`, `updated_date`) VALUES ('".$foreign_key."','".$em_employer_name."','".$em_from_to."','".$em_position."','".$em_reason."','".$em_reference."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')");

	
  if($employee_master && $employee_applied_for && $employee_general_info && $employee_work_history)
  {
    echo "THANK YOU FOR SENDING ALL EMPLOYMENT FORMS. RESTAURANT MANAGER WILL CALL YOU SOON TO DISCUSS Your ORINENTATION VERY SOON.";
  } 
  else
  {
    echo "Error".mysqli_errno($link);
  }
}
else
{
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
<title>Welcome to OG Foods Pty Ltd</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src='libs/jquery.js' type='text/javascript'></script>
<script src="libs/jSignature.min.js"></script>
<script src="libs/modernizr.js"></script>

<script type="text/javascript" src="libs/flashcanvas.js"></script>
<script>
$(document).ready(function() {
 
});
	
function AddRow()
{
    $('#myTable').append('<tr><td>&nbsp;</td><td><input type="text" name="em_employer_name[]" class="form-control"></td><td><input type="text" name="em_from_to[]" class="form-control"></td><td><input type="text" name="em_position[]" class="form-control"></td><td><input type="text" name="  em_reason[]" class="form-control"></td><td><input type="text" name="em_reference[]" class="form-control"></td></tr>')
}

</script>
</head>

<body>
<!--<h1 class="text-center pt-3">OG Foods Pty Ltd T/As Hungry Jacks Wodonga</h1>-->
<h2 class="text-center pt-3">Application for Employment Form</h2>
<h3 class="text-center pt-3 pb-3">PRIVATE AND CONFIDENTIAL</h3>
<div class="container">
<form action="" method="POST" enctype="multipart/form-data" >
  <div class="col-sm-12 titleColor">1. PERSONAL DETAILS</div>
  <div class="col-sm-12 bgMain">
    <ul class="formMain">
      <li class="col-sm-3">
        <label>Family Name: <i style="color: red;">*</i></label>
        <input type="text" name="em_family_name" id="em_family_name" class="form-control">
      </li>
      <li class="col-sm-3">
        <label>First Name:<i style="color: red;">*</i></label>
        <input type="text" name="em_first_name" id="em_first_name" class="form-control">
      </li>
      <li class="col-sm-3">
        <label>Address:<i style="color: red;">*</i></label>
        <input type="text" name="em_address" id="em_address" class="form-control">
      </li>
      <li class="col-sm-3">
        <label>Email:<i style="color: red;">*</i></label>
        <input type="text" name="em_email_address" id="em_email_address" class="form-control" value="<?php echo $email_address; ?>" <?php if($email_address){ echo "disabled"; } ?>>
      </li>	
      <li class="col-sm-3 pt-3">
        <label>Postcode:<i style="color: red;">*</i></label>
        <input type="text" name="em_postcode" id="em_postcode" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Birthdate:<i style="color: red;">*</i></label>
        <input type="text" name="em_birthdate" id="em_birthdate" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Phone No:<i style="color: red;">*</i></label>
        <input type="text" name="em_phone_no" id="em_phone_no" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Mobile:<i style="color: red;">*</i></label>
        <input type="text" name="em_mobile_no" id="em_mobile_no" class="form-control">
      </li>
      
      <li class="col-sm-6 pt-3">
        <label>Are you legally entitled to work in Australia?</label>
        <div class="col-sm-6">Yes
          <input type="radio" id="em_entitled_australia_rd" value="1" name="em_entitled_australia_rd" checked>
        </div>
        <div class="col-sm-6">No
          <input type="radio" name="em_entitled_australia_rd" value="0" id="em_entitled_australia_rd">
        </div>
      </li>
      <li class="col-sm-6 pt-3">
        <label>What identification can you produce to verify this?</label>
        <div class="col-sm-6">Passport</div>
        <div class="col-sm-3">Yes
          <input type="radio" name="em_identity_doc1" value="1" id="em_identity_doc1" checked>
        </div>
        <div class="col-sm-3">No
          <input type="radio" name="em_identity_doc1" value="0" id="em_identity_doc1">
        </div>
        <div class="col-sm-6">Birth Certificate</div>
        <div class="col-sm-3">Yes
          <input type="radio" name="em_identity_doc2" value="1" id="em_identity_doc2" checked>
        </div>
        <div class="col-sm-3">No
          <input type="radio" name="em_identity_doc2" value="0" id="em_identity_doc2">
        </div>
        <div class="col-sm-6">Other (provide details)</div>
        <div class="col-sm-3">Yes
          <input type="radio" name="em_identity_doc3" value="1" id="em_identity_doc3" checked>
        </div>
        <div class="col-sm-3">No
          <input type="radio" name="em_identity_doc3" value="0" id="em_identity_doc3">
        </div>
      </li>
    </ul>
  </div>
  <div class="col-sm-12 titleColor">2. KIN DETAILS</div>
  <div class="col-sm-12 bgMain">
    <ul class="formMain">
      <li class="col-sm-3">
        <label>Name of Kin: <i style="color: red;">*</i></label>
        <input type="text" name="em_kin_name" id="em_kin_name" class="form-control">
      </li>
      <li class="col-sm-3">
        <label>Relationship To Kin:<i style="color: red;">*</i></label>
        <input type="text" name="em_relation_to_kin" id="em_relation_to_kin" class="form-control">
      </li>
      <li class="col-sm-6">
        <label>Address Of Kin:<i style="color: red;">*</i></label>
        <input type="text" name="em_kin_address" id="em_kin_address" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Contact Detail Of The kin:<i style="color: red;">*</i></label>
        <input type="text" name="em_contact_kin_details" id="em_contact_kin_details" class="form-control">
      </li>
      <li class="col-sm-3 pt-3">
        <label>Email:<i style="color: red;">*</i></label>
        <input type="text" name="em_kin_email" id="em_kin_email" class="form-control">
      </li>
    </ul>
  </div>
  <div class="col-sm-12 titleColor">3. POSITION APPLIED FOR</div>
  <div class="col-sm-12 bgMain">
    <ul class="formMain">
    <!--  <li class="col-sm-6">
        <label>Job Title:<i style="color: red;">*</i></label>
        <select name="em_job_title[]" id="em_job_title" class="form-control">
			<option value="0">Select Job Titles</option>
        	<option value="Assistant Manager">Assistant Manager</option>
            <option value="Employee">Employee</option>  
            <option value="Team Leader">Team Leader</option>
            <option value="2IC">2IC</option>        
        </select>
        </li>
        <li class="col-sm-6">
         <label>Second Choice (if relevant)</label>
         <select name="em_job_title[]" id="em_job_title" class="form-control">
         	<option value="">Select Second Choice</option>
        	<option value="Assistant Manager">Assistant Manager</option>
            <option value="Employee">Employee</option>     
            <option value="Team Leader">Team Leader</option>
            <option value="2IC">2IC</option>   
        </select>
      
      </li>-->
    <!--  <li class="col-sm-6">
        <label>Second Choice (if relevant)</label>
        <input type="text" class="form-control" name="em_job_title[]"  id="em_job_title">
      </li>-->
     <!-- <li class="col-sm-6 pt-3">
        <label>Status of position applied for:<i style="color: red;">*</i></label>
        <div class="col-sm-4">
          <input type="checkbox" name="em_position_status_ch[]" value="full_time" id="em_position_status_ch">
          Full-Time</div>
        <div class="col-sm-4">
          <input type="checkbox" name="em_position_status_ch[]" value="part_time" id="em_position_status_ch">
          Part-Time</div>
        <div class="col-sm-4">
          <input type="checkbox" name="em_position_status_ch[]" value="casual" id="em_position_status_ch">
          Casual</div>
      </li>-->
     <!-- <li class="col-sm-4 pt-3">
        <label>How many hours you can work:<i style="color: red;">*</i></label>
	        <input type="text" name="em_woking_hours" id="em_woking_hours" class="form-control">  -->
        
      </li>
      <li class="col-sm-12 pt-3">
        <label>Are there any circumstances known to you which could affect your ability to undertake either shift or weekend work?<i style="color: red;">*</i></label>
        <div class="col-sm-4">
          <input type="checkbox" name="em_limit_availability[]" id="em_limit_availability" value="Yes">
          Yes</div>
        <div class="col-sm-4">
          <input type="checkbox" name="em_limit_availability[]" id="em_limit_availability" values="No">
          No</div>
      </li>
      <li class="col-sm-12 pt-3">
        <label>If Yes, please provide details of any limitations on your availability.</label>
        <textarea class="form-control" name="em_limit_availability[]" id="em_limit_availability"></textarea>
      </li>
      <li class="col-sm-12 pt-3">
        <label>Please indicate your work avaiablity :</label>
        <div class="table-responsive">
          <table class="table table-striped table-hover">
            <tbody>
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
                <th>Saturday</th>
                <th>Sunday</th>
              </tr>
            </thead>
            <tr>
              <td>Start Time:</td>
              <td>
              	<select name="monday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="tuesday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="wednesday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="thursday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="friday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="saturday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="sunday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
            </tr>
            <tr>
              <td>End Time:</td>
              <td>
              	<select name="monday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="tuesday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="wednesday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="thursday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="friday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="saturday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
              <td>
              	<select name="sunday_detail[]" class="form-control"><?php 
				$output = '';
              	$interval = '+60 minutes' ;
              	 $current = strtotime( '00:00' );
   				 $end = strtotime( '23:59' );

   				while( $current <= $end ) {
		        $time = date( 'H:i', $current );
		       
		        $output .= "<option value=\"{$time}\"{$sel}>" . date( 'h.i A', $current ) .'</option>';
		        $current = strtotime( $interval, $current );
		   		 }
		        echo $output;
				?></select>
              </td>
            </tr>
              </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
  <div class="col-sm-12 titleColor">4. EDUCATION/TRAINING</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-12">
      <div class="bgSub p-3">» Current Study</div>
      <ul class="formMain">
        <li class="col-sm-4 pt-3">
          <label>Are you currently studying?<i style="color: red;">*</i></label>
          <div class="col-sm-9">
            <input type="checkbox" name="em_current_study[]" value="1" id="em_current_study">
            Yes (If yes, specify below)</div>
          <div class="col-sm-3">
            <input type="checkbox" name="em_current_study[]" value="0" id="em_current_study">
            No</div>
        </li>
        <li class="col-sm-4 pt-3">
          <label>What is your form of study?<i style="color: red;">*</i></label>
          <div class="col-sm-6">
            <input type="checkbox" name="em_study_type[]" value="full_time" id="em_study_type">
            Full time</div>
          <div class="col-sm-6">
            <input type="checkbox" name="em_study_type[]" value="part_time" id="em_study_type">
            Part time</div>
        </li>
        <li class="col-sm-4 pt-3">
          <label>Which institution do you attend?<i style="color: red;">*</i></label>
          <div class="col-sm-4">
            <input type="checkbox" name="em_institute_type[]" value="school" id="em_institute_type">
            School</div>
          <div class="col-sm-5">
            <input type="checkbox" name="em_institute_type[]" value="university" id="em_institute_type">
            University</div>
          <div class="col-sm-5">
            <input type="checkbox" name="em_institute_type[]" value="tafe/other" id="em_institute_type">
            TAFE/Other</div>
        </li>
      </ul>
    </div>
    <div class="col-sm-12 pt-3">
      <div class="bgSub p-3">» Qualifications Attained</div>
      <div class="brdSub">Secondary -</div>
      <ul class="formMain">
        <li class="col-sm-5 pt-3">
          <label>Name of High School attended:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_hs_name" id="em_hs_name">
        </li>
        <li class="col-sm-5 pt-3">
          <label>Highest grade/level passed:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_hs_grade" id="em_hs_grade">
        </li>
        <li class="col-sm-2 pt-3">
          <label>In what year?:</label>
   <!--       <input type="text" class="form-control" name="em_hs_year" id="em_hs_year">-->
          <select name="em_hs_year" id="em_hs_year" class="form-control">
          	<option value="1">Select Year</option>
			<?php 
               for($i = 1970 ; $i < date('Y'); $i++){
                  echo "<option value='$i'>$i</option>";
               }
            ?>
            </select>
        </li>
      </ul>
      <div class="col-sm-12 p-0 pt-3">
        <div class="brdSub">Post-Secondary – <small>(attach a separate document if insufficient space)</small></div>
        <ul class="formMain">
          <li class="col-sm-4 pt-3">
            <label>When commenced:</label>
            <div class="col-sm-12 pt-3"> 
              <!--<input type="text" class="form-control" placeholder="1" name="em_ps_commenced[]" id="em_ps_commenced1">-->
            <select name="em_ps_commenced[]" id="em_ps_commenced[]" class="form-control">
            	<option>Select Year</option>
			<?php 
               for($i = date('Y') ; $i > 1969; $i--){
                  echo "<option value='$i'>$i</option>";
               }
            ?>
            </select>
            </div>
            <div class="col-sm-12 pt-3">
             <!-- <input type="text" class="form-control" placeholder="2" name="em_ps_commenced[]" id="em_ps_commenced2">-->
             <select name="em_ps_commenced[]" id="em_ps_commenced[]" class="form-control">
             	<option>Select Year</option>
			<?php 
               for($i = date('Y') ; $i > 1969; $i--){
                  echo "<option value='$i'>$i</option>";
               }
            ?>
            </select>
            </div>
            <div class="col-sm-12 pt-3">
              <!--<input type="text" class="form-control" placeholder="3" name="em_ps_commenced[]" id="em_ps_commenced3">-->
              <select name="em_ps_commenced[]" id="em_ps_commenced[]" class="form-control">
              	<option>Select Year</option>
			<?php 
               for($i = date('Y') ; $i > 1969; $i--){
                  echo "<option value='$i'>$i</option>";
               }
            ?>
            </select>
            </div>
          </li>
          <li class="col-sm-4 pt-3">
            <label>Year completed:</label>
            <div class="col-sm-12 pt-3">
              <!--<input type="text" class="form-control" placeholder="1" name="em_ps_year[]" id="em_ps_year1">-->			
              <select name="em_ps_year[]" id="em_ps_year[]" class="form-control">
              	<option>Select Year</option>
			<?php 
               for($i = date('Y') ; $i > 1969; $i--){
                  echo "<option value='$i'>$i</option>";
               }
            ?>
            </select>
            </div>
            <div class="col-sm-12 pt-3">
<!--              <input type="text" class="form-control" placeholder="2" name="em_ps_year[]" id="em_ps_year2">
-->     	<select name="em_ps_year[]" id="em_ps_year[]" class="form-control">
				<option>Select Year</option>
			<?php 
               for($i = date('Y') ; $i > 1969; $i--){
                  echo "<option value='$i'>$i</option>";
               }
            ?>
            </select> 
		    </div>
            <div class="col-sm-12 pt-3">
<!--              <input type="text" class="form-control" placeholder="3" name="em_ps_year[]" id="em_ps_year3">
-->         <select name="em_ps_year[]" id="em_ps_year[]" class="form-control">
				<option>Select Year</option>
			<?php 
               for($i = date('Y') ; $i > 1969; $i--){
                  echo "<option value='$i'>$i</option>";
               }
            ?>
            </select>   
			</div>
          </li>
          <li class="col-sm-4 pt-3">
            <label>Qualification/s Obtained:</label>
            <div class="col-sm-12 pt-3">
              <input type="text" class="form-control" placeholder="Enter Qualification" name="em_qualification[]" id="em_qualification1">
            </div>
            <div class="col-sm-12 pt-3">
              <input type="text" class="form-control" placeholder="Enter Qualification" name="em_qualification[]" id="em_qualification2">
            </div>
            <div class="col-sm-12 pt-3">
              <input type="text" class="form-control" placeholder="Enter Qualification" name="em_qualification[]" id="em_qualification3">
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-12 titleColor">5. EMPLOYMENT HISTORY</div>
  <div class="col-sm-12 bgMain">
    <p>By providing us with a list of referees whom we may speak to about your prior employment
      history, you acknowledge that we may approach these referees.
      (Note-we will ordinarily seek your consent before contacting your current employer).<br>
      <br>
      Please complete details below. Also attach Resume if available.</p>
    <div class="table-responsive">
      <table class="table table-striped table-hover" id="myTable">
        <tbody>
        <thead>
          <tr>
            <th nowrap><span class="cursor" onclick="AddRow()">+</span></th>
            <th nowrap>Employer Name,Tel No / Address</th>
            <th nowrap>From / To</th>
            <th nowrap>Position</th>
            <th nowrap>Reason for Leaving</th>
            <th nowrap>Referees Name &amp; Phone</th>
          </tr>
        </thead>
          </tbody>
        
      </table>
    </div>
  </div>
  <div class="col-sm-12 titleColor">6. Please list any skills, knowledge or experience that you may have which are
    relevant to the position for which you are applying.</div>
  <div class="col-sm-12 bgMain">
    <textarea class="form-control" name="em_skill" id="em_skill"></textarea>
  </div>
  <div class="col-sm-12 titleColor">7. What are your hobbies and interests?</div>
  <div class="col-sm-12 bgMain">
    <textarea class="form-control" name="em_hobby" id="em_hobby"></textarea>
  </div>
  <div class="col-sm-12 titleColor">8. Are you prepared to attend a medical examination by our Doctor, if required?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_prep_medical" value="1" id="em_prep_medical">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_prep_medical" value="0" id="em_prep_medical">
    </div>
  </div>
  <div class="col-sm-12 titleColor">9a. Do you suffer from any physical or mental disability for which the Company may need to make allowances?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_disability_status" value="1" id="em_disability_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_disability_status" value="0" id="em_disability_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details of such disability and any accommodation that we might need to make to allow you to perform your duties.</p>
    <textarea class="form-control" name="em_disability_status_info" id="em_disability_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">9b. NOTE – Victorian Applicants only to complete –</div>
  <div class="col-sm-12 bgMain">
    <p class="col-sm-12">The nature of the work you will perform for us is</p>
    <div class="col-sm-12">
      <input type="text" class="form-control" name="em_nature_of_work" id="em_nature_of_work">
    </div>
    <p class="col-sm-12 pt-3">Have you any pre-existing injury that may be affected by the nature of the work you perform for us? (Note that failure to notify us of a pre-existing injury which might be affected by your employment with us could result in an injury not being eligible for future compensation claims).</p>
    <div class="col-sm-2">Yes
      <input type="radio" name="em_injury_status" value="1" id="em_injury_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_injury_status" value="0" id="em_injury_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details:</p>
    <textarea class="form-control" name="em_injury_status_info" id="em_injury_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">10a. Have you been convicted of any minor criminal offence in the last five years, or of any major criminal offence in the last ten years?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_criminal_status" value="1" id="em_criminal_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_criminal_status" value="0" id="em_criminal_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details: (a criminal record will not necessarily preclude you from employment, but full disclosure is required).</p>
    <textarea class="form-control" id="em_criminal_status_info" name="em_criminal_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">10b. Do you have any other charges currently pending?</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-2">Yes
      <input type="radio" name="em_charges_status" value="1" id="em_charges_status">
    </div>
    <div class="col-sm-2">No
      <input type="radio" name="em_charges_status" value="0" id="em_charges_status">
    </div>
    <p class="col-sm-12 pt-3">If Yes, please furnish details.</p>
    <textarea class="form-control" name="em_charges_status_info" id="em_charges_status_info"></textarea>
  </div>
  <div class="col-sm-12 titleColor">11. Is there any additional information which you wish to provide which may assist your application for employment?</div>
  <div class="col-sm-12 bgMain">
    <textarea class="form-control" name="em_additionl" id="em_additionl"></textarea>
  </div>
    <div class="col-sm-12 titleColor">12. Upload Other forms</div>
  <div class="col-sm-4 bgMain">
 	<label>Confidentiality Agreement<i style="color: red;">*</i><br><a href="admin/all forms/confidentiality_agreement.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_con_agreement" id="em_con_agreement" accept="application/pdf" required> 
  </div>
  <div class="col-sm-4 bgMain">
    <label>Crew Member Acknowledgment<i style="color: red;">*</i><br><a href="admin/all forms/Crew Acknowledgment Letter.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_crewm_Ack" id="em_crewm_Ack" accept="application/pdf" required> 
  </div>
  <div class="col-sm-4 bgMain">
    <label>Employee Cash Drawer Policy<i style="color: red;">*</i><br><a href="admin/all forms/Employee Cash Drawer.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_cashdrawer_policy" id="em_cashdrawer_policy" accept="application/pdf" required> 
  </div>
  <!--<div class="col-sm-4 bgMain">
    <label>Crew/Staff letter off Employment offer<i style="color: red;">*</i><br><a href="admin/all forms/Employment Letter.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_cre_letteroff" id="em_cre_letteroff" accept="application/pdf" required> 
  </div>-->
  <div class="col-sm-6 bgMain">
    <label>Fair Work Information Statement<i style="color: red;">*</i><br><a href="admin/all forms/Fair-Work-Information-Statement.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_fair_workstat" id="em_fair_workstat" accept="application/pdf" required> 
  </div>
  <div class="col-sm-6 bgMain">
    <label>WHS FORM<i style="color: red;">*</i><br><a href="admin/all forms/WHS Form.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_whs_form" id="em_whs_form" accept="application/pdf" required> 
  </div>
  <div class="col-sm-4 bgMain">
    <label>WHS Responsibility<i style="color: red;">*</i><br><a href="admin/all forms/WHS Respnsibility.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_whs_responsibility" id="em_whs_responsibility" accept="application/pdf" required> 
  </div>
  <div class="col-sm-4 bgMain">
    <label>Emp superannuation choice form<i style="color: red;">*</i><br><a href="admin/all forms/Employee superannuation choice form.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_superanu_choice_form" id="em_superanu_choice_form" accept="application/pdf" required> 
  </div>
  <div class="col-sm-4 bgMain">
    <label>Uniform Deduction<i style="color: red;">*</i><br><a href="admin/all forms/Uniform Deduction.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_uniform_deducation" id="em_uniform_deducation" accept="application/pdf" required> 
  </div>
  
   <div class="col-sm-6 bgMain">
    <label>Uniform Policy<i style="color: red;">*</i><br><a href="admin/all forms/Policy- Uniform 9.2018.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_uniform_policy" id="em_uniform_policy" accept="application/pdf" required> 
  </div>
  <div class="col-sm-6 bgMain">
    <label>Tax File Declaration Form<i style="color: red;">*</i><br><a href="admin/all forms/TFN_declaration_form_N3092.pdf" download><i class="fa fa-download"></i> Download Form</a></label>
   	<input type="file" class="form-control" name="em_tfn_dec__form" id="em_tfn_dec__form" accept="application/pdf" required> 
  </div>
  
   <div class="col-sm-12 titleColor">13. Employee Payroll  Bank Detail</div>
  <div class="col-sm-12 bgMain">
    <div class="col-sm-12">
      <div class="bgSub p-3">» Primary Bank Detail</div>
      <ul class="formMain">
        <li class="col-sm-4 pt-3">
          <label>Bank Name:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_bank_name[]" id="em_bank_namep">
        </li>
        <li class="col-sm-4 pt-3">
          <label>Branch:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_branch[]" id="em_branchp">
        </li>
        <li class="col-sm-4 pt-3">
          <label>Account Name:</label>
          <input type="text" class="form-control" name="em_account_name[]" id="em_account_namep">
        </li>
      </ul>
      <ul class="formMain">
        <li class="col-sm-4 pt-3">
          <label>BSB Number:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_bsb_number[]" id="em_bsb_numberp"  maxlength="6">
        </li>
        <li class="col-sm-4 pt-3">
          <label>Account Number:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_account_num[]" id="em_account_nump" maxlength="10">
        </li>
        
      </ul>
    </div>
    <div class="col-sm-12 pt-3">
      <div class="bgSub p-3">» For Split Payroll Payment enter second bank account detail</div>
      <div class="brdSub">Secondary -</div>
       <ul class="formMain">
        <li class="col-sm-4 pt-3">
          <label>Bank Name:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_bank_name[]" id="em_bank_names">
        </li>
        <li class="col-sm-4 pt-3">
          <label>Branch:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_branch[]" id="em_branchs" >
        </li>
        <li class="col-sm-4 pt-3">
          <label>Account Name:</label>
          <input type="text" class="form-control" name="em_account_name[]" id="em_account_names">
        </li>
      </ul>
      <ul class="formMain">
        <li class="col-sm-4 pt-3">
          <label>BSB Number:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_bsb_number[]" id="em_bsb_numbers" maxlength="6">
        </li>
        <li class="col-sm-4 pt-3">
          <label>Account Number:<i style="color: red;">*</i></label>
          <input type="text" class="form-control" name="em_account_num[]" id="em_account_nums"  maxlength="10">
        </li>
      </ul>
    </div>
  </div>
  
  <div class="col-sm-12 bgMain mt-3 mb-3">
    <h3>NOTE:</h3>
    <p>(a) A three-month probationary period is applicable to all new employees and during this probationary period the employee’s performance will be reviewed on a regular basis.<br>
      <br>
      (b) The Company reserves the right to install and operate video surveillance equipment in the interests of safety, security and protection of employees and assets. Such equipment will not be used with the intention to monitor employees' work performance or to invade privacy.</p>
  </div>
  <p> In accordance with the Privacy Act, we would like to advise you of the following:
  <h4>Collection of Information:</h4>
  We will only use your personal information for the purpose of assessing your application for employment within the company. The information we collect from you will be handled sensitively and securely with proper regard for privacy. If you do not provide some of the personal information we request when you apply for a position within the company, we may not be able to process your application.<br>
  <br>
  We may choose to keep your details on file for a limited period of time to consider you for other job opportunities as they arise. If you would prefer that we do not keep your details on file, please contact us and we will remove them from our records.<br>
  <br>
  <h4>Disclosures:</h4>
  We will not usually disclose your personal information outside of the comnpany, except where certain functions are outsourced to other organisations (such as recruitment consultants), and then only for the purpose of enabling us to process your application. In these circumstances, confidentiality arrangements will apply to restrict the use and disclosure by those organisations who have your personal information disclosed to them.<br>
  <br>
  <h4>Declaration:</h4>
  Your signature below indicates your consent to the use and disclosure of your personal information as indicated above.<br>
  <br>
  I certify and declare that all particulars and information supplied by me in my application for employment are true and correct and I further understand that if I have knowingly or by neglect supplied details that are found to be false this could lead to the termination of my employment.<br>
  <br>
  Further, I hereby give permission for the company to release any medical or other personal records that may relate to my employment with the company, to specialist advisors for the purposes of obtaining specialist advice reasonably related to my employment.
  </p>
  <div class="col-sm-2">SIGNATURE:</div>
  <div class="col-sm-4" id="signature">
    
  </div>
   <div class="col-sm-2">DATE:</div>
  <div class="col-sm-5">
    <input type="text" class="form-control" name="em_date" id="em_date">
   
    <input type="hidden" class="form-control" name="sign" id="output">
  </div>
  <br><br>
  <div class="col-sm-12">
  <div class="col-sm-2"></div>
   &nbsp;<input type="text" class="" name="sign_name" id="sign_name"> ( Name of an applicant )</div><br>
  <div class="col-sm-12 titleColor"></div>
  <br>
  <input type="checkbox" name="terms" id="terms" >
   I hereby declare that all the above mentioned information given by me is true and correct to the best of my knowledge and belief<i style="color: red;">*</i>
  <div class="col-sm-12 bgMain text-center">
    <input type="submit" name="submit" id="submit"  onclick="return chk(this.form);" value="Submit Forms">
  </div>
<!-- this is to display signature
 <div id="displaySignature">
   </div>
   <script>
         $(document).ready(function(data){
           var i = new Image()
           var signature = signatureDataFromDataBase;
    //Here signatureDataFromDataBase is the string that you saved earlier
            i.src = 'data:' + signature;
            $(i).appendTo('#displaySignature')
           })
   </script>-->
  <p class="pb-3">&nbsp;</p>
</form>
</div>
</body>
</html>
<script>

$(document).ready(function() {

 // Initialize jSignature
 var $sigdiv = $("#signature").jSignature({'UndoButton':true,color:"#00f","background-color":"#999999"});

 $('#submit').click(function(){
  // Get response of type image
  var data = $sigdiv.jSignature('getData', 'svgbase64');

  // Storing in textarea
  $('#output').val(data);

  // Alter image source 
  $('#sign_prev').attr('src',"data:"+data);
  $('#sign_prev').show();
  
  
 });
});

</script>
<script language="javascript">
function trim(stringToTrim)
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function getFileExtension(filename)
{
	return filename.split('.').pop();
}
function chk(form)
{ 
	
	
	if(document.getElementById('terms').checked == false)
	{
		alert("Please Check The Terms and Conditions");	
		document.getElementById('terms').focus();
		return false;
	}
	
	if(trim(document.getElementById("em_family_name").value)=='')
	{
		alert("Please Enter Your Family Name");
		document.getElementById("em_family_name").focus();
		return false;
	}
	
	if(trim(document.getElementById("em_first_name").value)=='')
	{
		alert("Please Enter Your First Name");
		document.getElementById("em_first_name").focus();
		return false;
	}
	
	if(trim(document.getElementById("em_address").value)=='')
	{
		alert("Please Enter Your Address");
		document.getElementById("em_address").focus();
		return false;
	}
	
	if(trim(document.getElementById("em_postcode").value)=='')
	{
		alert("Please Enter Postcode");
		document.getElementById("em_postcode").focus();
		return false;
	}
	
	if(trim(document.getElementById("em_birthdate").value)=='')
	{
		alert("Please Enter Birthdate");
		document.getElementById("em_birthdate").focus();
		return false;
	}
	
	if(trim(document.getElementById("em_phone_no").value)=='')
	{
		alert("Please Enter Your Phone Number");
		document.getElementById("em_phone_no").focus();
		return false;
	}
	if(isNaN(document.getElementById("em_phone_no").value) )
	{
		alert("Please Enter valid Phone Number ");
		document.getElementById("em_phone_no").focus();
		return false;
	}
	if(document.getElementById("em_phone_no").value.length < 6 )
	{
		alert("Please Enter valid Phone Number ");
		document.getElementById("em_phone_no").focus();
		return false;
	}
	
	
	if(trim(document.getElementById("em_mobile_no").value)=='')
	{
		alert("Please Enter Your Mobile Number");
		document.getElementById("em_mobile_no").focus();
		return false;
	}
	if(isNaN(document.getElementById("em_mobile_no").value))
	{
		alert("Please Enter Valid Mobile Number");
		document.getElementById("em_mobile_no").focus();
		return false;
	}
	if(document.getElementById("em_mobile_no").value.length < 10 )
	{
		alert("Please Enter valid Mobile Number ");
		document.getElementById("em_mobile_no").focus();
		return false;
	}
	
	if(document.getElementById('em_entitled_australia_rd').value == "")
	{
		alert("Please Select Legal Entitle to work in Australia.. ");
		document.getElementById("em_entitled_australia_rd").focus();
		return false;
	}
	
	if(document.getElementById('em_kin_name').value == "")
	{
		alert("Please Enter Kin Name.. ");
		document.getElementById("em_kin_name").focus();
		return false;
	}
	
	if(document.getElementById('em_kin_address').value == "")
	{
		alert("Please Enter Kin Address.. ");
		document.getElementById("em_kin_address").focus();
		return false;
	}
	
	if(document.getElementById('em_kin_email').value == "")
	{
		alert("Please Enter Kin Email.. ");
		document.getElementById("em_kin_email").focus();
		return false;
	}
	
	if(document.getElementById('em_contact_kin_details').value == "")
	{
		alert("Please Enter Kin Contact Details.. ");
		document.getElementById("em_contact_kin_details").focus();
		return false;
	}
	
	if(document.getElementById('em_relation_to_kin').value == "")
	{
		alert("Please Enter Kin Relation.. ");
		document.getElementById("em_relation_to_kin").focus();
		return false;
	}
		
/*	if( form.em_job_title[0].selectedIndex == "0")
	{
			
			alert('Please Select Job Title');
			document.getElementById("em_job_title").focus();
			return false;
	}*/

/*	if ( ( form.em_position_status_ch[0].checked == false ) && ( form.em_position_status_ch[1].checked == false ) && ( form.em_position_status_ch[2].checked == false ) ) 
	{
		alert ( "Please choose your status of position applied for" ); 
		document.getElementById("em_position_status_ch").focus();
		return false;
	}*/
	
/*	if(document.getElementById('em_woking_hours').value == "")
	{
		alert("Please Enter Working Hours.. ");
		document.getElementById("em_woking_hours").focus();
		return false;
	}*/

	if ( ( form.em_limit_availability[0].checked == false ) && ( form.em_limit_availability[1].checked == false ) ) 
	{
		alert ( "Please choose Are there any circumstances known to you which could affect your ability to undertake either shift or weekend work?" ); 
		document.getElementById("em_limit_availability").focus();
		return false;
	}
	if ( ( form.em_limit_availability[0].checked == true ) && ( form.em_limit_availability[1].checked == true ) ) 
	{
		alert ( "Please choose one in ..Are there any circumstances known to you which could affect your ability to undertake either shift or weekend work?" ); 
		document.getElementById("em_limit_availability").focus();
		return false;
	}
	
	if( (form.em_limit_availability[0].checked == true) &&(form.em_limit_availability[2].value == "") )
	{
			alert("Please provide details of any limitations on your availability. ");
			document.getElementById("em_limit_availability").focus();
			return false;
	}
	
	if( (form.em_current_study[0].checked == false) &&(form.em_current_study[1].checked == false) )
	{
			alert("Please provide details of currently studying. ");
			document.getElementById("em_current_study").focus();
			return false;
	}
	
	if( (form.em_study_type[0].checked == false) &&(form.em_study_type[1].checked == false) )
	{
			alert("Please provide details of your form of study. ");
			document.getElementById("em_study_type").focus();
			return false;
	}
	
	if( (form.em_institute_type[0].checked == false) && (form.em_institute_type[1].checked == false) &&(form.em_institute_type[2].checked == false) )
	{
			alert("Please provide details of institution you attend. ");
			document.getElementById("em_institute_type").focus();
			return false;
	}
	
	if(document.getElementById('em_hs_name').value == "")
	{
			alert("Please provide details of High School attended. ");
			document.getElementById("em_hs_name").focus();
			return false;
	}
	
	if(document.getElementById('em_hs_grade').value == "")
	{
			alert("Please provide details of High School attended. ");
			document.getElementById("em_hs_grade").focus();
			return false;
	}
	if(document.getElementById('em_hs_year').value == "1")
	{
			alert("Please Select Year. ");
			document.getElementById("em_hs_year").focus();
			return false;
	}
	if(document.getElementById('em_con_agreement').value != "" )
	{
		var ftype = document.getElementById('em_con_agreement').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				alert("Select Only pdf Files .....");
				 document.getElementById('em_con_agreement').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_crewm_Ack').value != "" )
	{
		var ftype = document.getElementById('em_crewm_Ack').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				alert("Select Only pdf Files .....");
				 document.getElementById('em_crewm_Ack').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_cashdrawer_policy').value != "" )
	{
		var ftype = document.getElementById('em_cashdrawer_policy').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				alert("Select Only pdf Files .....");
				 document.getElementById('em_cashdrawer_policy').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_cre_letteroff').value != "" )
	{
		var ftype = document.getElementById('em_cre_letteroff').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				alert("Select Only pdf Files .....");
				 document.getElementById('em_cre_letteroff').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_fair_workstat').value != "" )
	{
		var ftype = document.getElementById('em_fair_workstat').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				alert("Select Only pdf Files .....");
				 document.getElementById('em_fair_workstat').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_whs_form').value != "" )
	{
		var ftype = document.getElementById('em_whs_form').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				alert("Select Only pdf Files .....");
				 document.getElementById('em_whs_form').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_whs_responsibility').value != "" )
	{
		var ftype = document.getElementById('em_whs_responsibility').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				 alert("Select Only pdf Files .....");
				 document.getElementById('em_whs_responsibility').focus();
				 return false;
		}
		
	}
	if(document.getElementById('em_superanu_choice_form').value != "" )
	{
		var ftype = document.getElementById('em_superanu_choice_form').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				 alert("Select Only pdf Files .....");
				 document.getElementById('em_superanu_choice_form').focus();
				 return false;
		}
		
	}
	if(document.getElementById('em_uniform_deducation').value != "" )
	{
		var ftype = document.getElementById('em_uniform_deducation').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				 alert("Select Only pdf Files .....");
				 document.getElementById('em_uniform_deducation').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_uniform_policy').value != "" )
	{
		var ftype = document.getElementById('em_uniform_policy').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				 alert("Select Only pdf Files .....");
				 document.getElementById('em_uniform_policy').focus();
				 return false;
		}
		
	}
	
	if(document.getElementById('em_tfn_dec__form').value != "" )
	{
		var ftype = document.getElementById('em_tfn_dec__form').value;
	
		var tpe = getFileExtension(ftype); //returns doc

		if(tpe != "pdf")
		{
				 alert("Select Only pdf Files .....");
				 document.getElementById('em_tfn_dec__form').focus();
				 return false;
		}
		
	}
	
	///bd-validation
	if(trim(document.getElementById('em_bank_namep').value) == "")
	{
			 alert("Please Confirm Your Bank Name...");
	 		 document.getElementById('em_bank_namep').focus();
			 return false;
	}
	if(trim(document.getElementById('em_branchp').value) == "")
	{
			 alert("Please Confirm Your Branch Name...");
			 document.getElementById('em_branchp').focus();
			 return false;
	}
	if(trim(document.getElementById('em_account_namep').value) == "")
	{
			 alert("Please Confirm Your Account Name...");
			 document.getElementById('em_account_namep').focus();
			 return false;
	}
	if(trim(document.getElementById('em_bsb_numberp').value) == "")
	{
			 alert("Please Confirm Your Bank BSB number...");
			 document.getElementById('em_bsb_numberp').focus();
			 return false;
	}
	if(trim(document.getElementById('em_account_nump').value) == "")
	{
			 alert("Please Confirm Your Account Number...");
             document.getElementById('em_account_nump').focus();
			 return false;
	}
	if(isNaN(document.getElementById('em_account_nump').value))
	{
			alert("Please Enter valid Account Number ");
			document.getElementById("em_account_nump").focus();
			return false;	
	}
	if(isNaN(document.getElementById('em_account_nums').value))
	{
			alert("Please Enter valid Account Number ");
			document.getElementById("em_account_nums").focus();
			return false;	
	}
	if(isNaN(document.getElementById('em_bsb_numberp').value))
	{
			alert("Please Enter valid BSB Number ");
			document.getElementById("em_bsb_numberp").focus();
			return false;	
	}
	if(isNaN(document.getElementById('em_bsb_numbers').value))
	{
			alert("Please Enter valid BSB Number ");
			document.getElementById("em_bsb_numbers").focus();
			return false;	
	}
	
	///bd-validation
	
	
	if(trim(document.getElementById('sign_name').value) == "")
	{
			 alert("Please Confirm Your signature...");
			 document.getElementById('sign_name').focus();
			 return false;
	}
	
	if(trim(document.getElementById('em_date').value) == "")
	{
			 alert("Please Confirm Your signature date...");
			 document.getElementById('em_date').focus();
			 return false;
	}
	alert("Are you sure want to submit all forms?");
}

</script>

<!--<script language="javascript">
$("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
</script>-->
<?php  } ?>